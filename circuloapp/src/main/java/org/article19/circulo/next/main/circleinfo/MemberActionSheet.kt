package org.article19.circulo.next.main.circleinfo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.maxkeppeler.sheets.core.Sheet
import info.guardianproject.keanu.core.util.PrettyTime
import org.article19.circulo.next.databinding.SheetMemberActionBinding
import java.util.Date

class MemberActionSheet(val name: String?,
                        val lastActiveTime: Long?,
                        val roleActionText: String? = null) : Sheet() {

    private lateinit var mBinding: SheetMemberActionBinding
    private var mMemberActionListener: MemberActionListener? = null

    override fun onCreateLayoutView(): View {
        mBinding = SheetMemberActionBinding.inflate(LayoutInflater.from(activity))
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        name?.let { mBinding.tvTitle.text = name }
        lastActiveTime?.let {
            val lastActiveDate = Date(Date().time - lastActiveTime)
            mBinding.tvLastActiveTime.text = PrettyTime.format(lastActiveDate)
        }
        mBinding.tvKickOut.setOnClickListener {
            mMemberActionListener?.onKickOut()
        }

        mBinding.tvGrantModerator.setOnClickListener {
            mMemberActionListener?.onGrantModeratorPermission()
        }
        roleActionText?.let {
            mBinding.tvGrantModerator.setText(roleActionText)
        }
    }

    /** Build [MemberActionSheet] and show it later. */
    fun build(ctx: Context, width: Int? = null, initSheetProperties: MemberActionSheet.() -> Unit): MemberActionSheet {
        this.windowContext = ctx
        this.width = width

        initSheetProperties()

        return this
    }

    /** Build and show [MemberActionSheet] directly. */
    fun show(ctx: Context, width: Int? = null, initSheetProperties: MemberActionSheet.() -> Unit): MemberActionSheet {
        this.windowContext = ctx
        this.width = width

        initSheetProperties()
        show()

        return this
    }

    fun setMemberActionListener(actionListener: MemberActionListener) {
        mMemberActionListener = actionListener
    }

    interface MemberActionListener {
        fun onKickOut()
        fun onGrantModeratorPermission()
    }
}