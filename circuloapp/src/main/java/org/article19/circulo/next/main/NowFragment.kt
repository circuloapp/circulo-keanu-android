package org.article19.circulo.next.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.MotionEvent.ACTION_DOWN
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.ui.room.RoomActivity
import info.guardianproject.keanu.core.util.WrapContentLinearLayoutManager
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.databinding.FragmentNowBinding
import org.article19.circulo.next.main.now.ThreadsAdapter
import org.article19.circulo.next.main.updatestatus.LocationDetailActivity
import org.article19.circulo.next.main.updatestatus.UpdateLocationStatusService
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import java.util.Timer


class NowFragment (): Fragment() {

    private lateinit var mBinding: FragmentNowBinding

    companion object {
        const val TAG = "NowFragment"
    }

    private val mApp: CirculoApp?
        get() = activity?.application as? CirculoApp

    private val mSharedTimeline: SharedTimeline
        get() = SharedTimeline.instance

    private var mThreadsAdapter : ThreadsAdapter? = null

    private var mTimerPress = Timer()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentNowBinding.inflate(inflater, container, false)

        mBinding.toolbar.ivNewStatus.setOnClickListener {

           // newStatus()
            openChat()
        }

        mBinding.toolbar.ivNewLocation.setOnClickListener {

            val locEvent = SharedTimeline.instance.getMyLocationThread()
            if (locEvent != null) {
                val i = Intent(requireActivity(), LocationDetailActivity::class.java)
                i.putExtra(LocationDetailActivity.EXTRA_LOCATION_ID, locEvent.eventId)
                val alertEvent = SharedTimeline.instance.getMyAlertThread()
                alertEvent?.let {
                    i.putExtra(LocationDetailActivity.EXTRA_ALERT_ID,alertEvent.eventId)
                }

                requireActivity().startActivity(i)
            } else {
                (requireActivity() as MainActivity).askSendLocationStatus()
            }
        }

        val llm = WrapContentLinearLayoutManager(context)
        mBinding.recyclerViewPeople.layoutManager = llm

        subscribeUserInfo()
        update(false)

        mThreadsAdapter = ThreadsAdapter(requireActivity() as MainActivity, emptyList())
        mBinding.recyclerViewPeople.adapter = mThreadsAdapter

        mBinding.recyclerViewPeople.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                if (!mSharedTimeline.containsStatusByMe) {
                    if (dy > 0) {
                        mBinding.btnSendAlert.visibility = View.GONE
                    } else {
                        mBinding.btnSendAlert.visibility = View.VISIBLE
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        /*
        mBinding.btnSendAlert.setOnLongClickListener() {

            sendUrgentStatus()

            false
        }*/
        mBinding.btnSendAlert.setOnTouchListener { view, motionEvent ->

            lastMotionEvent = motionEvent

            if (motionEvent.action == ACTION_DOWN)
                sendUrgentStatus(motionEvent)
            else {
                (requireActivity() as MainActivity).sendAlertEvent(motionEvent)
            }

            true
        }

        return mBinding.root
    }

    private var lastMotionEvent : MotionEvent? = null


    private fun openChat () {
        var intentChat =Intent(context, RoomActivity::class.java)
        intentChat.putExtra(RoomActivity.EXTRA_ROOM_ID,SharedTimeline.instance.room?.roomId)
        startActivity(intentChat)

    }

    private fun sendUrgentStatus (me: MotionEvent) {



        (requireActivity() as MainActivity).showAlertFragment(me)


    }





    override fun onResume() {
        super.onResume()

        update(mSharedTimeline.getThreads().value?.isNotEmpty() == true)

        mSharedTimeline.getThreads().observe(this) { threads ->
            mThreadsAdapter?.submitList(threads.sortedWith(compareBy<org.article19.circulo.next.main.now.Thread> { it.userId != SharedTimeline.instance.myUserId }.thenBy { it.lastCommentTime }) )

            update(threads.isNotEmpty())
        }
    }

    override fun onPause() {
        super.onPause()

        mSharedTimeline.getThreads().removeObservers(this)
    }

    fun showDefaultEmptyView() {
        mBinding.layoutNoCircleState.visibility = View.VISIBLE
      //  mBinding.tvPeopleTitle.visibility = View.GONE
        mBinding.recyclerViewPeople.visibility = View.GONE
        mBinding.btnSendAlert.visibility = View.GONE
        mBinding.toolbar.ivNewStatus.visibility = View.GONE
        mBinding.toolbar.ivNewLocation.visibility =  View.GONE
    }

    private fun subscribeUserInfo() {
        (activity as? MainActivity)?.observableUserInfo?.observe(viewLifecycleOwner) { user ->
            user?.let {
                mBinding.tvNowGreetings.text = getString(R.string.hello_, it.displayName)
                mBinding.tvNowGreetings.visibility = View.VISIBLE
            }
        }
    }

    private fun update(hasPeople: Boolean) {
        if (mSharedTimeline.isInsideCircle) {
            mBinding.layoutNoCircleState.visibility = View.GONE

            mBinding.toolbar.ivNewStatus.visibility = View.VISIBLE
            mBinding.toolbar.ivNewLocation.visibility = View.VISIBLE // if (mSharedTimeline.containsStatusByMe) View.GONE else View.VISIBLE
            mBinding.btnSendAlert.visibility = if (mSharedTimeline.getMyAlertThread() != null) View.GONE else View.VISIBLE

            if (hasPeople) {
                mBinding.recyclerViewPeople.visibility = View.VISIBLE
                mBinding.layoutNoStatuses.visibility = View.GONE
                mBinding.recyclerViewPeople.smoothScrollToPosition(0)

            }
            else {
                mBinding.recyclerViewPeople.visibility = View.GONE
                mBinding.layoutNoStatuses.visibility = View.VISIBLE
            }
        }
        else {
            showDefaultEmptyView()
        }
    }
}

