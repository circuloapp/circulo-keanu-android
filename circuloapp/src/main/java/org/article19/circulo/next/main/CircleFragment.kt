package org.article19.circulo.next.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.article19.circulo.next.databinding.FragmentCircleBinding

class CircleFragment : Fragment() {

    companion object {
        const val TAG = "CircleFragment"
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentCircleBinding.inflate(inflater, container, false)
        binding.btnJoinCircle.setOnClickListener {
            (activity as? MainActivity)?.joinOrCreateCircleResult?.launch(Intent(context, JoinCircleActivity::class.java))

        }
        binding.btnCreateCircle.setOnClickListener {
            (activity as? MainActivity)?.joinOrCreateCircleResult?.launch(Intent(context, CreateCircleActivity::class.java))
        }

        return binding.root

    }

}