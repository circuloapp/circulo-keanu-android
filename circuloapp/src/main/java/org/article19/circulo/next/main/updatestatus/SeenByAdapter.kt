package org.article19.circulo.next.main.updatestatus

import agency.tango.android.avatarview.AvatarPlaceholder
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.GlideUtils
import org.article19.circulo.next.databinding.RowSeenByBinding

class SeenByAdapter(private val readReceipts: MutableList<ReadReceiptData>) :
    RecyclerView.Adapter<SeenByAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowSeenByBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentMember = readReceipts[position]
        GlideUtils.loadAvatar(context, currentMember.avatarUrl, AvatarPlaceholder(""), holder.viewAvatar)
    }

    override fun getItemCount(): Int {
        return readReceipts.size
    }

    class ViewHolder(binding: RowSeenByBinding) : RecyclerView.ViewHolder(binding.root) {
        val viewAvatar = binding.viewAvatar
    }

    fun updateList(newReadReceipts : MutableList<ReadReceiptData>) {
        readReceipts.clear()
        readReceipts.addAll(newReadReceipts)
        notifyDataSetChanged()
    }
}