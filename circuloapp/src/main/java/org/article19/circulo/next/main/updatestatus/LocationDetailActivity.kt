package org.article19.circulo.next.main.updatestatus

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.Spannable
import android.view.LayoutInflater
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.AttachmentHelper
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanuapp.ui.widgets.AudioWife
import info.guardianproject.keanuapp.ui.widgets.ShareRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.SharedTimeline.Companion.TAG_REACTION_SEEN
import org.article19.circulo.next.SharedTimeline.Companion.TAG_RESOLVED
import org.article19.circulo.next.databinding.ActivityLocationDetailBinding
import org.article19.circulo.next.main.now.Status
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.mention.PillImageSpan
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.room.model.message.MessageLocationContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.util.MatrixItem
import timber.log.Timber
import java.io.File
import java.net.URL
import java.util.Date
import java.util.Timer
import java.util.TimerTask
import kotlin.math.max


class LocationDetailActivity : BaseActivity(),
    Observer<List<Thread>> {

    companion object {
        const val EXTRA_LOCATION_ID = "extra_thread_id"
        const val EXTRA_ALERT_ID = "extra_alert"
        const val EXTRA_RESOLVED = "extra_resolved"

    }

    private lateinit var mBinding: ActivityLocationDetailBinding

    //private var rvAdapter: ResponseMessageAdapter? = null
    private lateinit var mSeenByAdapter: SeenByAdapter

    private var audioFileUri : Uri? = null

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mThreadAlert : Thread? = null
    private var mThreadLocation : Thread? = null

    private val mRoom
        get() = SharedTimeline.instance.room

    private var mLocation = ""

    private lateinit var mAudioFilePath : File
    private var handler = Handler ()

    private var mIsMapAdjusted = false
    private var mIsResolved = false
    private var mIsMe = false

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private val imgAvatar: MutableLiveData<Bitmap> by lazy {
        MutableLiveData<Bitmap>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mApp?.cancelNotifications()

        mIsResolved = intent.getBooleanExtra(EXTRA_RESOLVED,false)

        mBinding = ActivityLocationDetailBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.toolbar.ivBack.setOnClickListener {
            finish()
        }

        var locationThreadId = intent.getStringExtra(EXTRA_LOCATION_ID) ?: return finish()
        mThreadLocation = SharedTimeline.instance.getThread(locationThreadId)

        var alertThreadId = intent.getStringExtra(EXTRA_ALERT_ID)
        if (alertThreadId != null)
            mThreadAlert = SharedTimeline.instance.getThread(alertThreadId)

        /**
        mThread = SharedTimeline.instance.getAlertEvent(threadEvent?.senderInfo?.userId)

        if (mThread == null)
            mThread = SharedTimeline.instance.getLocationEvent(threadEvent?.senderInfo?.userId)
        **/

        val readReceiptUpdateCallback = object : ReadReceiptUpdateCallback {
            override fun onReadReceiptsUpdated(readReceipts: MutableList<ReadReceiptData>) {
                mSeenByAdapter.updateList(readReceipts)
            }
        }

        setReadReceipt(mThreadLocation)

        mSeenByAdapter = SeenByAdapter(mutableListOf())
        mBinding.layoutSeenBy.recyclerViewSeenBy.adapter = mSeenByAdapter

        imgAvatar.observe(this, Observer {
            if (imgAvatar.value != null)
            {
                mThreadLocation?.let { it1 -> updateMapPlaces(it1) }
            }

        })



    }

    private fun setReadReceipt (thread: Thread?) {
        // Set read receipt by current user on the parent timeline event of the selected
        // status.
        mCoroutineScope.launch {

            thread?.eventId?.let {

                //Fetch read receipts data from the parent timeline event
                val readReceipts = thread?.events?.firstOrNull()?.readReceipts ?: listOf()
                val readReceiptsData = readReceipts
                    .map {
                        ReadReceiptData(
                            it.roomMember.userId,
                            it.roomMember.avatarUrl,
                            it.roomMember.displayName,
                            it.originServerTs
                        )
                    }
                    .toMutableList()

                //Check if current user already seen the event or not
                var alreadySetReadReceipt = false
                for (readReceipt in readReceiptsData) {
                    if (readReceipt.userId == mSession?.myUserId) {
                        alreadySetReadReceipt = true
                        break
                    }
                }

                //Only set read receipt/read marker if the current user has not ever seen
                //the message yet
                if (!alreadySetReadReceipt) {
                    //  mRoom?.readService()?.setReadReceipt(eventId, ReadService.THREAD_ID_MAIN)
                    // mRoom?.readService()?.setReadMarker(eventId)
                    mRoom?.readService()?.markAsRead()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        SharedTimeline.instance.getThreads().observe(this, this)
    }

    override fun onPause() {
        super.onPause()

        SharedTimeline.instance.getThreads().removeObservers(this)
    }

    private fun loadAvatar (thread: Thread?) {
        mCoroutineScope.launch {

            val mcxUrl = thread?.avatarUrl

            if (mcxUrl != null) {            val urlAvatar = URL(
                ImApp.sImApp?.matrixSession?.contentUrlResolver()?.resolveThumbnail(
                    mcxUrl, 40, 40,
                    ContentUrlResolver.ThumbnailMethod.SCALE
                )
            )

            imgAvatar.postValue(BitmapFactory.decodeStream(urlAvatar.openConnection().getInputStream()))
            }

        }


        mBinding.toolbar.tvTitle.text = thread?.name

        if (mThreadAlert != null)
        {
            mBinding.toolbar.tvTitle.text = getString(R.string.alert_from) + thread?.name
        }

        mBinding.toolbar.tvSubtitle.text = PrettyTime.format(thread?.lastCommentTime?.let { Date(it) }, applicationContext)

    }

    override fun onChanged(value: List<Thread>) {
      //  mThread = value.firstOrNull { it.eventId == mThreadId }

//        Timber.d("StatusDetailActivity onChanged ${mThread?.eventId}")

        loadAvatar(mThreadLocation)

        mIsMe = mThreadLocation?.userId == mSession?.myUserId

        if (mIsMe) {
            mBinding.toolbar.tvResolveStatus.visibility = View.VISIBLE
            mBinding.toolbar.tvResolveStatus.setOnClickListener {

                confirmStopSharing()
            }


            mBinding.btnMarkSeen.visibility = View.GONE
            mBinding.btnMarkSeenLabel.visibility = View.GONE

        }
        else
        {

            if (mThreadAlert?.isResolved == false) {
                mBinding.btnMarkSeen.visibility = View.VISIBLE
                mBinding.btnMarkSeenLabel.visibility = View.VISIBLE

                mBinding.btnMarkSeen.setOnClickListener {

                    mThreadAlert?.eventId?.let { it1 ->
                        SharedTimeline.instance.room?.relationService()?.sendReaction(
                            it1, TAG_REACTION_SEEN
                        )
                    }

                    mThreadAlert?.isUnread = false

                    mBinding.btnMarkSeen.text = getString(R.string.confirmation_sent)
                    mBinding.btnMarkSeen.setBackgroundColor(resources.getColor(R.color.android_green))
                }
            }
            else
            {
                mBinding.btnMarkSeen.visibility = View.GONE
                mBinding.btnMarkSeenLabel.visibility = View.GONE
            }

            updateAudio ()

            if (mThreadAlert?.isUnread == false)
            {
                mBinding.btnMarkSeen.text = getString(R.string.confirmation_sent)
                mBinding.btnMarkSeen.setBackgroundColor(resources.getColor(R.color.android_green))
            }


        }

        if (mThreadLocation?.isLocation == true) {
            mThreadLocation?.let { updateMapPlaces(it) }
            mBinding.map.visibility = View.VISIBLE
            mBinding.tvLocationExpires.visibility = View.VISIBLE
            mBinding.locationExpiresProgressBar.visibility = View.VISIBLE
            mBinding.audioContainer.visibility = View.GONE
        }
        else
        {
            mBinding.map.visibility = View.GONE
            mBinding.tvLocationExpires.visibility = View.GONE
            mBinding.locationExpiresProgressBar.visibility = View.GONE
            mBinding.audioContainer.visibility = View.VISIBLE

        }


/**
        mBinding.tvMessage.text = processStatus(event?.getLastMessageBody())
        mBinding.tvTime.text = PrettyTime.format(Date(ts), this)
        //Sender's name and message time on status's map view
        mBinding.tvMapTime.text = TimeTextUtils.formatMessageInfo(Date(ts), mThread?.name)
            **/

    }

    private fun confirmStopSharing () {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.are_you_sure_you_want_to_stop_sharing_your_location_with_your_circle))
            .setTitle(getString(R.string.stop_location_share))
            .setCancelable(false)
            .setPositiveButton(R.string.yes) { dialog, id ->

                stopLocationSharing();

                resolveAlert()
                redactLocations()

                handler.post {
                    finish()
                }


            }
            .setNegativeButton(R.string.no) { dialog, id ->
                // Dismiss the dialog
                dialog.dismiss()
            }
        val alert = builder.create()
        alert.show()




    }

    private fun stopLocationSharing () {
        val intent = Intent(this, UpdateLocationStatusService::class.java)
        stopService(intent)
    }

    private fun resolveAlert () {
        mThreadAlert?.eventId?.let { it1 ->
            SharedTimeline.instance.room?.relationService()?.sendReaction(
                it1, TAG_RESOLVED)

        }

        val resolveTime = Date().time - (mThreadAlert?.events?.firstOrNull()?.root?.originServerTs ?: 0)
        mApp?.cleanInsightsManager?.measureEvent("status-resolved",
            "resolve-time", resolveTime.toDouble())
    }

    private fun redactLocations () {


        mCoroutineScope.launch {


            mThreadLocation?.events.let {
                if (it != null) {
                    for (event in it) {
                        SharedTimeline.instance.room?.sendService()
                            ?.redactEvent(event.root, "resolved")
                        delay (50)
                    }
                }
            }

        }
    }

    private fun updateMapPlaces (thread:Thread) {

        places = ArrayList<Place>()

        for (eventNow in thread.events) {
            addPlaceFromEvent(eventNow)
        }

        setLocationSharingTimer ()

        refreshMap ()
    }

    private fun setLocationSharingTimer () {
        if (mThreadLocation?.locationSharingExpiration?.compareTo(0) != 0)
        {
            val timeToExpire = mThreadLocation?.locationSharingExpiration?.minus(Date().time)

            val timeSinceStart = mThreadLocation?.timestamp?.let { Date().time?.minus(it) }

            val hoursLeft = timeToExpire?.div(60000L*60L)
            var minutesLeft = timeToExpire?.div(60000L)

            var showProgressBar = true

            if (minutesLeft!! <= 0) {

                mBinding.tvLocationExpires.visibility = View.VISIBLE
                mBinding.tvLocationExpires.text = getString(R.string._0_min_left)
            }
            else if (minutesLeft!! < 60) {
                mBinding.tvLocationExpires.visibility = View.VISIBLE
                mBinding.tvLocationExpires.text =
                    "$minutesLeft " + getString(R.string.minutes_s_left)

                mBinding.locationExpiresProgressBar.progress = (minutesLeft - 60L).toInt()

            }
            else if (minutesLeft!! > (60*24)) {
                mBinding.tvLocationExpires.visibility = View.VISIBLE
                mBinding.tvLocationExpires.text = ""

                mBinding.locationExpiresProgressBar.progress = (minutesLeft - 60L).toInt()

                showProgressBar = false

            }
            else {

                if (hoursLeft != null) {
                    if (minutesLeft != null)
                        minutesLeft -= hoursLeft*60L
                }

                mBinding.tvLocationExpires.visibility = View.VISIBLE
                mBinding.tvLocationExpires.text =
                    "$hoursLeft hours, $minutesLeft min(s) left"

                if (minutesLeft != null) {
                    mBinding.locationExpiresProgressBar.progress = (minutesLeft - 60L).toInt()
                }


            }

            if (showProgressBar) {
                val percentage =
                    100F - ((timeToExpire!!.toDouble() / timeSinceStart!!.toDouble()) * 100F)

                mBinding.locationExpiresProgressBar.progress = percentage?.toInt()!!
                mBinding.locationExpiresProgressBar.visibility = View.VISIBLE
            }
            else
            {
                mBinding.locationExpiresProgressBar.visibility = View.GONE
            }

            Timer().schedule(object : TimerTask() {
                override fun run() {

                    setLocationSharingTimer()

                }
            }, 60000) //wait this long in MS


        }
    }

    private fun updateAudio () {

        if (audioFileUri == null) {
            mCoroutineScope.launch {

                if (mThreadAlert?.events != null) {
                    for (event in mThreadAlert?.events!!) {

                        if (event.getLastMessageContent() is MessageWithAttachmentContent) {


                            var msgAudio =
                                event.getLastMessageContent() as MessageWithAttachmentContent

                            val fileMedia =
                                SharedTimeline.instance.getSession()?.fileService()?.downloadFile(
                                    msgAudio.getFileName(),
                                    msgAudio.mimeType,
                                    msgAudio.getFileUrl(),
                                    msgAudio.encryptedFileInfo?.toElementToDecrypt()
                                )

                            audioFileUri = Uri.fromFile(fileMedia)

                            handler.post {
                                mBinding.audioContainer.visibility = View.VISIBLE
                                var audioWife = AudioWife()
                                var uriAudio = Uri.fromFile(fileMedia)
                                audioWife?.init(applicationContext, uriAudio, msgAudio.mimeType)
                                    ?.useDefaultUi(
                                        mBinding.audioContainer,
                                        LayoutInflater.from(applicationContext)
                                    )
                            }


                            break;
                        }
                    }
                }
            }
        }
    }

    private fun addPlaceFromEvent (event: TimelineEvent?) {

        var lat = 0.0
        var lon = 0.0

        if (event?.getLastMessageContent() is MessageLocationContent)
        {

            val msgLoc = (event.getLastMessageContent() as MessageLocationContent)

            val geoParts = msgLoc.geoUri.split(";")
            var uncertainty = ""
            if (geoParts.size > 1)
                uncertainty = geoParts[1].substring(2)

            val latLon = geoParts[0].substring(4).split(",")
            lat = latLon[0].toDouble()
            lon = latLon[1].toDouble()

            var rootTs = 0L

            if (event.root.originServerTs != null)
                rootTs = event.root.originServerTs!!

            val placeName = "" + PrettyTime.format(
                Date(
                    rootTs
                ),applicationContext)

            /**
            getAddress(lat,lon).let {
            }**/
            Timber.d("place: $lat, $lon")

            val placeLoc : LatLng = LatLng(lat,lon)
            places.add(Place(placeName,placeLoc))

            val sAddress = getAddress(lat, lon)
            mBinding.tvLocation.text = sAddress

            mBinding.btnOpenMap.setOnClickListener {
                val uri = "geo:$lat,$lon?q=$sAddress"
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
            }

        }

    }

    private fun refreshMap () {

        val mapFragment = supportFragmentManager.findFragmentById(
            R.id.map
        ) as? SupportMapFragment
        mapFragment?.getMapAsync { googleMap ->

            if (places.isNotEmpty()) {
                addMarkers(googleMap)

                if (!mIsMapAdjusted) {
                    val bounds = LatLngBounds.builder()
                    places.forEach { bounds.include(it.latLng) }
                    var camBounds = CameraUpdateFactory.newLatLngBounds(bounds.build(), 20)
                    googleMap.moveCamera(camBounds)
                    googleMap.moveCamera(CameraUpdateFactory.zoomBy(-1f))
                    mIsMapAdjusted = true
                }
            }


        }
    }

    data class Place(
        val name: String,
        val latLng: LatLng
    )

    private var places = ArrayList<Place>()

    /**
     * Adds marker representations of the places list on the provided GoogleMap object
     */
    private fun addMarkers(googleMap: GoogleMap) {


        googleMap.clear()

        val lineOptions = PolylineOptions()
        places.forEach { place ->
            lineOptions.add(place.latLng)

            /**
            googleMap.addMarker(
                MarkerOptions()
                    .title(place.name)
                    .position(place.latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            )**/


        }
        lineOptions.geodesic(true)
        lineOptions.width(5f)
        lineOptions.color(Color.BLUE)

        googleMap.addPolyline(lineOptions)


        var lastPlace = places.last()
        googleMap.addMarker(
            MarkerOptions()
                .title(lastPlace.name)
                .position(lastPlace.latLng)
                .icon(imgAvatar.value?.let { BitmapDescriptorFactory.fromBitmap(it) })
        )


    }

    private var mLastStatusTs = -1L

    private fun updateStatusFromString(statusTs: Long?, status: Status?, isUrgent: Boolean) : Boolean {

        if (statusTs != null) {
            if (statusTs < mLastStatusTs)
                return false

            mLastStatusTs = statusTs
        }

        return true
    }

    private fun showResolved() {
        val joinCircleString = getString(R.string.resolve_title)
        val joinCircleMessageString = getString(R.string.resolve_message)
        val okString = getString(R.string.ok)

        InfoSheet().show(this) {
            title(joinCircleString)
            content(joinCircleMessageString)
            style(SheetStyle.BOTTOM_SHEET)
            displayCloseButton()
            onNegative (""){ }
            onPositive (okString) {
                dismiss()
                requireActivity().finish()
            }
            onClose {
            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): String? {
        val geocoder = Geocoder(this)
        val list = geocoder?.getFromLocation(lat, lng, 1)
        list.let {
            if (list?.size!! > 0)
                return list[0]?.getAddressLine(0)
        }

        return null
    }






    private var requestCodeLocation = 9999
    private var requestCodeAudio = 9998




    private fun sendMediaAttachment(mediaUri: Uri, replyId: String) {
        sendAttachment(mediaUri, null, replyId)
    }

    private fun sendAudioStatus(replyId: String) {
        if (this::mAudioFilePath.isInitialized) {
            val uriAudio = Uri.fromFile(mAudioFilePath)
            sendAttachment(uriAudio, org.article19.circulo.next.common.utils.MimeTypes.DEFAULT_AUDIO_FILE, replyId)
        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null, replyId: String) {
        val room = mRoom ?: return

        try {
            val mimeType = fallbackMimeType
                ?: MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString()))

            val attachment =
                AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

            room.sendService().sendMedia(
                attachment,
                true,
                setOf(room.roomId),
                replyId,
                RelationDefaultContent(null,null, ReplyToContent(replyId))
            )

        }
        catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }

    /**
    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RoomActivity.REQUEST_ADD_MEDIA) {
            if (resultCode != RESULT_OK) return

            for (mediaUri in Matisse.obtainResult(data)) {
                mThreadDefault?.eventId?.let { eventId -> sendMediaAttachment(mediaUri, eventId) }
            }
        }
        else {
            // Need to keep this for the Matisse library, which didn't get an overhaul in the
            // last 1.5 years. Grml.
            @Suppress("DEPRECATION")
            super.onActivityResult(requestCode, resultCode, data)
        }
    }**/


    @Suppress("SameParameterValue")
    private fun insertMatrixItem(editText: EditText, editable: Editable, firstChar: String, matrixItem: MatrixItem) {
        // Detect last firstChar and remove it.
        val startIndex = max(editable.lastIndexOf(firstChar), 0)

        // Detect next word separator.
        var endIndex = editable.indexOf(" ", startIndex)
        if (endIndex < 0) endIndex = editable.length

        // Replace the word by its completion.
        val displayName = matrixItem.displayName ?: ""

        // Adding trailing space " " or ": " if the user started mention someone.
        val displayNameSuffix = if (firstChar == "@" && startIndex == 0) ": " else " "

        editable.replace(startIndex, endIndex, "$displayName$displayNameSuffix")

        // Add the span.
        val span = PillImageSpan(editText.context, displayName, matrixItem)
        span.bind(editText)

        editable.setSpan(span, startIndex, startIndex + displayName.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    fun sendShareRequest(request: ShareRequest) {
        sendAttachment(request.media, request.mimeType)
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null) {
        val room = mRoom ?: return

        val mimeType = fallbackMimeType ?: MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                MimeTypeMap.getFileExtensionFromUrl(contentUri.toString()))

        val attachment = AttachmentHelper.createFromContentUri(contentResolver, contentUri, mimeType)

        room.sendService().sendMedia(attachment, true, setOf(room.roomId), null)
    }


    interface ReadReceiptUpdateCallback {
        fun onReadReceiptsUpdated(readReceipts: MutableList<ReadReceiptData>)
    }
}
