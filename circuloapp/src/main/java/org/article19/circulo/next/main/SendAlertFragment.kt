package org.article19.circulo.next.main


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.KeanuConstants.LOG_TAG
import info.guardianproject.keanu.core.util.AttachmentHelper
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.utils.MimeTypes
import org.article19.circulo.next.databinding.FragmentSendAlertBinding
import org.article19.circulo.next.main.now.Status.Companion.NOTSAFE_STRING
import org.article19.circulo.next.main.now.Status.Companion.URGENT_STRING
import org.article19.circulo.next.main.updatestatus.UpdateLocationStatusService
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Timer
import java.util.TimerTask
import org.article19.circulo.next.main.now.Thread
import org.matrix.android.sdk.api.session.events.model.RelationType
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import java.util.Date


class SendAlertFragment (val motionEvent: MotionEvent) : Fragment(), OnTouchListener, Observer<List<Thread>> {

    private var recorder: MediaRecorder? = null
    private var fileAudio: String? = null

    private var recordTime = 1000L;
    private var requestCodeAudio = 9998

    private lateinit var mBinding: FragmentSendAlertBinding

    private  val timer = Timer();

    companion object {
        const val TAG = "Alert"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mBinding = FragmentSendAlertBinding.inflate(inflater, container, false)
        mBinding.alertButton?.setOnTouchListener(this)
        mBinding.alertButton?.dispatchTouchEvent(motionEvent)
        fileAudio = File.createTempFile("alert","m4a").absolutePath

        return mBinding.root
    }

    override fun onResume() {
        super.onResume()

        if(ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(requireActivity(),arrayOf(Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else {
            if (recorder == null)
                startRecording()
        }


    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            requestCodeAudio -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    startRecording()
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the feature requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(fileAudio)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            start()
        }

        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                recordTime+=1000
                updateTimer()
            }
        }, 0, 1000)

    }
    private fun updateTimer() {
        activity?.runOnUiThread {
            mBinding.textAudioTimer.text = "${SimpleDateFormat("mm:ss").format(recordTime)}"
        }
    }


    private fun stopRecording() {
        timer.cancel()
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {


        val actualY = event.rawY-50

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
             //   Toast.makeText(this,"touch down",Toast.LENGTH_SHORT).show()
            }

            MotionEvent.ACTION_MOVE -> {
                mBinding.alertButton.y = actualY
            }

            MotionEvent.ACTION_UP -> {

                stopRecording()


                if (event.rawY > mBinding.alertDelete.y)
                {
                    fileAudio?.let { File(it).delete() }

                }
                else
                {
                    sendAlert()

                }
            }

            else -> return false
        }

        return true
    }

    private fun closeAlert () {

        (requireActivity() as MainActivity).removeAlertFragment()
    }

    private fun sendAlert () {

        SharedTimeline.instance.getThreads().observe(this, this)

        SharedTimeline.instance.room?.sendService()
            ?.sendTextMessage("$URGENT_STRING $NOTSAFE_STRING")




    }
    private fun startLocationShare () {

        val timeTotal = 60*4; //4 hours
        val intent = Intent(context, UpdateLocationStatusService::class.java)
        intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_URGENT,true)
        intent.putExtra(UpdateLocationStatusService.EXTRA_TOTAL_TIME,timeTotal)
        intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_LOCATION,true)
        ContextCompat.startForegroundService(requireActivity().applicationContext,intent)
    }

    var alertSent = false

    override fun onChanged(value: List<Thread>) {

        for (thread in value) {

            val event = thread.events.firstOrNull() ?: continue

            val eventId = event.eventId
            if (eventId.contains("local")) continue

            val mc = event.getLastMessageContent() as? MessageTextContent ?: continue

            val senderIsMe = event.senderInfo.userId == SharedTimeline.instance.myUserId

            if (event.root.originServerTs != null) {
                val timeIsNow = Date().time - event.root.originServerTs!! < 1000 * 60

                if (senderIsMe && timeIsNow && mc.body?.startsWith(URGENT_STRING) == true) {

                    synchronized(mc)
                    {
                        if (!alertSent) {

                            SharedTimeline.instance.getThreads().removeObservers(this)

                            var fileAudio = File(fileAudio)
                            if (fileAudio != null && fileAudio.exists()) {
                                val uriAudio = Uri.fromFile(fileAudio)
                                sendAttachment(uriAudio, MimeTypes.DEFAULT_AUDIO_FILE, eventId)
                            }

                            startLocationShare()

                            (requireActivity() as MainActivity).showAlertControlFragment()

                            alertSent = true
                        }
                    }

                    closeAlert()
                    break
                }
            }

        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null, replyId: String) {
        try {
            var mimeType = fallbackMimeType
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString())
                )
            }

            val attachment =
                AttachmentHelper.createFromContentUri(requireActivity().contentResolver, contentUri, mimeType)

            //    public abstract fun sendMedia(attachment: org.matrix.android.sdk.api.session.content.ContentAttachmentData,
            //    compressBeforeSending: kotlin.Boolean,
            //    roomIds: kotlin.collections.Set<kotlin.String>,
            //    rootThreadEventId: kotlin.String? /* = compiled code */,
            //    relatesTo: org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent? /* = compiled code */,
            //    additionalContent: org.matrix.android.sdk.api.session.events.model.Content? /* = kotlin.collections.Map<kotlin.String, @kotlin.jvm.JvmSuppressWildcards kotlin.Any>? */ /* = compiled code */): org.matrix.android.sdk.api.util.Cancelable

            val room = SharedTimeline.instance.room

            var rdc = RelationDefaultContent(
                type = RelationType.THREAD,
                eventId = replyId,
                isFallingBack = true,
                // False when is a rich reply from within a thread, and true when is a reply that should be visible from threads
                inReplyTo = ReplyToContent(eventId = replyId)
            )

            room?.sendService()?.sendMedia(
                attachment,
                true,
                setOf(room.roomId),
                replyId,
                rdc
            )

        } catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }




}