package org.article19.circulo.next.settings

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.Preferences
import info.guardianproject.keanuapp.ui.contacts.DevicesActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.SwitchButton
import org.article19.circulo.next.databinding.FragmentSettingSecurityPrivacyBinding
import org.article19.circulo.next.main.MainActivity

open class SettingSecurityPrivacyFragment : SettingBaseFragment() {

    private lateinit var mViewBinding: FragmentSettingSecurityPrivacyBinding

    val mApp: ImApp? = activity?.application as? ImApp
    private val BLOCK_SCREENSHOTS = "prefBlockScreenshots"
    private lateinit var mHostActivity : MainActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mViewBinding = FragmentSettingSecurityPrivacyBinding.inflate(inflater, container, false)
        setupNavigation(mViewBinding.toolbar.tvBackText, mViewBinding.toolbar.tvTitle,
            getString(R.string.security_privacy),
            savedInstanceState
        )
        mHostActivity = activity as MainActivity

        val sbBlockScreenshot = mViewBinding.swBlockScreenshot
        sbBlockScreenshot.isChecked = Preferences.doBlockScreenshots()

        sbBlockScreenshot.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                val prefs = PreferenceManager.getDefaultSharedPreferences(requireActivity())
                prefs.edit()
                    .putBoolean(BLOCK_SCREENSHOTS, isChecked).apply()
            }
        })

        mViewBinding.layoutMySessions.setOnClickListener {

            val i = Intent(context, DevicesActivity::class.java)
            i.putExtra(DevicesActivity.EXTRA_USER_ID, mApp?.matrixSession?.myUserId)
            mHostActivity.startActivity(i)

        }

        return mViewBinding.root
    }
}