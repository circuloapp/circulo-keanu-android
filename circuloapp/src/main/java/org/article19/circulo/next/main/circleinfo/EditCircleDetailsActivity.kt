package org.article19.circulo.next.main.circleinfo

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import info.guardianproject.keanu.core.util.extensions.betterMessage
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ActivityEditCircleDetailsBinding
import org.article19.circulo.next.main.circleinfo.viewmodel.CircleInfoViewModel

class EditCircleDetailsActivity : BaseActivity() {

    private lateinit var mBinding: ActivityEditCircleDetailsBinding
    private lateinit var mCircleName: String
    private lateinit var mCircleDescription: String
    private lateinit var mCircleInfoViewModel: CircleInfoViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCircleInfoViewModel = ViewModelProvider(this)[CircleInfoViewModel::class.java]
        mBinding = ActivityEditCircleDetailsBinding.inflate(layoutInflater)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR

        setContentView(mBinding.root)

        mBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }
        mBinding.toolbar.tvTitle.text = getString(R.string.edit_details)


        mCircleName = intent.getStringExtra(CircleInfoActivity.BUNDLE_KEY_CIRCLE_NAME) ?: ""
        mCircleDescription = intent.getStringExtra(CircleInfoActivity.BUNDLE_KEY_CIRCLE_TOPIC) ?: ""
        val roomId = intent.getStringExtra(CircleInfoActivity.BUNDLE_KEY_ROOM_ID) ?: ""

        mBinding.toolbar.tvAction.visibility = View.VISIBLE
        mBinding.toolbar.tvAction.apply {
            visibility = View.VISIBLE
            text = getString(R.string.save)
            isEnabled = false
            setOnClickListener {
                showLoading()
                mCircleInfoViewModel.updateCircleDetails(roomId,
                    mBinding.tvCircleName.text.toString(),
                    mBinding.tvCircleDescription.text.toString())
            }
        }

        mBinding.tvCircleName.apply {
            setText(mCircleName)
            doOnTextChanged { text, _, _, _ ->
                val newName = text.toString()
                val currentDescription = mBinding.tvCircleDescription.text.toString()
                val shouldEnable = (mCircleName != newName || mCircleDescription != currentDescription)
                        && newName.isNotEmpty()

                enableActionText(shouldEnable)
            }
        }
        mBinding.tvCircleDescription.apply {
            setText(mCircleDescription)
            doOnTextChanged { text, _, _, _ ->
                val newDescription = text.toString()
                val currentName = mBinding.tvCircleName.text.toString()
                val shouldEnable = (mCircleName != currentName || mCircleDescription != newDescription)
                        && currentName.isNotEmpty()

                enableActionText(shouldEnable)
            }
        }
        subscribeLiveData()
    }

    private fun enableActionText(enable: Boolean) {
        mBinding.toolbar.tvAction.isEnabled = enable
    }

    private fun subscribeLiveData() {
        mCircleInfoViewModel.observableEditDetailsLiveData.observe(this) { isSuccess ->
            if (isSuccess) {
                setResult(RESULT_OK, Intent().apply {
                    putExtra(CircleInfoActivity.BUNDLE_KEY_CIRCLE_NAME, mBinding.tvCircleName.text.toString())
                    putExtra(CircleInfoActivity.BUNDLE_KEY_CIRCLE_TOPIC, mBinding.tvCircleDescription.text.toString())
                })
                this@EditCircleDetailsActivity.finish()
            }
            hideLoading()
        }

        mCircleInfoViewModel.observableErrorLiveData.observe(this) {
            Snackbar.make(mBinding.root, it.betterMessage, Snackbar.LENGTH_LONG).show()
            hideLoading()
        }
    }
}