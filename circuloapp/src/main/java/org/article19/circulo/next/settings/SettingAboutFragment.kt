package org.article19.circulo.next.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.article19.circulo.next.BuildConfig
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.FragmentSettingAboutBinding

open class SettingAboutFragment : SettingBaseFragment() {

    private lateinit var mViewBinding: FragmentSettingAboutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mViewBinding = FragmentSettingAboutBinding.inflate(inflater, container, false)
        setupNavigation(mViewBinding.toolbar.tvBackText, mViewBinding.toolbar.tvTitle,
            getString(R.string.about),
            savedInstanceState
        )

        mViewBinding.viewResourcesRow.setOnClickListener {
            openResourcesPage()
        }

        mViewBinding.supportRow.setOnClickListener {
            openSupportEmail()
        }


        mViewBinding.tvStateVersion.text = BuildConfig.VERSION_NAME

        return mViewBinding.root
    }

    private fun openResourcesPage () {
        val urlResources = "https://encirculo.org/en/community-resources/"
        openPage(urlResources)
    }

    private fun openSupportEmail () {
        val supportEmail = "support@guardianproject.info"
        sendEmail(supportEmail,getString(R.string.app_name),"")

    }

    private fun openPage(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    private fun sendEmail(recipient: String, subject: String, message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)

        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "..."))
        }
        catch (e: Exception){
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
         //   Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }

    }
}