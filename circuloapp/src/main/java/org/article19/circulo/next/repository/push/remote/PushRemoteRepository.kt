package org.article19.circulo.next.repository.push.remote

interface PushRemoteRepository {
    suspend fun registerPushToken(token: String)
}