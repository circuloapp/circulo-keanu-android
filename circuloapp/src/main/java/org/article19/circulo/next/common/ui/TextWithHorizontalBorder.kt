package org.article19.circulo.next.common.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.TextWithHorizontalBorderBinding

class TextWithHorizontalBorder @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): ConstraintLayout (context, attrs, defStyleAttr) {

    private var mContext: Context? = null
    private lateinit var mViewTopSeparator: View
    private lateinit var mTvContent: AppCompatTextView
    private lateinit var mViewBottomSeparator: View

    init {
        init(context, attrs, defStyleAttr)
        mContext = context
    }

    fun init(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) {

        val binding = TextWithHorizontalBorderBinding.inflate(LayoutInflater.from(context), this, true)

        mTvContent = binding.tvContent
        mViewTopSeparator = binding.viewSeparatorTop
        mViewBottomSeparator = binding.viewSeparatorBottom

        val array = context.obtainStyledAttributes(attrs, R.styleable.TextWithHorizontalBorder)
        val text = array.getString(R.styleable.TextWithHorizontalBorder_text)
        val showTopBorder = array.getBoolean(R.styleable.TextWithHorizontalBorder_showTopBorder, true)
        val showBottomBorder = array.getBoolean(R.styleable.TextWithHorizontalBorder_showBottomBorder, true)
        array.recycle()

        mTvContent.text = text
        mViewTopSeparator.visibility = if (showTopBorder) View.VISIBLE else View.GONE
        mViewBottomSeparator.visibility = if (showBottomBorder) View.VISIBLE else View.GONE

    }

    fun setText(text: String) {
        mTvContent.text = text
    }

}