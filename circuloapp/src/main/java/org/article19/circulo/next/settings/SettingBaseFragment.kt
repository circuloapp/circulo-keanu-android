package org.article19.circulo.next.settings

import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.article19.circulo.next.R
import org.article19.circulo.next.main.MainActivity

open class SettingBaseFragment : Fragment() {

    val coroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

//    private val mResultBundleValues = mutableMapOf<String, Any?>()

    fun setupNavigation(toolbarBackText: TextView,
                        toolbarTitleText: TextView,
                        toolbarTitle: String,
                        savedInstanceState: Bundle?) {
        var mainActivity: MainActivity? = null

        activity?.let {
            if (activity is MainActivity) {
                mainActivity = activity as MainActivity
            }
        }
        if (savedInstanceState == null) {
            mainActivity?.hideNavigationView()
        }

        toolbarBackText.text = getString(R.string.profile)
        toolbarBackText.setOnClickListener {
            mainActivity?.showProfileFragment(false)
            mainActivity?.showNavigationView()
        }

        toolbarTitleText.text = toolbarTitle
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(MainActivity.KEY_LAST_FRAGMENT_INDEX, (activity as MainActivity).getCurrentSelectedPos())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //Restore the fragment's state here
        if (savedInstanceState != null) {
            val lastFragmentIndex = savedInstanceState.getInt(MainActivity.KEY_LAST_FRAGMENT_INDEX)
            if (lastFragmentIndex == -1) {
                (activity as MainActivity).setCurrentFragment(this)
            }
        }
    }

}