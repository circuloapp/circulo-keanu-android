package org.article19.circulo.next.main.circleinfo

import android.content.Context
import android.net.Uri
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.RowCircleMemberBinding
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.room.powerlevels.Role
import java.util.Date

class CircleMemberAdapter(private val memberList: MutableList<CircleMember>) :
    RecyclerView.Adapter<CircleMemberAdapter.ViewHolder>() {

    private lateinit var context: Context
    private var mOnMemberClickListener: MemberClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = RowCircleMemberBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentMember = memberList[position]
        holder.tvName.text = currentMember.name

        if (currentMember.lastActiveAgo != null) {
            if (currentMember.lastActiveAgo < 1000 * 60) //less than five minutes
            {
                holder.tvLastActiveTime.text = context.getString(R.string.status_online)
            }
            else {
                val lastActiveDate = Date(Date().time - currentMember.lastActiveAgo)
                holder.tvLastActiveTime.text = PrettyTime.format(lastActiveDate)
            }
        }

        if (TextUtils.isEmpty(currentMember.avatarUrl)) {
            holder.ivAvatar.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_avatar_chloe))
        } else {
            if (context is CircleInfoActivity) {
                val parentActivity = context as CircleInfoActivity
                parentActivity.mCoroutineScope.launch {
                    parentActivity.session?.contentUrlResolver()?.resolveThumbnail(currentMember.avatarUrl, 59, 59, ContentUrlResolver.ThumbnailMethod.SCALE)
                        ?.let {
                            withContext(Dispatchers.Main) {
                                if (!parentActivity.isDestroyed) {
                                    GlideUtils.loadImageFromUri(context, Uri.parse(it), holder.ivAvatar, true)
                                }
                            }
                        }
                }
            }
        }

        if (currentMember.role == Role.Admin.value || currentMember.role == Role.Moderator.value) {
            holder.ivAdminBadge.visibility = View.VISIBLE
        } else {
            holder.ivAdminBadge.visibility = View.GONE
        }

        holder.rootView.setOnClickListener {
            mOnMemberClickListener?.onMemberClicked(currentMember)
        }
    }

    override fun getItemCount(): Int {
        return memberList.size
    }

    fun updateMemberList(members: List<CircleMember>) {
        memberList.clear()
        memberList.addAll(members)
        notifyDataSetChanged()
    }

    class ViewHolder(binding: RowCircleMemberBinding) : RecyclerView.ViewHolder(binding.root) {
        val ivAvatar = binding.ivAvatar
        val ivAdminBadge = binding.ivAdminBadge
        val tvName = binding.tvName
        val tvLastActiveTime = binding.tvLastActiveTime
        val rootView = binding.root
    }

    fun setOnMemberClickListener(listener: MemberClickListener) {
        mOnMemberClickListener = listener
    }

    interface MemberClickListener {
        fun onMemberClicked(member: CircleMember)
    }
}