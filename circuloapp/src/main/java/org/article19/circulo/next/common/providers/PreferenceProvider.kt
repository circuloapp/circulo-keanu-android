package org.article19.circulo.next.common.providers

import android.content.SharedPreferences
import android.net.Uri
import androidx.preference.PreferenceManager
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.settings.NotificationType

object PreferenceProvider {

    val appPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(CirculoApp.getInstance().applicationContext)
    }

    private const val PREF_KEY_ENABLE_DISGUISE = "pref_key_enable_disguise"
    private const val PREF_KEY_DISGUISE_ICON_ID = "pref_key_disguise_icon_id"

    private const val PREF_KEY_ENABLE_NOTIFICATION = "pref_key_enable_notification"
    private const val PREF_KEY_NOTIFICATION_NEW_STATUS = "pref_key_notification_new_status"
    private const val PREF_KEY_NOTIFICATION_RESPONSE = "pref_key_notification_response"

    private const val PREF_KEY_NOTIFICATION_SOUND_URGENT = "pref_key_notification_sound_urgent"
    private const val PREF_KEY_NOTIFICATION_VIBRATE_URGENT = "pref_key_notification_vibrate_urgent"
    private const val PREF_KEY_NOTIFICATION_RINGTONE_URGENT = "pref_key_notification_ringtone_urgent"

    private const val PREF_KEY_CI_GROUP_CODE = "pref_key_ci_group_code"

    var enableDisguise: Boolean
    get() = appPreferences.getBoolean(PREF_KEY_ENABLE_DISGUISE, false)
    set(value) = appPreferences.edit().putBoolean(PREF_KEY_ENABLE_DISGUISE, value).apply()

    var disguiseId: Int
    get() = appPreferences.getInt(PREF_KEY_DISGUISE_ICON_ID, 0)
    set(value) = appPreferences.edit().putInt(PREF_KEY_DISGUISE_ICON_ID, value).apply()

    var enableNotification: Boolean
    get() = appPreferences.getBoolean(PREF_KEY_ENABLE_NOTIFICATION, true)
    set(value) = appPreferences.edit().putBoolean(PREF_KEY_ENABLE_NOTIFICATION, value).apply()

    var notificationNewStatus: Int
    get() = appPreferences.getInt(PREF_KEY_NOTIFICATION_NEW_STATUS, NotificationType.NOTIFICATION_NEW_STATUS_ANYTIME)
    set(value) = appPreferences.edit().putInt(PREF_KEY_NOTIFICATION_NEW_STATUS, value).apply()

    var notificationResponse: Int
    get() = appPreferences.getInt(PREF_KEY_NOTIFICATION_RESPONSE, NotificationType.NOTIFICATION_RESPONSE_ANYTIME)
    set(value) = appPreferences.edit().putInt(PREF_KEY_NOTIFICATION_RESPONSE, value).apply()

    var notificationVibrateUrgent: Boolean
    get() = appPreferences.getBoolean(PREF_KEY_NOTIFICATION_VIBRATE_URGENT, true)
    set(value) = appPreferences.edit().putBoolean(PREF_KEY_NOTIFICATION_VIBRATE_URGENT, value).apply()

    var notificationSoundUrgent: Boolean
    get() = appPreferences.getBoolean(PREF_KEY_NOTIFICATION_SOUND_URGENT, true)
    set(value) = appPreferences.edit().putBoolean(PREF_KEY_NOTIFICATION_SOUND_URGENT, value).apply()

    var notificationRingtoneUrgent: Uri
    get() = Uri.parse(appPreferences.getString(PREF_KEY_NOTIFICATION_RINGTONE_URGENT, ""))
    set(value) = appPreferences.edit().putString(PREF_KEY_NOTIFICATION_RINGTONE_URGENT, value.toString()).apply()

    var ciGroupCode: String?
    get() = appPreferences.getString(PREF_KEY_CI_GROUP_CODE, null)
    set(value) = appPreferences.edit().putString(PREF_KEY_CI_GROUP_CODE, value).apply()
}