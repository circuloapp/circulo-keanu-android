package org.article19.circulo.next.main.updatestatus

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReadReceiptData(
    val userId: String,
    val avatarUrl: String?,
    val displayName: String?,
    val timestamp: Long
) : Parcelable
