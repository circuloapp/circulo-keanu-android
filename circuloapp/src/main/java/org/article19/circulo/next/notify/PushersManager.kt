/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.article19.circulo.next.notify

import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.pushers.HttpPusher
import org.matrix.android.sdk.api.session.pushers.Pusher
import java.util.UUID
import javax.inject.Inject
import kotlin.math.abs

internal const val DEFAULT_PUSHER_FILE_TAG = "mobile"

class PushersManager @Inject constructor(
    private val pusherAppId: String,
    private val pushGatewayUrl: String,
    private val currentSession: Session,
    private val deviceDisplayName: String,
    private val deviceId: String
) {
    suspend fun testPush(pushGatewayUrl: String, pushToken: String) {
        currentSession.pushersService().testPush(
            pushGatewayUrl ?: return,
                pusherAppId,
            pushToken,
                TEST_EVENT_ID
        )

    }

    suspend fun enqueueRegisterPusherWithFcmKey(pushKey: String): UUID {
        return enqueueRegisterPusher(
                pushKey = pushKey,
                gateway = pushGatewayUrl
        )
    }

    suspend fun enqueueRegisterPusher(
            pushKey: String,
            gateway: String
    ): UUID {
        val pusher = createHttpPusher(pushKey, gateway)
        return currentSession.pushersService().enqueueAddHttpPusher(pusher)
    }

    private suspend fun createHttpPusher(
            pushKey: String,
            gateway: String
    ) = HttpPusher(
            pushkey = pushKey,
            appId = pusherAppId,
            profileTag = DEFAULT_PUSHER_FILE_TAG + "_" + abs(currentSession.myUserId.hashCode()),
            lang = "en",
            appDisplayName = "Circulo",
            deviceDisplayName = deviceDisplayName,
            url = gateway,
            enabled = true,
            deviceId = deviceId,
            append = false,
            withEventIdOnly = true,
    )

   fun getPusherForCurrentSession(): Pusher? {
        val deviceId = currentSession.sessionParams.deviceId
        return currentSession.pushersService().getPushers().firstOrNull { it.deviceId == deviceId }
    }

    suspend fun unregisterPusher(pushKey: String) {
        currentSession.pushersService().removeHttpPusher(pushKey, pusherAppId)
    }

    companion object {
        const val TEST_EVENT_ID = "\$THIS_IS_A_FAKE_EVENT_ID"
    }
}
