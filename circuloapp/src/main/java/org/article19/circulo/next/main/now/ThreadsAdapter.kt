package org.article19.circulo.next.main.now

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.PrettyTime
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.internal.notify
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.ui.RoundRectCornerImageView
import org.article19.circulo.next.databinding.AlertPeopleRowItemBinding
import org.article19.circulo.next.databinding.PeopleRowItemBinding
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.updatestatus.LocationDetailActivity
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.crypto.keysbackup.extractCurveKeyFromRecoveryKey
import org.matrix.android.sdk.api.session.room.model.message.MessageLocationContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import java.net.URL
import java.util.Date
import java.util.Timer
import java.util.TimerTask


class ThreadsAdapter(private var activity: MainActivity, threads: List<Thread>) :
    ListAdapter<Thread, ThreadsAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        const val BUNDLE_EXTRA_THREAD = "bundle_extra_thread"

        private const val AVATAR_SIZE = 59

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Thread>() {

            override fun areItemsTheSame(oldItem: Thread, newItem: Thread): Boolean {
                return oldItem.eventId == newItem.eventId
            }

            override fun areContentsTheSame(oldItem: Thread, newItem: Thread): Boolean {
                return oldItem == newItem
            }
        }
    }


    init {
        setHasStableIds(true)

        submitList(threads)
    }

    private val mSession: Session?
        get() = (activity.application as? ImApp)?.matrixSession

    private lateinit var mContext: Context
    private lateinit var mSafeString: String
    private lateinit var mUnsafeString: String
    private lateinit var mUncertainString: String

    private var mHandler = Handler()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    override fun getItemViewType(position: Int): Int {

        val currentPersonStatus = currentList[position]
        if (currentPersonStatus.isUrgent == true)
            return 1;
        else
            return 0;

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        if (viewType == 1) {
            val binding = AlertPeopleRowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            mContext = parent.context

            mSafeString = mContext.getString(R.string.safe)
            mUnsafeString = mContext.getString(R.string.not_safe)
            mUncertainString = mContext.getString(R.string.uncertain)

            return AlertPeopleViewHolder(binding)
        }
        else {
            val binding = PeopleRowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            mContext = parent.context

            mSafeString = mContext.getString(R.string.safe)
            mUnsafeString = mContext.getString(R.string.not_safe)
            mUncertainString = mContext.getString(R.string.uncertain)

            binding.map.onCreate(null)

            return PeopleViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentPersonStatus = currentList[position]

        if (holder is AlertPeopleViewHolder) {

            holder.tvName.text = currentPersonStatus.name
            /**
            holder.tvCommentCount.text = mContext.resources.getQuantityString(
                R.plurals.comment_counts,
                currentPersonStatus.commentCount ?: 0,
                currentPersonStatus.commentCount
            )**/

            holder.ivAvatar.visibility = View.VISIBLE
            holder.tvUnread.visibility = View.GONE
            holder.ivUrgent.visibility = View.GONE

            /**
            holder.tvLastCommentTime.text = context.resources.getString(
            R.string.last_comment_time,
            currentPersonStatus.lastCommentTime
            )**/


            //sometimes the server time is ahead a few seconds, so take of 5 seconds so it doesn't appear in the future
            var lastCommentTime =
                currentPersonStatus.lastCommentTime?.let { it }
            holder.tvLastCommentTime.text = PrettyTime.format(lastCommentTime?.let { Date(it) })

            holder.rootLayout.setOnClickListener {


                openLocationDetail(currentPersonStatus.userId)

            }

            if (currentPersonStatus.isResolved == true)
                holder.cardLayout.setBackgroundColor(mContext.resources.getColor(R.color.holo_green_dark))
            else
                holder.cardLayout.setBackgroundColor(mContext.resources.getColor(R.color.holo_red_dark))

            holder.btnShow.setOnClickListener {
                openLocationDetail(currentPersonStatus.userId)
            }

        }
        else if (holder is PeopleViewHolder) {

            var isMe = currentPersonStatus.userId == SharedTimeline.instance.myUserId
            /**
            holder.tvCommentCount.text = mContext.resources.getQuantityString(
                R.plurals.comment_counts,
                currentPersonStatus.commentCount ?: 0,
                currentPersonStatus.commentCount
            )**/

            val mainActivity = activity as? MainActivity ?: return

            if (isMe)
            {
                holder.ivAvatar.visibility = View.GONE
            }
            else {
                holder.tvName.text = currentPersonStatus.name

                loadUserAvatar(currentPersonStatus.avatarUrl, holder.ivAvatar)
                if (currentPersonStatus.isOnline == true) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.ivAvatar.foreground = null
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        holder.ivAvatar.foreground =
                            ContextCompat.getDrawable(mainActivity, R.color.grey_30)
                    }
                }
            }

            holder.tvUnread.visibility = View.GONE

            if (currentPersonStatus.isUrgent == true) {
                holder.ivUrgent.visibility = View.VISIBLE
            } else {
                holder.ivUrgent.visibility = View.GONE
            }

            if (currentPersonStatus?.locationSharingExpiration?.compareTo(0) != 0)
            {
                val timeToExpire = currentPersonStatus?.locationSharingExpiration?.minus(Date().time)

                val timeSinceStart = currentPersonStatus.timestamp?.let { Date().time?.minus(it) }

                val hoursLeft = timeToExpire?.div(60000L*60L)
                var minutesLeft = timeToExpire?.div(60000L)

                var showProgressBar = true

                if (minutesLeft!! <= 0) {
                    holder.tvLastCommentTime.visibility = View.VISIBLE
                    holder.tvLastCommentTime.text = mContext.getString(R.string._0_min_left)
                }
                else if (minutesLeft!! < 60) {
                    holder.tvLastCommentTime.visibility = View.VISIBLE
                    holder.tvLastCommentTime.text =
                        "$minutesLeft " + mContext.getString(R.string.minutes_s_left)

                    holder.pbLocationExpires.progress = (minutesLeft - 60L).toInt()

                }
                else if (minutesLeft!! > (60*24)) {
                    holder.tvLastCommentTime.visibility = View.VISIBLE
                    holder.tvLastCommentTime.text = ""

                    holder.pbLocationExpires.progress = (minutesLeft - 60L).toInt()

                    showProgressBar = false

                }
                else {

                    if (hoursLeft != null) {
                        if (minutesLeft != null)
                            minutesLeft -= hoursLeft*60L
                    }

                    holder.tvLastCommentTime.visibility = View.VISIBLE
                    holder.tvLastCommentTime.text =
                        "$hoursLeft hours, $minutesLeft min(s) left"

                    if (minutesLeft != null) {
                        holder.pbLocationExpires.progress = (minutesLeft - 60L).toInt()
                    }


                }

                if (showProgressBar) {
                    val percentage =
                        100F - ((timeToExpire!!.toDouble() / timeSinceStart!!.toDouble()) * 100F)
                    holder.pbLocationExpires.progress = percentage?.toInt()!!
                    holder.pbLocationExpires.visibility = View.VISIBLE
                }
                else
                {
                    holder.pbLocationExpires.visibility = View.GONE
                }

                Timer().schedule(object : TimerTask() {
                    override fun run() {

                        mHandler.post {
                            notifyDataSetChanged()
                        }

                    }
                }, 60000) //wait this long in MS



            }
            else
            {
                holder.pbLocationExpires.visibility = View.GONE
            }

            //sometimes the server time is ahead a few seconds, so take of 5 seconds so it doesn't appear in the future
            /**
            var lastCommentTime =
                currentPersonStatus.lastCommentTime?.let { it }
            holder.tvLastCommentTime.text = PrettyTime.format(lastCommentTime?.let { Date(it) })
            **/

            when (currentPersonStatus.status) {
                Status.SAFE -> {
                    holder.tvStatus.text = mSafeString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_safe),
                        null,
                        null,
                        null
                    )
                }

                Status.NOT_SAFE -> {
                    holder.tvStatus.text = mUnsafeString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_not_safe),
                        null,
                        null,
                        null
                    )
                }

                Status.UNCERTAIN -> {
                    holder.tvStatus.text = mUncertainString
                    holder.tvStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        AppCompatResources.getDrawable(mContext, R.drawable.ic_status_uncertain),
                        null,
                        null,
                        null
                    )
                }

                else -> {



                    if (currentPersonStatus.msgBody?.contains("geo:")==true) {

                        if (currentPersonStatus.events.isNotEmpty() && currentPersonStatus.events.last().getLastMessageContent() is MessageLocationContent) {
                            val lastEvent: TimelineEvent = currentPersonStatus.events.last()
                            if (lastEvent.getLastMessageContent() is MessageLocationContent) {

                                mCoroutineScope.launch {
                                    val geoParts =
                                        (lastEvent.getLastMessageContent() as MessageLocationContent).geoUri.split(
                                            ";"
                                        )

                                    val latLon = geoParts[0].substring(4).split(",")
                                    val address = getAddress(latLon[0].toDouble(), latLon[1].toDouble())
                                    mHandler.post {
                                        holder.tvName.text = address

                                    }
                                }
                            }
                        }
                        else {
                            mCoroutineScope.launch {
                                val geoString = currentPersonStatus.msgBody!!.substring(
                                    currentPersonStatus.msgBody!!.indexOf("geo:") + 4
                                ).split(" ").get(0)
                                val geoParts = geoString.split(";")
                                val latLon = geoParts[0].split(",")
                                val address = getAddress(latLon[0].toDouble(), latLon[1].toDouble())
                                mHandler.post {
                                    holder.tvName.text = address

                                }
                            }
                        }

                        holder.tvStatus.visibility = View.VISIBLE
                        holder.tvStatus.text = currentPersonStatus.name

                    }
                    else
                    {
                        holder.tvName.text = ""
                        holder.tvStatus.visibility = View.GONE
                    }

                    if (isMe)
                    {
                        holder.tvStatus.text = ""
                        holder.tvStatus.visibility = View.GONE
                        holder.ivImageMap.visibility = View.VISIBLE
                        holder.btnNext.visibility = View.VISIBLE
                        holder.map.visibility = View.GONE
                    }
                    else
                    {
                        holder.ivImageMap.visibility = View.GONE
                        holder.btnNext.visibility = View.GONE
                        holder.map.visibility = View.VISIBLE

                        refreshMap(holder, currentPersonStatus)

                    }


                }
            }

            holder.rootLayout.setOnClickListener {

                openLocationDetail(currentPersonStatus.userId)

            }
        }
    }

    private fun openLocationDetail (userId: String) {
        val i = Intent(mContext, LocationDetailActivity::class.java)

        val locThread = SharedTimeline.instance.getLocationThread(userId)
        locThread?.let {
            i.putExtra(LocationDetailActivity.EXTRA_LOCATION_ID,it.eventId)
        }
        val alertThread = SharedTimeline.instance.getAlertThread(userId)
        alertThread?.let {
            i.putExtra(LocationDetailActivity.EXTRA_ALERT_ID,it.eventId)
        }


        mContext.startActivity(i)
    }

    private fun refreshMap (holder : PeopleViewHolder, thread: Thread) {

        mCoroutineScope.launch {
            val mcxUrl = thread?.avatarUrl

            if (mcxUrl != null) {
                val urlAvatar = URL(
                    ImApp.sImApp?.matrixSession?.contentUrlResolver()?.resolveThumbnail(
                        mcxUrl, 40, 40,
                        ContentUrlResolver.ThumbnailMethod.SCALE
                    )
                )

                holder.avatar =
                    (BitmapFactory.decodeStream(urlAvatar.openConnection().getInputStream()))
            }
        }
        /**
        val mapFragment = activity.supportFragmentManager.findFragmentById(
            R.id.map
        ) as? SupportMapFragment
        **/
        holder.map?.getMapAsync() { googleMap ->

            googleMap.clear()

            googleMap.uiSettings.setAllGesturesEnabled(false)
            googleMap.uiSettings.isMapToolbarEnabled = false
            googleMap.uiSettings.isMyLocationButtonEnabled = false
            googleMap.setOnMapClickListener {  }


            if (thread.events.isNotEmpty()) {
                var lastEvent: TimelineEvent = thread.events.first()

                if (lastEvent.getLastMessageContent() is MessageLocationContent) {
                    var geoParts =
                        (lastEvent.getLastMessageContent() as MessageLocationContent).geoUri.split(
                            ";"
                        )

                    var latLon = geoParts[0].substring(4).split(",")


                    val place: LatLng = LatLng(latLon.get(0).toDouble(), latLon.get(1).toDouble())

                    googleMap.addMarker(
                        MarkerOptions()
                            .position(place)
                            .icon(holder.avatar?.let { BitmapDescriptorFactory.fromBitmap(it) })
                    )

                    val bounds = LatLngBounds.builder()
                    bounds.include(place)
                    val camBounds = CameraUpdateFactory.newLatLngBounds(bounds.build(), 20)
                    googleMap.moveCamera(camBounds)
                    googleMap.moveCamera(CameraUpdateFactory.zoomBy(-5f))
                }
            }

        }
    }


    private fun getAddress(lat: Double, lng: Double): String? {
        val geocoder = mContext?.let { Geocoder(it) }
        val list = geocoder?.getFromLocation(lat, lng, 1)
        list.let {
            if (list?.size!! > 0)
                return list[0]?.getAddressLine(0)
        }

        return null
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    private fun getPhotoThumbFromUrl (url: String?): String? {
        return mSession?.contentUrlResolver()?.resolveThumbnail(
            url,
            AVATAR_SIZE,
            AVATAR_SIZE,
            ContentUrlResolver.ThumbnailMethod.SCALE
        )
    }

    private fun loadUserAvatar(avatarUrl: String?, ivAvatar: RoundRectCornerImageView) {
        mCoroutineScope.launch {
            getPhotoThumbFromUrl(avatarUrl)?.let {avatarUrl ->
                withContext(Dispatchers.Main) {
                    if (!activity.isDestroyed) {
                        GlideUtils.loadImageFromUri(activity, Uri.parse(avatarUrl), ivAvatar, false)
                    }
                }
            }
        }
    }

    open class PeopleViewHolder(binding: PeopleRowItemBinding) : ViewHolder(binding.root) {
        val rootLayout = binding.cardViewRoot
        val tvName = binding.tvName
        val tvStatus = binding.tvStatus
        //val tvCommentCount = binding.tvCommentCount
        val tvUnread = binding.tvUnread
        val ivUrgent = binding.ivUrgent
        val tvLastCommentTime = binding.tvLastCommentTime
        val ivAvatar = binding.ivAvatar
        val ivNoInternet = binding.ivNoInternet
        val ivImageMap = binding.ivImageMap
        val map = binding.map
        val btnNext = binding.tvNext
        var pbLocationExpires = binding.locationExpiresProgressBar
        var avatar : Bitmap? = null
    }

    class AlertPeopleViewHolder(binding: AlertPeopleRowItemBinding) : ViewHolder(binding.root)  {
        val rootLayout = binding.cardViewRoot
        val cardLayout = binding.cardViewColor
        val tvName = binding.tvName
       // val tvStatus = binding.tvStatus
       // val tvCommentCount = binding.tvCommentCount
        val tvUnread = binding.tvUnread
        val ivUrgent = binding.ivUrgent
        val tvLastCommentTime = binding.tvLastCommentTime
        val ivAvatar = binding.ivAvatar
        val ivNoInternet = binding.ivNoInternet
        val btnShow = binding.btnShow

    }

    open class ViewHolder (root: LinearLayout) : RecyclerView.ViewHolder(root)  {

    }
}
