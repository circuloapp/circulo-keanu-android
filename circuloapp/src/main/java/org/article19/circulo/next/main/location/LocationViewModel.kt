package org.article19.circulo.next.main.location

import android.location.Location

interface LocationViewModel {
    fun getLocation(onSuccess: (location: Location) -> Unit,
                    onFail: (e: Exception) -> Unit)
}