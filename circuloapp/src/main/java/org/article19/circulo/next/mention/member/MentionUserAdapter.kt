package org.article19.circulo.next.mention.member

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.AdapterSuggestedMentionBinding
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary

class MentionUserAdapter : RecyclerView.Adapter<MentionUserAdapter.Holder>() {

    class Holder internal constructor(val binding: AdapterSuggestedMentionBinding) : RecyclerView.ViewHolder(binding.root) {
        val mFullName: TextView = binding.tvDisplayName
        val mUserName: TextView = binding.tvUserId
    }

    private var mUserClickListener: UserPresenter.UserClickListener? = null

    private var mUserList: List<RoomMemberSummary> = mutableListOf()

    fun setData(data: List<RoomMemberSummary>) {
        mUserList = data
    }

    fun setUserClickListener(userClickListener: UserPresenter.UserClickListener) {
        mUserClickListener = userClickListener
    }

    override fun getItemCount(): Int {
        return if (isEmpty()) 1 else mUserList.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Holder {
        val binding = AdapterSuggestedMentionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return Holder(binding)
    }

    private fun isEmpty(): Boolean {
        return mUserList.isEmpty()
    }

    override fun onBindViewHolder(
        holder: Holder,
        position: Int
    ) {
        if (isEmpty()) {
            val context = holder.itemView.context
            holder.mFullName.text = context.getString(R.string.no_user)
            holder.mUserName.text = context.getString(R.string.sorry)
            holder.binding.root.setOnClickListener(null)
            return
        }
        val user: RoomMemberSummary = mUserList.get(position)
        holder.mFullName.text = user.displayName
        holder.mUserName.text = user.userId
        holder.binding.root.setOnClickListener { mUserClickListener?.onUserClicked(user) }
    }
}