package org.article19.circulo.next.main

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import info.guardianproject.keanu.core.ImApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ActivityCreateCircleBinding
import org.matrix.android.sdk.api.session.room.model.GuestAccess
import org.matrix.android.sdk.api.session.room.model.PowerLevelsContent
import org.matrix.android.sdk.api.session.room.model.RoomDirectoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomHistoryVisibility
import org.matrix.android.sdk.api.session.room.model.RoomJoinRules
import org.matrix.android.sdk.api.session.room.model.create.CreateRoomParams
import org.matrix.android.sdk.api.session.room.powerlevels.Role

class CreateCircleActivity : BaseActivity() {

    private val mApp: ImApp?
        get() = application as? ImApp


    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private val mHandler = Handler()

    private lateinit var mViewBinding: ActivityCreateCircleBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = ActivityCreateCircleBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)

        mViewBinding.toolbar.tvBackText.visibility = View.VISIBLE
        mViewBinding.toolbar.tvBackText.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }


        val editTextRoomName = mViewBinding.editTextRoomName

        mViewBinding.toolbar.tvAction.visibility = View.VISIBLE
        mViewBinding.toolbar.tvAction.setText(R.string.Done)
        mViewBinding.toolbar.tvAction.setOnClickListener {
            createCircle(editTextRoomName.text.toString())

        }


        editTextRoomName.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                createCircle(editTextRoomName.text.toString())
                return@OnKeyListener true
            }
            false
        })

        mViewBinding.btnNext.setOnClickListener {

           createCircle(editTextRoomName.text.toString())

        }
    }

    private fun createCircle (name : String) {

        if (name.isEmpty())
            return

        mViewBinding.editTextRoomName.isEnabled = false

        mViewBinding.progressbar.visibility = View.VISIBLE

        val params = CreateRoomParams()
        params.historyVisibility = RoomHistoryVisibility.JOINED
        params.visibility = RoomDirectoryVisibility.PRIVATE

        params.enableEncryption()
        params.name = name
        params.powerLevelContentOverride = PowerLevelsContent(Role.Admin.value)

        mCoroutineScope.launch {
            val roomId = mApp?.matrixSession?.roomService()?.createRoom(params)

            if (roomId != null) {
                val room = mApp?.matrixSession?.roomService()?.getRoom(roomId)
                room?.stateService()
                    ?.updateJoinRule(RoomJoinRules.PUBLIC, GuestAccess.Forbidden)

            }

            withContext(Dispatchers.Main) {
                mViewBinding.progressbar.visibility = View.GONE
                setResult(Activity.RESULT_OK)
                finish()
            }
        }

    }
}
