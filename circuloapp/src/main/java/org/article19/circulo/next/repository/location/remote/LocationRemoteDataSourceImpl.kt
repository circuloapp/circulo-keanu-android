package org.article19.circulo.next.repository.location.remote

import android.Manifest
import android.content.Context
import android.location.Location
import androidx.annotation.RequiresPermission
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.article19.circulo.next.di.module.IO_DISPATCHER
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class LocationRemoteDataSourceImpl @Inject constructor(
    @Named(IO_DISPATCHER) private val iODispatcher: CoroutineDispatcher,
    @ApplicationContext private val context: Context,
) : LocationRemoteDataSource {

    private val locationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(context)
    }

    @RequiresPermission(
        allOf = [
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION]
    )
    override suspend fun getLocation(
        onSuccess: (location: Location) -> Unit,
        onFail: (e: Exception) -> Unit,
    ) {
        locationClient.lastLocation
            .addOnSuccessListener { loc: Location? ->
                loc?.let {
                    onSuccess.invoke(it)
                } ?: let {
                    Timber.w("Last location is null. Requesting new now!")
                    CoroutineScope(iODispatcher).launch {
                        val locationRequest = LocationRequest.Builder(
                            Priority.PRIORITY_HIGH_ACCURACY, 10000
                        ).build()
                        val builder = LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest)

                        LocationServices.getSettingsClient(context)
                            .checkLocationSettings(builder.build()).addOnSuccessListener {
                                locationClient.getCurrentLocation(
                                    Priority.PRIORITY_HIGH_ACCURACY,
                                    null
                                )
                                    .addOnSuccessListener { location ->
                                        onSuccess.invoke(location)
                                    }.addOnFailureListener {
                                        onFail.invoke(it)
                                    }
                            }.addOnFailureListener {
                                Timber.e("checkLocationSettings exception: $it.message")
                                onFail.invoke(it)
                            }
                    }
                }
            }
    }
}