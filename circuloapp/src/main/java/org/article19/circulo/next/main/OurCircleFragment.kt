package org.article19.circulo.next.main

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.extensions.displayNameWorkaround
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ButtonAddToCircleBinding
import org.article19.circulo.next.databinding.FragmentOurCircleBinding
import org.article19.circulo.next.main.circleinfo.CircleInfoActivity
import org.article19.circulo.next.main.circleinfo.CircleMember
import org.article19.circulo.next.main.circleinfo.CircleSummary
import org.article19.circulo.next.main.circleinfo.viewmodel.CircleInfoViewModel
import org.article19.circulo.next.main.listeners.OnCircleLoadedListener
import org.article19.circulo.next.onboarding.CirculoOnboardProvider
import org.matrix.android.sdk.api.query.QueryStringValue
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.members.RoomMemberQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.create.RoomCreateContent
import kotlin.random.Random

class OurCircleFragment : Fragment(), OnCircleLoadedListener {

    private lateinit var mViewBinding: FragmentOurCircleBinding

    private val mRandomAvatars = listOf(
        R.drawable.ic_avatar_ana,
        R.drawable.ic_avatar_chloe,
        R.drawable.ic_avatar_juana,
        R.drawable.ic_avatar_marta,
        R.drawable.ic_avatar_mariel
    )

    private var mRoom: Room? = null
    private var mRoomSummary: RoomSummary? = null

    private val mApp: ImApp?
        get() = activity?.application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mCircleMembers = mutableListOf<CircleMember>()

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO)
    }

    private lateinit var mCircleInfoViewModel: CircleInfoViewModel

    private var mCircleCreator: String = ""

    companion object {
        const val BUNDLE_EXTRA_CIRCLE_SUMMARY = "bundle_extra_circle_summary"
        const val TAG = "OurCircleFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        mCircleInfoViewModel = ViewModelProvider(this)[CircleInfoViewModel::class.java]
        mViewBinding = FragmentOurCircleBinding.inflate(inflater, container, false)

        inflateMemberData()

        mViewBinding.tvInfo.setOnClickListener {
//          open group info view
            val circleInfoIntent = Intent(activity, CircleInfoActivity::class.java)
            circleInfoIntent.putExtra(BUNDLE_EXTRA_CIRCLE_SUMMARY,
                CircleSummary(mRoomSummary?.roomId ?:"", mRoomSummary?.displayName ?: "", mCircleMembers,
            mRoomSummary?.isPublic ?: false, mCircleCreator, topic = mRoomSummary?.topic ?: ""))
            (activity as? MainActivity)?.viewCircleInfoResult?.launch(circleInfoIntent)
        }

        observeLiveData()
        return mViewBinding.root
    }

    private fun updateCircleName () {
        mViewBinding.tvHeadline.text = mRoom?.roomSummary()?.displayNameWorkaround
    }

    private fun openInviteView () {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, CirculoOnboardProvider.generateInviteLink(mRoom?.roomId ?: ""))
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)

    }

    private fun inflateMemberData() {
        for (i in 0..5) {
            inflatePersonData(i)
        }

    }

    private fun inflatePersonData(memberIndex: Int) {

        val buttonAddToCircleBinding: ButtonAddToCircleBinding = when (memberIndex) {
            0 -> mViewBinding.btnAdd6
            1 -> mViewBinding.btnAdd1
            2 -> mViewBinding.btnAdd2
            3 -> mViewBinding.btnAdd3
            4 -> mViewBinding.btnAdd4
            5 -> mViewBinding.btnAdd5
            else -> mViewBinding.btnAdd6
        }

        val ivAvatar = buttonAddToCircleBinding.ivAvatar
        val layoutAvatar = buttonAddToCircleBinding.layoutAvatar
        val ivNoInternet = buttonAddToCircleBinding.ivNoInternet

        if (memberIndex < mCircleMembers.size) {
            layoutAvatar.background = null
            val tvMemberName = buttonAddToCircleBinding.tvMemberName
            ivAvatar.visibility = View.VISIBLE

            val groupMember = mCircleMembers[memberIndex]
            mCoroutineScope.launch {

                if (groupMember.avatarUrl?.isNotEmpty() == true) {

                    mSession?.contentUrlResolver()?.resolveThumbnail(groupMember.avatarUrl, 59, 59, ContentUrlResolver.ThumbnailMethod.SCALE)
                        ?.let {
                            withContext(Dispatchers.Main) {
                                GlideUtils.loadImageFromUri(activity, Uri.parse(it), ivAvatar, true)
                            }
                        }

                }
                else
                {
                    withContext(Dispatchers.Main) {
                        ivAvatar.setImageResource(mRandomAvatars[Random.nextInt(5)])
                    }
                }
            }

            if (mSession?.myUserId?.equals(mCircleMembers[memberIndex].userId) == true) {
                tvMemberName.text = getString(R.string.me_title)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tvMemberName.setBackgroundColor(requireActivity().getColor(R.color.holo_green_dark))
                }
            } else {
                tvMemberName.text = mCircleMembers[memberIndex].name

                if (mCircleMembers[memberIndex].isOnline) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tvMemberName.setBackgroundColor(requireActivity().getColor(R.color.holo_green_dark))
                    }
               //     ivNoInternet.visibility = View.GONE
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ivAvatar.foreground = null
                    }
                } else {
                 //   ivNoInternet.visibility = View.VISIBLE
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        ivAvatar.foreground = ContextCompat.getDrawable(CirculoApp.getInstance().applicationContext, R.color.grey_30)
                    }
                    tvMemberName.background = ContextCompat.getDrawable(CirculoApp.getInstance().applicationContext, R.drawable.bg_text_name_our_circle)
                }
            }
            tvMemberName.visibility = View.VISIBLE

            buttonAddToCircleBinding.root.contentDescription = tvMemberName.text
            buttonAddToCircleBinding.ivAdd.visibility = View.GONE
            buttonAddToCircleBinding.root.setOnClickListener {
                //TODO: Should we open person's profile?
            }
        } else {

            ivAvatar.visibility = View.GONE
            layoutAvatar.setBackgroundResource(R.drawable.bg_button_add_to_circle)
            buttonAddToCircleBinding.ivAdd.visibility = View.VISIBLE
            buttonAddToCircleBinding.root.contentDescription = getString(R.string.content_description_add_member)

            buttonAddToCircleBinding.root.setOnClickListener {
                openInviteView()
            }
        }

    }

    private fun updateMembers() {
        val builder = RoomMemberQueryParams.Builder()
        builder.memberships = arrayListOf(Membership.JOIN, Membership.INVITE)
        val query = builder.build()

        mRoom?.membershipService()?.getRoomMembersLive(query)?.observe(this) {
            val roomId = mRoom?.roomId ?: return@observe
            mCircleInfoViewModel.getCircleMembers(roomId, it)

        }


    }

    override fun onCircleLoaded(circle: Room?, circleSummary: RoomSummary?) {
        mRoom = circle

        //Getting room creator name
        val roomCreatorId = mRoom?.stateService()?.getStateEvent(EventType.STATE_ROOM_CREATE, QueryStringValue.IsEmpty)
            ?.content
            ?.toModel<RoomCreateContent>()
            ?.creator

        roomCreatorId?.let {
            mCircleCreator = mApp?.matrixSession?.userService()?.getUser(roomCreatorId)?.displayName ?: ""
        }

        mRoomSummary = circleSummary
        updateCircleName()
        updateMembers()
    }

    private fun observeLiveData() {
        val hostActivity = activity ?: return
        mCircleInfoViewModel.observableGetCircleMembersLiveData.observe(hostActivity) {
            mCircleMembers.clear()
            mCircleMembers.addAll(it)
            inflateMemberData()
        }
    }
}