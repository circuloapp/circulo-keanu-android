package org.article19.circulo.next.settings.camouflage

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.ActivityAppIconChooserBinding
import org.article19.circulo.next.settings.SettingPhysicalSafetyFragment

class AppIconChooserActivity : BaseActivity() {

    private lateinit var mViewBinding: ActivityAppIconChooserBinding

    private var mAppIconList = mutableListOf<AppIconChooserModel>()
    private lateinit var mIconChooserAdapter: AppIconChooserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = ActivityAppIconChooserBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)
        mViewBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }
        mViewBinding.toolbar.tvTitle.text = getString(R.string.settings_app_icon_chooser_title)
        showAppIconData()
    }

    private fun showAppIconData() {

        mAppIconList.apply {
            add(AppIconChooserModel(
                packageName = SettingPhysicalSafetyFragment.disguisePackage[0],
                appIconRes = R.mipmap.ic_launcher_circulo,
                appIconNameRes = R.string.app_name))
            add(
                AppIconChooserModel(
                    packageName = SettingPhysicalSafetyFragment.disguisePackage[1],
                    appIconRes = R.drawable.ic_camouflage_night_watch,
                    appIconNameRes = R.string.app_icon_chooser_label_night_watch)
            )
            add(
                AppIconChooserModel(
                    packageName = SettingPhysicalSafetyFragment.disguisePackage[2],
                    appIconRes = R.drawable.ic_camouflage_assistant,
                    appIconNameRes = R.string.app_icon_chooser_label_assistant)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[3],
                    appIconRes = R.drawable.ic_camouflage_paint,
                    appIconNameRes = R.string.app_icon_chooser_label_paint)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[4],
                    appIconRes = R.drawable.ic_camouflage_tetras,
                    appIconNameRes = R.string.app_icon_chooser_label_tetras)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[5],
                    appIconRes = R.drawable.ic_camouflage_todo,
                    appIconNameRes = R.string.app_icon_chooser_label_todo)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[6],
                    appIconRes = R.drawable.ic_camouflage_fitgrit,
                    appIconNameRes = R.string.app_icon_chooser_label_fit_grit)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[7],
                    appIconRes = R.drawable.ic_camouflage_birdie,
                    appIconNameRes = R.string.app_icon_chooser_label_birdie)
            )
        }

        mViewBinding.recyclerViewAppIcon.layoutManager = GridLayoutManager(this, 4)
        mIconChooserAdapter = AppIconChooserAdapter(this, mAppIconList)
        mViewBinding.recyclerViewAppIcon.adapter = mIconChooserAdapter
    }
}