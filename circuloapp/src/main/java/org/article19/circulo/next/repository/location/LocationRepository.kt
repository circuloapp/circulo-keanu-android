package org.article19.circulo.next.repository.location

import android.location.Location

interface LocationRepository {
    suspend fun getLocation(onSuccess: (location: Location) -> Unit,
                            onFail: (e: Exception) -> Unit)
}