package org.article19.circulo.next.main

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.messaging.FirebaseMessaging
import com.scottyab.rootbeer.RootBeer
import dagger.hilt.android.AndroidEntryPoint
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.MainActivity.Companion.EXTRA_PRESELECT_TAB
import info.guardianproject.keanu.core.util.PrettyTime
import info.guardianproject.keanuapp.ui.conversation.QuickReaction
import info.guardianproject.keanuapp.ui.widgets.AudioWife
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.SharedTimeline.Companion.TAG_REACTION_SEEN
import org.article19.circulo.next.SharedTimeline.Companion.TAG_RESOLVED
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.databinding.ActivityMainBinding
import org.article19.circulo.next.databinding.FragmentAlertNowBinding
import org.article19.circulo.next.main.listeners.OnCircleLoadedListener
import org.article19.circulo.next.main.now.Status.Companion.NOTSAFE_STRING
import org.article19.circulo.next.main.now.Status.Companion.URGENT_STRING
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.main.now.ThreadsAdapter
import org.article19.circulo.next.main.updatestatus.StatusDetailActivity
import org.article19.circulo.next.main.updatestatus.UpdateLocationStatusService
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity.Companion.STATUS_SEND_LOCATION
import org.article19.circulo.next.notify.viewmodel.PushViewModel
import org.article19.circulo.next.notify.viewmodel.PushViewModelImpl
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.crypto.attachments.toElementToDecrypt
import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.getTimelineEvent
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.message.MessageLocationContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.message.getFileName
import org.matrix.android.sdk.api.session.room.model.message.getFileUrl
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.sync.SyncState
import org.matrix.android.sdk.api.session.user.model.User
import timber.log.Timber
import java.util.Date


@AndroidEntryPoint
class MainActivity : BaseActivity(), Observer<Any> {

    private lateinit var fragmentNow: NowFragment
    private lateinit var fragmentCircle: Fragment
    private lateinit var fragmentOurCircle: Fragment
    private lateinit var fragmentProfile: ProfileFragment
    private lateinit var fragmentManager: FragmentManager
    private lateinit var currentFragment: Fragment
    private lateinit var mBinding: ActivityMainBinding

    private var inACircle : Boolean = false

    private val mApp: CirculoApp?
        get() = application as? CirculoApp

    private var mRoomSummary: RoomSummary? = null
    private var mRoom : Room? = null

    private val mSession: Session?
        get() = mApp?.matrixSession

    private var mCircleLoadedListeners = mutableSetOf<OnCircleLoadedListener>()

    private val mUserInfoLiveData = MutableLiveData<User?>()
    val observableUserInfo = mUserInfoLiveData as LiveData<User?>

    private lateinit var mPushViewModel: PushViewModel

    val joinOrCreateCircleResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            val queryParams = RoomSummaryQueryParams.Builder().apply {
                memberships = listOf(Membership.JOIN)
            }.build()
            inACircle = mApp?.matrixSession?.roomService()?.getRoomSummaries(queryParams)?.isEmpty() == false

            currentFragment = if (inACircle) {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                fragmentOurCircle
            } else {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                fragmentCircle
            }
        }
    }

    val viewCircleInfoResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val isLeavingCircle = it.data?.getStringExtra(BUNDLE_KEY_LEAVE_CIRCLE)?.equals(BUNDLE_VALUE_LEAVE_CIRCLE) == true
        if ((it.resultCode == Activity.RESULT_OK) && isLeavingCircle) {
            fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
            currentFragment = fragmentCircle
        }
    }

    companion object {
        const val KEY_LAST_FRAGMENT_INDEX = "key_last_fragment_index"
        const val TAB_NOW = 0
        const val TAB_CIRCLE = 1
        const val TAB_PROFILE = 2
        const val BUNDLE_KEY_LEAVE_CIRCLE = "bundle_key_leave_circle"
        const val BUNDLE_VALUE_LEAVE_CIRCLE = "bundle_value_leave_circle"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPushViewModel = ViewModelProvider(this)[PushViewModelImpl::class.java]
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.bottomBar.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.now -> {
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragmentNow).commit()
                    currentFragment = fragmentNow
                    true
                }

                R.id.circle -> {
                    currentFragment = if (inACircle) {
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                        fragmentOurCircle
                    } else {
                        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                        fragmentCircle
                    }

                    true
                }

                R.id.profile -> {
                    fragmentManager.beginTransaction().hide(currentFragment).show(fragmentProfile).commit()
                    currentFragment = fragmentProfile

                    true
                }

                else -> false
            }
        }

        queryCircles()

        fragmentManager = supportFragmentManager
        initFragments(savedInstanceState)

        checkAndShowLockScreenFragment()

      //  disableBatteryOptimization()

        requestPermission(Manifest.permission.POST_NOTIFICATIONS)
        requestPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        requestPermission(Manifest.permission.RECORD_AUDIO)

        startObservingUserInfo()

        stopVibration()
        initFCMData()

        checkRoot ()

        checkShortcutLaunch()
    }


    val ACTION_SEND_URGENT = "org.article19.circulo.next.SEND_ALERT"
    val ACTION_SEND_LOCATION = "org.article19.circulo.next.SEND_LOCATION"

    private fun checkShortcutLaunch () {

        val autoLocation = intent.getBooleanExtra(STATUS_SEND_LOCATION, false) || intent.action == (ACTION_SEND_LOCATION)
        val autoAlert = intent.action == (ACTION_SEND_URGENT)

        if (autoLocation) {
            sendLocationStatus(4)
            finish()
        }

        if (autoAlert) {
            SharedTimeline.instance.room?.sendService()
                ?.sendTextMessage("$URGENT_STRING $NOTSAFE_STRING")
            sendLocationStatus(4)
            finish()
        }

    }

    private fun checkRoot () {
        val rootBeer = RootBeer(this)
        if (rootBeer.isRooted) {
            //we found indication of root
            Toast.makeText(this, getString(R.string.ROOT_WARNING), Toast.LENGTH_LONG).show()

        } else {
            //we didn't find indication of root
        }


    }

    override fun onResume() {
        super.onResume()

        showAlertControlFragment()
    }

    private fun initFCMData() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener {
            if (!it.isSuccessful) {
                return@addOnCompleteListener
            }
            onNewToken(it.result)
        }
    }

    private fun onNewToken(token: String) {
        mPushViewModel.registerPushToken(token)
    }

    private fun initFragments(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            fragmentNow = NowFragment()
            fragmentCircle = CircleFragment()
            fragmentOurCircle = OurCircleFragment()
            fragmentProfile = ProfileFragment()

            currentFragment = fragmentNow

            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentCircle, CircleFragment.TAG).hide(fragmentCircle).commit()
            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentOurCircle, OurCircleFragment.TAG).hide(fragmentOurCircle).commit()
            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentProfile, ProfileFragment.TAG).hide(fragmentProfile).commit()

            fragmentManager.beginTransaction().add(R.id.layout_main_container, fragmentNow, NowFragment.TAG).commit()

        } else {
            //Restore the fragment's instance
            fragmentNow = fragmentManager.getFragment(savedInstanceState, NowFragment.TAG) as NowFragment
            fragmentCircle = fragmentManager.getFragment(savedInstanceState, CircleFragment.TAG) as CircleFragment
            fragmentOurCircle = fragmentManager.getFragment(savedInstanceState, OurCircleFragment.TAG) as OurCircleFragment
            fragmentProfile = fragmentManager.getFragment(savedInstanceState, ProfileFragment.TAG) as ProfileFragment

            val lastFragmentIndex = savedInstanceState.getInt(KEY_LAST_FRAGMENT_INDEX)
            currentFragment = getFragment(lastFragmentIndex)
        }

        mCircleLoadedListeners.add(fragmentOurCircle as OurCircleFragment)
        mCircleLoadedListeners.add(fragmentProfile)

        val preselectedTabPos = intent.getIntExtra(EXTRA_PRESELECT_TAB, -1)
        if (preselectedTabPos >= 0) {
            mBinding.bottomBar.selectedItemId = getTabIdFromPosition(preselectedTabPos)
        }
    }

    private fun getFragment(index: Int): Fragment {
        return when (index) {
            TAB_NOW -> fragmentNow
            TAB_CIRCLE -> fragmentOurCircle
            TAB_PROFILE -> fragmentProfile
            else -> {fragmentNow}
        }
    }

    private fun getTabIdFromPosition(pos: Int): Int {
        return when (pos) {
            TAB_NOW -> R.id.now
            TAB_CIRCLE -> R.id.circle
            TAB_PROFILE -> R.id.profile
            else -> R.id.now
        }
    }


    fun replaceCurrentFragment(newFragment: Fragment) {
        fragmentManager.beginTransaction().hide(currentFragment).add(R.id.layout_main_container, newFragment, "").commit()
        currentFragment = newFragment
        fragmentManager.beginTransaction().show(currentFragment).commit()
    }

    fun showProfileFragment(reloadUserInfo: Boolean) {
        fragmentManager.beginTransaction().hide(currentFragment).show(fragmentProfile).commit()
        currentFragment = fragmentProfile

        if (reloadUserInfo) {
            //this is often called after a reload fragment
            fragmentProfile.reloadUserInfo()
        }
    }

    fun removeLockScreenFragment() {
        mBinding.bottomBar.visibility = View.VISIBLE
        fragmentManager.beginTransaction().remove(currentFragment).show(fragmentNow).commit()
        currentFragment = fragmentNow
        mBinding.bottomBar.selectedItemId = R.id.now
    }

    private fun queryCircles() {
        SharedTimeline.instance.refreshCircles(this, this)
    }


    override fun onChanged(value: Any) {
        val syncState = value as? SyncState

        if (syncState != null) {
            Timber.d("SyncState: $syncState")

            return
        }

        val roomSummary = (value as? List<*>)?.firstOrNull() as? RoomSummary

        val newInACircle: Boolean
        if (roomSummary != null) {
            newInACircle = true

            //Circulo is always meant to be in just ONE room, so we can just get that room
            mRoomSummary = roomSummary
            mRoom = mSession?.roomService()?.getRoom(roomSummary.roomId)

            for (circleLoadedListener in mCircleLoadedListeners) {
                circleLoadedListener.onCircleLoaded(mRoom, mRoomSummary)
            }
        }
        else {
            newInACircle = false
        }

        inACircle = newInACircle

        //Circle was just deleted. Reset Now fragment to default empty view
        if (inACircle && !newInACircle) {
            fragmentNow.showDefaultEmptyView()
        }

        inACircle = newInACircle

        if (getCurrentSelectedPos() == 1) {
            currentFragment = if (inACircle) {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentOurCircle).commit()
                fragmentOurCircle
            } else {
                fragmentManager.beginTransaction().hide(currentFragment).show(fragmentCircle).commit()
                fragmentCircle
            }
        }

        if (alertBottomSheet?.isVisible == true)
            alertBottomSheet?.updateEvent()
    }

    fun hideNavigationView() {
        mBinding.bottomBar.visibility = View.GONE
    }

    fun showNavigationView() {
        mBinding.bottomBar.visibility = View.VISIBLE
    }

    /**
     * check if the log is enables or not
     */
    private fun checkAndShowLockScreenFragment() {
        PFPinCodeViewModel().isPinCodeEncryptionKeyExist.observe(
            this,
            Observer { result ->
                if (result == null) {
                    return@Observer
                }
                if (result.error != null) {
                    Toast.makeText(
                        this@MainActivity,
                        info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching,
                        Toast.LENGTH_SHORT
                    ).show()
                    return@Observer
                }
                showLockScreenFragment(result.result)
            }
        )
    }

    /**
     * show or hide the lock screen
     */
    fun showLockScreenFragment(isPinExist: Boolean) {

        if (!isPinExist) {
            return
        }

        val builder = PFFLockScreenConfiguration.Builder(this)
            .setTitle(getString(info.guardianproject.keanuapp.R.string.lock_screen_passphrase_not_set_enter))
            .setCodeLength(6)
            .setNewCodeValidation(true)
            .setNewCodeValidationTitle(getString(info.guardianproject.keanuapp.R.string.lock_screen_confirm_passphrase))
            .setErrorVibration(true)
            .setClearCodeOnError(true)
            .setErrorAnimation(true)
            .setTitle(getString(info.guardianproject.keanuapp.R.string.title_activity_lock_screen))

        val fragment = PFLockScreenFragment()
        builder.setMode(PFFLockScreenConfiguration.MODE_AUTH)
        fragment.setEncodedPinCode(PreferenceProvider.appPreferences.getString(KeanuConstants.PREFERENCE_KEY_ENCODED_PASS, ""))
        fragment.setLoginListener(mLoginListener)
        fragment.setConfiguration(builder.build())

        mBinding.bottomBar.visibility = View.GONE
        replaceCurrentFragment(fragment)
    }

    fun sendAlertEvent (me: MotionEvent) {
            mBinding.alertButton.dispatchTouchEvent(me)
    }

    fun showAlertFragment (me: MotionEvent) {
        mBinding.bottomBar.visibility = View.GONE

        mBinding.alertContainer.visibility = View.VISIBLE
        SendAlertHelper(this, me, mBinding).init()
    }

    fun removeAlertFragment() {
//        mBinding.bottomBar.selectedItemId = R.id.now
        mBinding.alertContainer.visibility = View.GONE
        mBinding.bottomBar.visibility = View.VISIBLE

    }

    var alertBottomSheet : AlertBottomSheet? = null

    fun showAlertControlFragment () {

        Handler().postDelayed({
            SharedTimeline.instance.getMyAlertThread().let { itAlert ->
                if (itAlert != null) {
                    val itLocation = SharedTimeline.instance.getMyLocationThread()

                    alertBottomSheet = AlertBottomSheet(this, itAlert, itLocation)

                    try {
                        alertBottomSheet?.show(supportFragmentManager, AlertBottomSheet.TAG)
                    }
                    catch (ise : IllegalStateException ){}

                }
            }

        }, 5000)

    }



    private val mLoginListener: PFLockScreenFragment.OnPFLockScreenLoginListener = object :
        PFLockScreenFragment.OnPFLockScreenLoginListener {
        override fun onCodeInputSuccessful() {
            removeLockScreenFragment()
        }

        override fun onFingerprintSuccessful() {
            removeLockScreenFragment()
        }

        override fun onPinLoginFailed() {
            Toast.makeText(this@MainActivity, info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching, Toast.LENGTH_SHORT).show()
        }

        override fun onFingerprintLoginFailed() {

            Toast.makeText(this@MainActivity, info.guardianproject.keanuapp.R.string.lock_screen_passphrases_not_matching, Toast.LENGTH_SHORT).show()
        }
    }

    fun stopApp () {
        mApp?.stopApp()
        finish()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        //Save the fragment's instance
        fragmentManager.putFragment(outState, NowFragment.TAG, fragmentNow)
        fragmentManager.putFragment(outState, CircleFragment.TAG, fragmentCircle)
        fragmentManager.putFragment(outState, OurCircleFragment.TAG, fragmentOurCircle)
        fragmentManager.putFragment(outState, ProfileFragment.TAG, fragmentProfile)

        outState.putInt(KEY_LAST_FRAGMENT_INDEX, getCurrentSelectedPos())


    }

    fun getCurrentSelectedPos(): Int {
        return when (mBinding.bottomBar.selectedItemId) {
            R.id.now -> TAB_NOW
            R.id.circle -> TAB_CIRCLE
            R.id.profile ->  {
                if (currentFragment !is ProfileFragment) {
                    -1
                } else {
                    TAB_PROFILE
                }
            }
            else -> {
                TAB_NOW
            }
        }
    }

    fun setCurrentFragment(fragment: Fragment) {
        currentFragment = fragment
    }

    /**
    private fun disableBatteryOptimization() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent()
            val packageName = packageName
             val pm = getSystemService(POWER_SERVICE) as PowerManager
              if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                  intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                  intent.data = Uri.parse("package:$packageName")
                  startActivity(intent)
              }
        }
    }**/

    private fun requestPermission (permission: String) {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                this,
                permission
            ) -> {
                // You can use the API that requires the permission.
            }
            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    requestPermissionLauncher.launch(permission)
                }
            }
        }
    }


    private fun stopVibration() {
        mApp?.cancelNotifications()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)

        val detailIntent = Intent(this, StatusDetailActivity::class.java)

        val personStatus = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.extras?.getParcelable(ThreadsAdapter.BUNDLE_EXTRA_THREAD, Thread::class.java)
        } else {
            @Suppress("DEPRECATION")
            intent.extras?.getParcelable(ThreadsAdapter.BUNDLE_EXTRA_THREAD)
        }

        personStatus?.let {
            detailIntent.putExtra(StatusDetailActivity.EXTRA_THREAD_ID, it.eventId)
            startActivity(detailIntent)
        }
    }

    private fun startObservingUserInfo() {
        mApp?.matrixSession?.myUserId?.let {
            mApp?.matrixSession?.userService()?.getUserLive(it)?.observe(this) { currentUser ->
                mUserInfoLiveData.value = currentUser.getOrNull()
            }
        }
    }



    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { }

    class AlertBottomSheet (val _context: Context, val _alertThread: Thread, val _locThread: Thread?): BottomSheetDialogFragment() {

        private lateinit var mBinding: FragmentAlertNowBinding

        private lateinit var audioFileUri: Uri

        private var handler = Handler()

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = FragmentAlertNowBinding.inflate(inflater, container, false)

            mBinding.btnImOkay.setOnClickListener {


                confirmCancel()

            }

            updateEvent()

            return mBinding.root
        }

        private fun confirmCancel () {
            val builder = AlertDialog.Builder(requireActivity())
            builder.setMessage(getString(R.string.we_ll_notify_your_circle_and_close_the_alert))
                .setTitle(getString(R.string.glad_you_re_okay))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.action_okay)) { dialog, id ->

                    cancelAlert()


                }
                .setNegativeButton(getString(R.string.not_okay)) { dialog, id ->
                    // Dismiss the dialog
                    dialog.dismiss()
                }
            val alert = builder.create()
            alert.show()




        }

        public fun updateEvent () {

            if (_locThread != null) {
                for (event: TimelineEvent in _locThread.events) {

                    if (event.getLastMessageContent() is MessageLocationContent) {


                        val geoParts =
                            (event.getLastMessageContent() as MessageLocationContent).geoUri.split(
                                ";"
                            )

                        val latLon = geoParts[0].substring(4).split(",")
                        val address = context?.let {
                            getAddress(
                                it,
                                latLon[0].toDouble(),
                                latLon[1].toDouble()
                            )
                        }
                        mBinding.sentFrom.text = address

                        SharedTimeline.instance.getAlertThread(event.senderInfo.userId)?.events?.get(
                            0
                        )
                            ?.let { updateSeenBy(it) }

                        //found so break the loop
                        break;
                    }

                }
            }

            GlobalScope.launch {

                for (event: TimelineEvent in _alertThread.events) {

                    if (event.getLastMessageContent() is MessageWithAttachmentContent) {


                        var msgAudio = event.getLastMessageContent() as MessageWithAttachmentContent

                        val fileMedia =
                            SharedTimeline.instance.getSession()?.fileService()?.downloadFile(
                                msgAudio.getFileName(),
                                msgAudio.mimeType,
                                msgAudio.getFileUrl(),
                                msgAudio.encryptedFileInfo?.toElementToDecrypt()
                            )

                        audioFileUri = Uri.fromFile(fileMedia)

                        handler.post {
                            var audioWife = AudioWife()
                            var uriAudio = Uri.fromFile(fileMedia)
                            audioWife?.init(requireActivity(), uriAudio, msgAudio.mimeType)
                                ?.useDefaultUi(
                                    mBinding.audioContainer,
                                    LayoutInflater.from(requireActivity())
                                )
                        }


                        break;
                    }
                }
            }



        }

        private fun getAddress(context: Context, lat: Double, lng: Double): String? {
            val geocoder = context?.let { Geocoder(it) }
            val list = geocoder?.getFromLocation(lat, lng, 1)
            list.let {
                if (list?.size!! > 0)
                    return list[0]?.getAddressLine(0)
            }

            return null
        }

        private fun updateSeenBy (event: TimelineEvent) {
            val reactions = HashMap<String, QuickReaction>()

            val newEvent = SharedTimeline.instance.room?.getTimelineEvent(event.eventId)

            for (reaction in newEvent?.annotations?.reactionsSummary ?: emptyList()) {
                val qr = reactions[reaction.key] ?: QuickReaction(reaction.key, ArrayList())

                if (reaction.addedByMe) qr.sentByMe = true

                for (eventId in reaction.sourceEvents) {
                    SharedTimeline.instance.room?.timelineService()?.getTimelineEvent(eventId)?.senderInfo?.userId?.let {
                        qr.senders.add(it)
                    }
                }

                reactions[reaction.key] = qr
            }

            if (reactions.containsKey(TAG_REACTION_SEEN)) {

                val sb = StringBuilder()

                var senders = reactions.get(TAG_REACTION_SEEN)?.senders!!

                sb.append(getString(R.string.seen_by_1_people, senders.size))
                sb.append(("\n\n")) //line break

                for (userId in senders)
                {
                    val displayName = SharedTimeline.instance.getDisplayName(userId)
                    if (displayName != null) {
                        sb.append(displayName)
                    }
                    else {
                        sb.append(userId.split(":")[0])
                    }

                    sb.append((" "))
                }

                mBinding.seenBy.text = sb.toString()
            }

            mBinding.sentAt.text =
                PrettyTime.format(event?.root?.originServerTs?.let { Date(it) })

        }

        private fun cancelAlert () {

            //get latest alert status and cancel it

            /**
            SharedTimeline.instance.room?.relationService()?.sendReaction(
                it1, TAG_RESOLVED
            )**/

            val threadAlert = SharedTimeline.instance.getMyAlertThread()

            if (threadAlert != null) {
                addStatusReply("#resolved", threadAlert)

                SharedTimeline.instance.room?.relationService()?.sendReaction(
                    threadAlert.eventId, TAG_RESOLVED)
            }

            /**
            for (event in threadAlert?.events!!) {
                SharedTimeline.instance.room?.sendService()?.redactEvent(event.root, "resolved")
            }**/

            val intent = Intent(_context, UpdateLocationStatusService::class.java)
            _context.stopService(intent)

            dismiss()

        }

        private fun addStatusReply (update : CharSequence, threadAlert : Thread)
        {
            val event = threadAlert?.events?.firstOrNull() ?: return
            SharedTimeline.instance.room?.relationService()?.replyInThread(event.eventId,update, MessageType.MSGTYPE_TEXT)
        }

        companion object {
            const val TAG = "ModalBottomSheet"
        }


    }


    private var requestCodeLocation = 9999
    private var requestCodeLocationUrgent = 9998
    private var requestCodeAudio = 9997

    fun sendLocationStatus (timeIntervalSelect : Int) {
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION),requestCodeLocation)

        }
        else
        {
            var totalTimeMinutes = 15;

            when (timeIntervalSelect)
            {
                0 -> totalTimeMinutes = 15
                1 -> totalTimeMinutes = 60
                2 -> totalTimeMinutes = 60*4
                3 -> totalTimeMinutes = 60*8
                4 -> totalTimeMinutes = -1
            }
            val intent = Intent(this, UpdateLocationStatusService::class.java)

            intent.putExtra(UpdateLocationStatusService.EXTRA_TOTAL_TIME,totalTimeMinutes)
            intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_LOCATION,true)

            ContextCompat.startForegroundService(this,intent)
        }

        addStatusMeasurement()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            //if location granted, then add location
            requestCodeLocation -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    askSendLocationStatus()
                } else {

                }
                return
            }
            requestCodeLocationUrgent -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //do nothing lastMotionEvent
                }

                /**
                lastMotionEvent.let {
                    showAlertFragment(it!!)
                }**/
                return
            }
        }
    }

    fun askSendLocationStatus () {

        val items = arrayOf(getString(R.string.share_location_15_min),
            getString(R.string.share_location_1_hr),
            getString(R.string.share_location_4_hrs),
            getString(R.string.share_location_8_hrs),
            getString(R.string.share_location_until_i_arrive));

        val builder = AlertDialog.Builder(this)
        builder
            .setItems(items) { dialog, which ->

                sendLocationStatus(which)
                addStatusMeasurement()
            }
            .setCancelable(true)

        val alert = builder.create()
        alert.show()


    }

    private fun addStatusMeasurement () {


        var isNetworkOnline = 0.0
        if (mApp?.isNetworkAvailable() == true)
            isNetworkOnline = 1.0

        mApp?.cleanInsightsManager?.measureEvent("status-created",
            "online", isNetworkOnline)
    }


}
