package org.article19.circulo.next.notify.viewmodel

interface PushViewModel {
    fun registerPushToken(token: String)
}