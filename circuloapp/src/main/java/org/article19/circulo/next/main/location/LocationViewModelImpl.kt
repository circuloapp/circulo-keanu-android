package org.article19.circulo.next.main.location

import android.location.Location
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import info.guardianproject.keanuapp.viewmodel.BaseViewModel
import kotlinx.coroutines.launch
import org.article19.circulo.next.repository.location.LocationRepository
import javax.inject.Inject

@HiltViewModel
class LocationViewModelImpl @Inject constructor(
    private val locationRepository: LocationRepository)
    : BaseViewModel(), LocationViewModel {
    override fun getLocation(
        onSuccess: (location: Location) -> Unit,
        onFail: (e: Exception) -> Unit
    ) {
        viewModelScope.launch {
            locationRepository.getLocation(onSuccess, onFail)
        }
    }
}