package org.article19.circulo.next.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.preference.PreferenceManager
import org.acra.ACRA
import org.article19.circulo.next.CirculoApp
import org.article19.circulo.next.CleanInsightsManager
import org.article19.circulo.next.R
import org.article19.circulo.next.common.ui.SwitchButton
import org.article19.circulo.next.databinding.FragmentSettingFeedbackBinding

open class SettingFeedbackFragment : SettingBaseFragment() {

    private lateinit var mBinding: FragmentSettingFeedbackBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        mBinding = FragmentSettingFeedbackBinding.inflate(inflater, container, false)

        setupNavigation(mBinding.toolbar.tvBackText, mBinding.toolbar.tvTitle,
            getString(R.string.feedback), savedInstanceState)

        val prefShared = PreferenceManager.getDefaultSharedPreferences(requireContext())

        mBinding.sbSendDebugLog.isChecked = !prefShared.getBoolean(ACRA.PREF_DISABLE_ACRA, false)

        mBinding.sbSendDebugLog.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener {
            override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                prefShared.edit().putBoolean(ACRA.PREF_DISABLE_ACRA,!isChecked).apply()
            }
        })

        val cim = (requireActivity().application as? CirculoApp)?.cleanInsightsManager

        if (cim?.isConfigured == true) {
            mBinding.sbMeasurement.isChecked = cim.hasConsent
            mBinding.sbMeasurement.setOnCheckedChangeListener(object : SwitchButton.OnCheckedChangeListener{
                override fun onCheckedChanged(view: SwitchButton?, isChecked: Boolean) {
                    if (isChecked) {
                        cim.getConsent(requireActivity()) { hasConsented ->
                            mBinding.sbMeasurement.isChecked = hasConsented
                            mBinding.tvMeasurement.text = measurementSubtext(cim)
                        }
                    } else {
                        cim.stopConsent()

                        mBinding.tvMeasurement.text = measurementSubtext(cim)
                    }
                }
            })

            mBinding.tvMeasurement.text = measurementSubtext(cim)
        } else {
            mBinding.viewMeasurement.visibility = View.GONE
        }

        return mBinding.root
    }

    private fun measurementSubtext(cim: CleanInsightsManager): String {
        return if (cim.hasConsent) {
            getString(R.string.consent_given_to_measure_for_x_more_days, cim.remainingConsentDays)
        }
        else {
            getString(R.string.help_improve_the_app_with_confidential_usage_stats)
        }
    }
}