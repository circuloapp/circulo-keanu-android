package org.article19.circulo.next.repository.location.remote

import android.location.Location

interface LocationRemoteDataSource {
    suspend fun getLocation(onSuccess: (location: Location) -> Unit,
                            onFail: (e: Exception) -> Unit)
}