package org.article19.circulo.next.onboarding

import info.guardianproject.keanu.core.ui.onboarding.OnboardingProvider

object CirculoOnboardProvider: OnboardingProvider() {
    override val BASE_INVITE_URL = "https://encirculo.org/i/#"
    override val DEFAULT_SCHEME = "matrix"
    override val REQUEST_SCAN = 1111
}