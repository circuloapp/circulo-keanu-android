package org.article19.circulo.next.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import com.maxkeppeler.sheets.core.SheetStyle
import com.maxkeppeler.sheets.info.InfoSheet
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.util.extensions.betterMessage
import info.guardianproject.keanuapp.ui.qr.QrScanActivity
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import org.article19.circulo.next.BaseActivity
import org.article19.circulo.next.Config
import org.article19.circulo.next.R
import org.article19.circulo.next.common.utils.ToastUtils
import org.article19.circulo.next.databinding.ActivityJoinCircleBinding
import org.matrix.android.sdk.api.failure.Failure
import org.matrix.android.sdk.api.session.room.RoomSummaryQueryParams
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import timber.log.Timber

class JoinCircleActivity : BaseActivity() {

    private val TAG = "JoinCircleActivity"

    private lateinit var mViewBinding: ActivityJoinCircleBinding

    private val mApp: ImApp?
        get() = application as? ImApp

    private var mCurrentRoom: RoomSummary? = null

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO) + mCoroutineExceptionHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding = ActivityJoinCircleBinding.inflate(layoutInflater)
        setContentView(mViewBinding.root)

        mViewBinding.toolbar.tvBackText.visibility = View.VISIBLE
        mViewBinding.toolbar.tvBackText.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
        mViewBinding.toolbar.tvAction.visibility = View.VISIBLE
        mViewBinding.toolbar.tvAction.text = getString(R.string.Done)

        val editTextCircleLink = mViewBinding.editTextCircleLink

        mViewBinding.toolbar.tvAction.setOnClickListener {
            if (editTextCircleLink.text.isNotEmpty()) {
                Config.ENABLE_EMPTY_STATE = false
                joinRoom()
            }
        }

        mViewBinding.btnQr.setOnClickListener {
            startActivityForResult(Intent(this,QrScanActivity::class.java),9999)
        }

        editTextCircleLink.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {

                if (editTextCircleLink.text.isNotEmpty()) {
                    Config.ENABLE_EMPTY_STATE = false
                    joinRoom()
                }

                return@OnKeyListener true
            }
            false
        })

        if (intent?.dataString?.isNotEmpty() == true)
            editTextCircleLink.setText(intent?.dataString)
        if (intent?.data != null)
            editTextCircleLink.setText(intent?.data?.toString())



        queryCircles()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 9999 && resultCode == Activity.RESULT_OK) {

            var link = data?.getStringArrayListExtra("result")?.get(0)
            mViewBinding.editTextCircleLink.setText(link)
            if (link?.isNotEmpty() == true) {
                Config.ENABLE_EMPTY_STATE = false
                joinRoom()
            }
        }
    }

    fun queryCircles () {
        val builder = RoomSummaryQueryParams.Builder()
        builder.memberships = listOf(Membership.JOIN)
        val queryParams = builder.build()

        val listRooms = mApp?.matrixSession?.roomService()?.getRoomSummaries(queryParams)
        if (listRooms?.isEmpty() == false)
            mCurrentRoom = listRooms.get(0)
    }

    private val mCoroutineExceptionHandler = CoroutineExceptionHandler { coroutineContext, exception ->
        Log.e(TAG, exception.toString())
        if (exception is Failure.ServerError) {
            val httpCode = exception.httpCode
            val errorMessage = exception.betterMessage

            if (httpCode == 404 && "No known servers" == errorMessage) {
                ToastUtils.showToast(getString(R.string.error_message_circle_not_found), this)
            }
        }
    }

    private fun joinRoom () {
        var roomId = mViewBinding.editTextCircleLink.text.toString()

        if (roomId.isNotEmpty()) {
            mCoroutineScope.launch (mCoroutineExceptionHandler) {

                roomId = roomId.split("#")[1]

                if (mCurrentRoom == null) {
                    roomId = mApp?.matrixSession?.roomService()?.joinRoom(roomId).toString()

                    try {
                        finish()
                    }
                    catch (nfe: ClassNotFoundException) {
                        Timber.d(nfe, "")
                    }

                    startActivity((application as? ImApp)?.router?.main(this@JoinCircleActivity, preselectTab = MainActivity.TAB_CIRCLE))
                                    }
                else
                {
                    showLeaveCircleAlert(roomId)
                }


            }
        }

    }

    private fun showLeaveCircleAlert(newRoomId: String) {

        //val newRoom = mApp?.matrixSession?.getRoom(newRoomId)
        //val newRoomDisplayName = newRoom?.roomSummary()?.displayName

        val leaveCircleString =
            getString(R.string.leave_circle_title, mCurrentRoom?.displayName)
        val leaveCircleMessage = getString(R.string.leave_circle_message)
        val notNowString = getString(R.string.not_now)
        val yesString = getString(R.string.Yes)
        let {
            InfoSheet().show(it) {
                title(leaveCircleString)
                content(leaveCircleMessage)
                style(SheetStyle.DIALOG)
                onNegative(notNowString) {
                    dismiss()
                }
                onPositive(yesString) {

                    mCoroutineScope.launch {

                        //leave current room
                        mApp?.matrixSession?.roomService()?.leaveRoom(mCurrentRoom?.roomId!!,"")

                        //join new room
                        mApp?.matrixSession?.roomService()?.joinRoom(newRoomId).toString()
                    }

                    finish()
                }

            }
        }
    }

}