package org.article19.circulo.next.notify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // get data if you have passed them
        Bundle extras = intent.getExtras();

        // do something

    }
}
