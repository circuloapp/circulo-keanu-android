package org.article19.circulo.next.repository.push

import org.article19.circulo.next.repository.push.remote.PushRemoteRepositoryImpl
import javax.inject.Inject

class PushRepositoryImpl @Inject constructor(private val mRemoteRepository: PushRemoteRepositoryImpl)
    : PushRepository {
    override suspend fun registerPushToken(token: String) {
        mRemoteRepository.registerPushToken(token)
    }
}