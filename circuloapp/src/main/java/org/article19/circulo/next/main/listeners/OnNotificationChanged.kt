package org.article19.circulo.next.main.listeners

interface OnNotificationChanged {
    fun onChanged(enable: Boolean)
}