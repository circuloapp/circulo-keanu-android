package org.article19.circulo.next.main.listeners

import org.matrix.android.sdk.api.session.room.Room
import org.matrix.android.sdk.api.session.room.model.RoomSummary

interface OnCircleLoadedListener {
    fun onCircleLoaded(circle: Room?, circleSummary: RoomSummary?)
}