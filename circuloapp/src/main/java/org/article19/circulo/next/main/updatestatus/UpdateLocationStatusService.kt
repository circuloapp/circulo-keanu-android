package org.article19.circulo.next.main.updatestatus

import android.Manifest
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.app.job.JobInfo.PRIORITY_DEFAULT
import android.app.job.JobInfo.PRIORITY_MAX
import android.app.job.JobInfo.PRIORITY_MIN
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.BatteryManager
import android.os.Build
import android.os.IBinder
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.ServiceCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.Granularity
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.OnTokenCanceledListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.article19.circulo.next.CirculoApp.Companion.NOTIFICATION_CHANNEL_ID_LOCATION
import org.article19.circulo.next.CirculoRouter
import org.article19.circulo.next.R
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.main.MainActivity
import org.article19.circulo.next.main.now.Thread
import org.article19.circulo.next.notify.StatusBarNotifier
import org.matrix.android.sdk.api.session.events.model.Content
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.LocalEcho
import org.matrix.android.sdk.api.session.events.model.RelationType
import org.matrix.android.sdk.api.session.events.model.UnsignedData
import org.matrix.android.sdk.api.session.events.model.toContent
import org.matrix.android.sdk.api.session.room.getTimelineEvent
import org.matrix.android.sdk.api.session.room.model.message.LocationAsset
import org.matrix.android.sdk.api.session.room.model.message.LocationAssetType
import org.matrix.android.sdk.api.session.room.model.message.LocationInfo
import org.matrix.android.sdk.api.session.room.model.message.MessageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageLocationContent
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.isRootThread
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneOffset

import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import java.util.Timer
import java.util.TimerTask


class UpdateLocationStatusService : LifecycleService(), Observer<List<Thread>> {

    private lateinit var mLastLocation: Location

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private var mThreadId: String? = null

    private val tsFormat = SimpleDateFormat(FORMAT_LOCATION_EXPIRE, Locale.UK)

    private var totalTimeMs = 0L

    private var isUrgent = false

    private var notification_stop_id=9000

    companion object {

        const val FORMAT_LOCATION_EXPIRE = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        const val EXTRA_SEND_URGENT = "urgent"
        const val EXTRA_SEND_LOCATION = "location"
        const val EXTRA_TOTAL_TIME = "time"
        const val EXTRA_THREAD_ID = "eventid"
        const val EXTRA_TIME_INTERVAL_MIN = "interval"

        const val ACTION_SEND_URGENT = "org.article19.circulo.next.SEND_ALERT"
        const val ACTION_SEND_LOCATION = "org.article19.circulo.next.SEND_LOCATION"

        const val BATTERY_CHARGE_NOTIFICATION_THRESHOLD = 20
    }

    override fun onCreate() {
        super.onCreate()

        //make sure this has permission before you start

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED  || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

            val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            }

            val utc = TimeZone.getTimeZone("UTC");
            tsFormat.timeZone = utc


            startForeground()
            SharedTimeline.instance.getThreads().observe(this, this)
        }

    }

    private fun startForeground() {

        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val notifyPendingIntent = PendingIntent.getActivity(
            this, 0, notifyIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )

        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel()
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId )
        val notification = notificationBuilder.setOngoing(true)
            .setContentIntent(buildLaunchIntent())
            .setSmallIcon(R.drawable.ic_location)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setContentText(getString(R.string.circulo_location_updates))
            .setOngoing(true)
            .setAutoCancel(false)
            .build()
        startForeground(101, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String{
        val channelId = NOTIFICATION_CHANNEL_ID_LOCATION
        val channelName = getString(R.string.circulo_location_updates)
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT)
        chan.lightColor = Color.GREEN
        chan.importance = NotificationManager.IMPORTANCE_NONE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationProviderClient.flushLocations()
        fusedLocationProviderClient.removeLocationUpdates { mLocationCallback }
    }

    @Deprecated("Deprecated in Java")
    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED  || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {


            isUrgent = intent?.getBooleanExtra(EXTRA_SEND_URGENT, false) == true

            val totalTimeMin = intent?.getIntExtra(EXTRA_TOTAL_TIME, -1)
            startLocationUpdates()


            if (totalTimeMin != null && totalTimeMin > 0) {

                totalTimeMs = totalTimeMin * 60 * 1000L


                //run for specified time, then stop
                Timer().schedule(object : TimerTask() {
                    override fun run() {

                        fusedLocationProviderClient?.removeLocationUpdates { mLocationCallback }

                        showStopNotification()

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            stopForeground(Service.STOP_FOREGROUND_REMOVE)
                        } else {
                            stopForeground(true)
                        }

                        stopSelf()
                    }
                }, totalTimeMs) //wait this long in MS
            }
        }
    }

    private var UNIQUE_INT_PER_CALL = 3000

    private fun buildLaunchIntent () : PendingIntent? {
        val intent = CirculoRouter.instance.main(this)
        intent?.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP

        var pIntent : PendingIntent? = null

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pIntent = (PendingIntent.getActivity(this, UNIQUE_INT_PER_CALL++,
                intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE))
        }
        else {
            pIntent = (PendingIntent.getActivity(this, UNIQUE_INT_PER_CALL++,
                intent, PendingIntent.FLAG_UPDATE_CURRENT))
        }

        return pIntent
    }

    private fun showStopNotification ()
    {


        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID_LOCATION )
        val notification = notificationBuilder.setOngoing(true)
            .setContentIntent(buildLaunchIntent())
            .setSmallIcon(R.drawable.ic_location)
            .setCategory(Notification.CATEGORY_SERVICE)
            .setContentText(getString(R.string.your_history_is_available_to_your_circle_until_you_delete_it))
            .setContentTitle(getString(R.string.you_re_done_sharing_location))
            .setAutoCancel(true)
            .setOngoing(false)
            .build()

        (getSystemService(Context.NOTIFICATION_SERVICE) as? android.app.NotificationManager)?.notify(notification_stop_id++,notification)

    }

    private fun startLocationUpdates() {

        val builder = LocationSettingsRequest.Builder()
        builder.setAlwaysShow(true)
        builder.setNeedBle(false)

        val locationInterval = 100L
        val locationFastestInterval = 2000L
        val locationMaxWaitTime = 100L


        val locationRequestBuilder = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, locationInterval)
            .setWaitForAccurateLocation(false)
            .setMinUpdateIntervalMillis(locationFastestInterval)
            .setMaxUpdateDelayMillis(locationMaxWaitTime)
            .setGranularity(Granularity.GRANULARITY_PERMISSION_LEVEL)

        if (isUrgent) {
           locationRequestBuilder.setMinUpdateDistanceMeters(5F)
        }
        else
        {
            locationRequestBuilder.setMinUpdateDistanceMeters(10F)
        }

        builder.addLocationRequest(locationRequestBuilder.build())


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        fusedLocationProviderClient!!.requestLocationUpdates(
            locationRequestBuilder.build(),
            mLocationCallback,
            Looper.myLooper()!!)


    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            locationResult.lastLocation?.let { locationChanged(it) }

        }
    }

    fun locationChanged(location: Location) {
        mLastLocation = location

        mLocationUncertainty = mLastLocation.accuracy.toDouble()

        lifecycleScope.launch {

            sendStatus()

           checkAndSendBatteryLevel ()
        }

    }

    private fun checkAndSendBatteryLevel () {
        val batteryStatus: Intent? = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            registerReceiver(null, ifilter)
        }

        val batteryPct: Float? = batteryStatus?.let { intent ->
            val level: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale: Int = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            level * 100 / scale.toFloat()
        }

        if (batteryPct != null) {
            if (batteryPct < BATTERY_CHARGE_NOTIFICATION_THRESHOLD)
                sendReply(getString(R.string.battery_level) + " $batteryPct%")
        }

    }

    private var mLocationUncertainty = 0.00
    private fun sendStatus () {

        mLastLocation?.let {

            sendLocationEvent(mLastLocation.latitude,
                mLastLocation.longitude,
                mLocationUncertainty)


        }

        /**
        val statusText = StringBuffer()

        mLastLocation.let {
            //Log.i(TAG,"Location: ${location.latitude}  ${location.longitude}")
            statusText.append("#geo:${it.latitude},${it.longitude}")

            mLastStatusText = statusText.toString()

        if (mThreadId == null)
            SharedTimeline.instance.room?.sendService()?.sendTextMessage(mLastStatusText)
        else
        {
            SharedTimeline.instance.room?.relationService()?.replyInThread(mThreadId!!,mLastStatusText, MessageType.MSGTYPE_TEXT)
        }**/

    }

    private fun sendReply (msg: String) {
        if (mThreadId != null)
            SharedTimeline.instance.room?.relationService()?.replyInThread(mThreadId!!,msg, MessageType.MSGTYPE_TEXT)
    }

    override fun onChanged(value: List<Thread>) {

        synchronized(tsFormat) {
            if (mThreadId == null) {

                for (thread in value) {
                    val event = thread.events.firstOrNull() ?: continue

                    val eventId = event.eventId
                    if (eventId.contains("local")) continue

                    if (event.getLastMessageContent() is MessageLocationContent) {

                        if (event.senderInfo.userId?.equals(SharedTimeline.instance.myUserId) == true) {

                            if (event.root.originServerTs != null) {
                                val timeIsNow =
                                    Date().time - event.root.originServerTs!! < 1000 * 60

                                if (timeIsNow) {

                                    mThreadId = eventId

                                    sendReply(tsFormat.format(Date().time + totalTimeMs))

                                    checkAndSendBatteryLevel()
                                }

                                break
                            }
                        }
                    }
                }
            }
        }
    }

    fun sendLocationEvent(
        latitude: Double,
        longitude: Double,
        uncertainty: Double?
    ) {
        val geoUri = buildGeoUri(latitude, longitude, uncertainty)
        val assetType = LocationAssetType.SELF

        val relatesToContent : RelationDefaultContent

        if (mThreadId != null)
        {
            relatesToContent = generateReplyRelationContent(
                eventId = mThreadId!!,
                rootThreadEventId = mThreadId,
                showInThread = true
            )

            val content = MessageLocationContent(
                geoUri = geoUri,
                body = geoUri,
                unstableLocationInfo = LocationInfo(geoUri = geoUri, description = geoUri),
                unstableLocationAsset = LocationAsset(type = assetType),
                unstableTimestampMillis = Date().time,
                unstableText = geoUri,
                relatesTo = relatesToContent
            )

            SharedTimeline.instance.room?.sendService()
                ?.sendEvent(EventType.MESSAGE, content.toContent())
        }
        else {

            val content = MessageLocationContent(
                geoUri = geoUri,
                body = geoUri,
                unstableLocationInfo = LocationInfo(geoUri = geoUri, description = geoUri),
                unstableLocationAsset = LocationAsset(type = assetType),
                unstableTimestampMillis = Date().time,
                unstableText = geoUri
            )

            SharedTimeline.instance.room?.sendService()
                ?.sendEvent(EventType.MESSAGE, content.toContent())
        }

       // return createMessageEvent(SharedTimeline.instance.room!!.roomId, content, null)
    }

    private fun generateReplyRelationContent(eventId: String, rootThreadEventId: String? = null, showInThread: Boolean): RelationDefaultContent =
        rootThreadEventId?.let {
            RelationDefaultContent(
                type = RelationType.THREAD,
                eventId = it,
                isFallingBack = showInThread,
                // False when is a rich reply from within a thread, and true when is a reply that should be visible from threads
                inReplyTo = ReplyToContent(eventId = eventId)
            )
        } ?: RelationDefaultContent(null, null, ReplyToContent(eventId = eventId))

    fun createEvent(roomId: String, type: String, content: Content?, additionalContent: Content? = null): Event {

        val newContent = content
        val updatedNewContent = newContent?.plus(additionalContent.orEmpty()) ?: additionalContent
        val localId = LocalEcho.createLocalEchoId()
        return Event(
            roomId = roomId,
            originServerTs = Date().time,
            senderId = SharedTimeline.instance.myUserId,
            eventId = localId,
            type = type,
            content = updatedNewContent,
            unsignedData = UnsignedData(age = null, transactionId = localId)
        )
    }

    private fun buildGeoUri(latitude: Double, longitude: Double, uncertainty: Double?): String {
        return buildString {
            append("geo:")
            append(latitude)
            append(",")
            append(longitude)
            /**
            uncertainty?.let {
                append(";u=")
                append(it)
            }**/
        }
    }

}
