package org.article19.circulo.next.repository.push

interface PushRepository {
    suspend fun registerPushToken(token: String)

}