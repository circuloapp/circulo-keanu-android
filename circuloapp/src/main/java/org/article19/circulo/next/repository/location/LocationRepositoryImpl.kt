package org.article19.circulo.next.repository.location

import android.Manifest
import android.location.Location
import androidx.annotation.RequiresPermission
import org.article19.circulo.next.repository.location.remote.LocationRemoteDataSourceImpl
import javax.inject.Inject

class LocationRepositoryImpl @Inject constructor(
    private val remoteDataSource: LocationRemoteDataSourceImpl)
    : LocationRepository {

    @RequiresPermission(allOf = [
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION])
        override suspend fun getLocation(
        onSuccess: (location: Location) -> Unit,
        onFail: (e: Exception) -> Unit
    ) {
        remoteDataSource.getLocation(onSuccess, onFail)
    }
}