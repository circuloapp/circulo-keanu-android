package org.article19.circulo.next.mention

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.FontMetricsInt
import android.graphics.RectF
import android.text.style.ReplacementSpan
import android.util.TypedValue
import org.article19.circulo.next.CirculoApp
import kotlin.math.roundToInt


class RoundedBackgroundSpan(context: Context, private val backgroundColor: Int) : ReplacementSpan() {

    private val mContext = context
    private var textColor =
        getColorFromAttribute(org.article19.circulo.next.R.attr.messageTextPrimary)

    override fun draw(
        canvas: Canvas,
        text: CharSequence,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        val rect = RectF(
            x, top.toFloat(), x + measureText(paint, text, start, end),
            bottom.toFloat()
        )
        paint.color = backgroundColor
        canvas.drawRoundRect(rect, CORNER_RADIUS.toFloat(), CORNER_RADIUS.toFloat(), paint)
        paint.color = textColor
        canvas.drawText(text, start, end, x/* + 1F * PADDING*/, y.toFloat(), paint)
    }

    override fun getSize(
        paint: Paint,
        text: CharSequence,
        start: Int,
        end: Int,
        fm: FontMetricsInt?
    ): Int {
        return paint.measureText(text, start, end).roundToInt()
    }

    private fun measureText(paint: Paint, text: CharSequence, start: Int, end: Int): Float {
        return paint.measureText(text, start, end)/* + 2f * PADDING*/
    }

    companion object {
        private fun dp(value: Int): Int {
            return (CirculoApp.getInstance().applicationContext.resources.displayMetrics.density * value + 0.5f).toInt()
        }

        private var CORNER_RADIUS = dp(8)
//        private var PADDING = dp(5)
    }

    private fun getColorFromAttribute(attr: Int): Int {
        val typedValue = TypedValue()
        mContext.theme.resolveAttribute(attr, typedValue, true)
        return typedValue.data
    }
}