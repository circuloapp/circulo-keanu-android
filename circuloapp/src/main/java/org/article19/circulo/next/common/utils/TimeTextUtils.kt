package org.article19.circulo.next.common.utils

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ImageSpan
import info.guardianproject.keanu.core.util.PrettyTime
import org.article19.circulo.next.CirculoApp
import java.util.*

object TimeTextUtils {

    /**
     * Return message info including sender's name and message time.
     */
    fun formatMessageInfo(
        date: Date?, nickname: String?): SpannableString {
        val text = StringBuilder()

        if (nickname?.isNotEmpty() == true) {
            text.append(nickname)
            text.append(' ')
        }

        /**
        if (DateUtils.isToday(date!!.time)) {
        text.append(mTimeFormat.format(date))
        } else
         **/
        text.append(PrettyTime.format(date, CirculoApp.getInstance().applicationContext))

        val icons = ArrayList<Int>()

        /**
        if (isOutgoing) {
        if (isDelivered) {
        icons.add(R.drawable.ic_delivered_grey)
        } else {
        icons.add(R.drawable.ic_sent_grey)
        }
        }**/

        /**
        if (isEncrypted) {
        icons.add(R.drawable.ic_encrypted_grey)
        } else if (isRoomEncrypted) {
        icons.add(R.drawable.ic_message_wait_grey)
        }**/

        if (icons.isNotEmpty()) text.append(' ')

        repeat(icons.size) {
            text.append('X')
        }

        val spannable = SpannableString(text.toString())
        val pos = spannable.length - icons.size

        val context = CirculoApp.getInstance().applicationContext ?: return spannable

        repeat(icons.size) {
            spannable.setSpan(
                ImageSpan(context, icons[it]), pos + it, pos + it + 1,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        return spannable
    }
}