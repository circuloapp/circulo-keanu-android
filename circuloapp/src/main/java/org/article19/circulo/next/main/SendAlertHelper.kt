package org.article19.circulo.next.main


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaRecorder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import info.guardianproject.keanu.core.KeanuConstants.LOG_TAG
import info.guardianproject.keanu.core.util.AttachmentHelper
import org.article19.circulo.next.SharedTimeline
import org.article19.circulo.next.common.utils.MimeTypes
import org.article19.circulo.next.databinding.ActivityMainBinding
import org.article19.circulo.next.databinding.FragmentSendAlertBinding
import org.article19.circulo.next.main.now.Status.Companion.NOTSAFE_STRING
import org.article19.circulo.next.main.now.Status.Companion.URGENT_STRING
import org.article19.circulo.next.main.updatestatus.UpdateLocationStatusService
import org.article19.circulo.next.main.updatestatus.UpdateStatusActivity
import org.matrix.android.sdk.api.session.room.model.message.MessageTextContent
import org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent
import org.matrix.android.sdk.api.session.room.model.relation.ReplyToContent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Timer
import java.util.TimerTask
import org.article19.circulo.next.main.now.Thread
import org.matrix.android.sdk.api.session.events.model.RelationType
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import java.util.Date


class SendAlertHelper (val activity: MainActivity, val motionEvent: MotionEvent, val mBinding: ActivityMainBinding) : OnTouchListener, Observer<List<Thread>> {

    private var recorder: MediaRecorder? = null
    private var fileAudio: String? = null

    private var recordTime = 1000L;
    private var requestCodeAudio = 9998
    private  val timer = Timer();

    companion object {
        const val TAG = "Alert"

    }
    fun init()  {

        fileAudio = File.createTempFile("alert","m4a").absolutePath

        mBinding.alertButton?.setOnTouchListener(this)
        mBinding.alertButton?.dispatchTouchEvent(motionEvent)
        onTouch(mBinding.alertButton,motionEvent)

        if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(activity,arrayOf(Manifest.permission.RECORD_AUDIO),requestCodeAudio)
        }
        else {
            if (recorder == null)
                startRecording()
        }

    }

    fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            requestCodeAudio -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    startRecording()
                } else {
                    // Explain to the user that the feature is unavailable because
                    // the feature requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private var isRecording = false

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(fileAudio)
            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            isRecording = true
            start()
        }

        timer?.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                recordTime+=1000
                updateTimer()
            }
        }, 0, 1000)

    }
    private fun updateTimer() {
        activity?.runOnUiThread {
            mBinding.textAudioTimer.text = "${SimpleDateFormat("mm:ss").format(recordTime)}"
        }
    }


    private fun stopRecording() {
        timer.cancel()

        if (isRecording) {
            try {
                recorder?.apply {
                    stop()
                    release()
                }
            }
            catch (re : RuntimeException){}
            recorder = null
            isRecording = false
        }
    }

    override fun onTouch(view: View, event: MotionEvent): Boolean {


        val actualY = event.rawY-200

        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
             //   Toast.makeText(this,"touch down",Toast.LENGTH_SHORT).show()
            }

            MotionEvent.ACTION_MOVE -> {
                mBinding.alertButton.y = actualY
            }

            MotionEvent.ACTION_UP -> {

                stopRecording()


                if (event.rawY > mBinding.alertDelete.y)
                {
                    fileAudio?.let { File(it).delete() }
                    activity?.removeAlertFragment()

                }
                else
                {
                    sendAlert()

                }
            }

            else -> return false
        }

        return true
    }


    private fun sendAlert () {

        SharedTimeline.instance.getThreads().observe(activity, this)

        SharedTimeline.instance.room?.sendService()
            ?.sendTextMessage("$URGENT_STRING $NOTSAFE_STRING")




    }
    private fun startLocationShare () {

        if(ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

            val timeTotal = 60 * 4; //4 hours
            val intent = Intent(activity, UpdateLocationStatusService::class.java)
            intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_URGENT, true)
            intent.putExtra(UpdateLocationStatusService.EXTRA_TOTAL_TIME, timeTotal)
            intent.putExtra(UpdateLocationStatusService.EXTRA_SEND_LOCATION, true)
            ContextCompat.startForegroundService(activity.applicationContext, intent)
        }
    }

    var alertSent = false

    override fun onChanged(value: List<Thread>) {

        for (thread in value) {

            val event = thread.events.firstOrNull() ?: continue

            val eventId = event.eventId
            if (eventId.contains("local")) continue

            val mc = event.getLastMessageContent() as? MessageTextContent ?: continue

            val senderIsMe = event.senderInfo.userId == SharedTimeline.instance.myUserId

            if (event.root.originServerTs != null) {
                val timeIsNow = Date().time - event.root.originServerTs!! < 1000 * 60

                if (senderIsMe && timeIsNow && mc.body?.startsWith(URGENT_STRING) == true) {

                    synchronized(mc)
                    {
                        if (!alertSent) {

                            SharedTimeline.instance.getThreads().removeObserver(this)

                            var fileAudio = File(fileAudio)
                            if (fileAudio != null && fileAudio.exists()) {
                                val uriAudio = Uri.fromFile(fileAudio)
                                sendAttachment(uriAudio, MimeTypes.DEFAULT_AUDIO_FILE, eventId)
                            }

                            startLocationShare()

                            activity.showAlertControlFragment()

                            alertSent = true
                        }
                    }

                    activity?.removeAlertFragment()
                    break
                }
            }

        }
    }

    private fun sendAttachment(contentUri: Uri, fallbackMimeType: String? = null, replyId: String) {
        try {
            var mimeType = fallbackMimeType
            if (mimeType == null) {
                mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(contentUri.toString())
                )
            }

            val attachment =
                AttachmentHelper.createFromContentUri(activity.contentResolver, contentUri, mimeType)

            //    public abstract fun sendMedia(attachment: org.matrix.android.sdk.api.session.content.ContentAttachmentData,
            //    compressBeforeSending: kotlin.Boolean,
            //    roomIds: kotlin.collections.Set<kotlin.String>,
            //    rootThreadEventId: kotlin.String? /* = compiled code */,
            //    relatesTo: org.matrix.android.sdk.api.session.room.model.relation.RelationDefaultContent? /* = compiled code */,
            //    additionalContent: org.matrix.android.sdk.api.session.events.model.Content? /* = kotlin.collections.Map<kotlin.String, @kotlin.jvm.JvmSuppressWildcards kotlin.Any>? */ /* = compiled code */): org.matrix.android.sdk.api.util.Cancelable

            val room = SharedTimeline.instance.room

            var rdc = RelationDefaultContent(
                type = RelationType.THREAD,
                eventId = replyId,
                isFallingBack = true,
                // False when is a rich reply from within a thread, and true when is a reply that should be visible from threads
                inReplyTo = ReplyToContent(eventId = replyId)
            )

            room?.sendService()?.sendMedia(
                attachment,
                true,
                setOf(room.roomId),
                replyId,
                rdc
            )

        } catch (e: Exception) {
            Timber.e(e, "error sending file")
        }
    }




}