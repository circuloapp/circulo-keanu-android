package org.article19.circulo.next.settings.feedback

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.maxkeppeler.sheets.core.Sheet
import org.article19.circulo.next.R
import org.article19.circulo.next.databinding.SheetFeedbackConsentBinding

class FeedbackConsentSheet : Sheet() {

    private lateinit var mBinding: SheetFeedbackConsentBinding

    var isConsentGiven = false
    var groupCode: String? = null

    /**
     * Implement this method and add your own layout, which will be appended to the default sheet with toolbar and buttons.
     */
    override fun onCreateLayoutView(): View {
        mBinding = SheetFeedbackConsentBinding.inflate(LayoutInflater.from(activity))

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.rbYes.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mBinding.rbYes.buttonDrawable =
                    ContextCompat.getDrawable(windowContext, R.drawable.ic_check)
                isConsentGiven = true

                enableConfirmButton()
            }
            else {
                mBinding.rbYes.buttonDrawable = null
            }
        }

        mBinding.rbNo.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mBinding.rbNo.buttonDrawable =
                    ContextCompat.getDrawable(windowContext, R.drawable.ic_check)
                isConsentGiven = false

                enableConfirmButton()
            }
            else {
                mBinding.rbNo.buttonDrawable = null
            }
        }

        mBinding.btnConfirm.setOnClickListener {
            if (isConsentGiven) {
                GroupCodeSheet().show(windowContext) {
                    displayPositiveButton(false)
                    displayNegativeButton(false)
                    displayHandle(true)
                    onClose {
                        this@FeedbackConsentSheet.groupCode = groupCode
                        this@FeedbackConsentSheet.dismiss()
                    }
                }
            }
            else {
                dismiss()
            }
        }
    }

    private fun enableConfirmButton() {
        mBinding.btnConfirm.background =
            ContextCompat.getDrawable(windowContext, R.drawable.bg_button_rounded_purple)
        mBinding.btnConfirm.isClickable = true
        mBinding.btnConfirm.isFocusable = true
    }

    /** Build [FeedbackConsentSheet] and show it later. */
    fun build(ctx: Context, width: Int? = null, func: FeedbackConsentSheet.() -> Unit): FeedbackConsentSheet {
        this.windowContext = ctx
        this.width = width

        func()

        return this
    }

    /** Build and show [FeedbackConsentSheet] directly. */
    fun show(ctx: Context, width: Int? = null, func: FeedbackConsentSheet.() -> Unit): FeedbackConsentSheet {
        this.windowContext = ctx
        this.width = width

        func()
        show()

        return this
    }
}