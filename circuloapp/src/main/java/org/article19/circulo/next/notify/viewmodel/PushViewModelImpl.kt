package org.article19.circulo.next.notify.viewmodel

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import info.guardianproject.keanuapp.viewmodel.BaseViewModel
import kotlinx.coroutines.launch
import org.article19.circulo.next.repository.push.PushRepository
import javax.inject.Inject

@HiltViewModel
class PushViewModelImpl @Inject constructor(private val mPushRepository: PushRepository)
    : PushViewModel, BaseViewModel() {
    override fun registerPushToken(token: String) {
        viewModelScope.launch {
            mPushRepository.registerPushToken(token)
        }
    }
}