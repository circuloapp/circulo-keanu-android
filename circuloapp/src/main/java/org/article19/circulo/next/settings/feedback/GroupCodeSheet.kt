package org.article19.circulo.next.settings.feedback

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.maxkeppeler.sheets.core.Sheet
import org.article19.circulo.next.R
import org.article19.circulo.next.common.providers.PreferenceProvider
import org.article19.circulo.next.databinding.SheetGroupCodeBinding

class GroupCodeSheet : Sheet() {

    private lateinit var mBinding: SheetGroupCodeBinding

    val groupCode: String?
        get() {
            if (mBinding.etGroupCode.text.isNullOrBlank()) {
                return null
            }

            return mBinding.etGroupCode.text?.trim()?.toString()
        }

    /**
     * Implement this method and add your own layout, which will be appended to the default sheet with toolbar and buttons.
     */
    override fun onCreateLayoutView(): View {
        mBinding = SheetGroupCodeBinding.inflate(LayoutInflater.from(activity))

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.etGroupCode.setText(PreferenceProvider.ciGroupCode)
        mBinding.etGroupCode.addTextChangedListener {
            setButtonStates()
        }

        mBinding.btEnter.setOnClickListener {
            dismiss()
        }

        mBinding.btWithoutCode.setOnClickListener {
            dismiss()
        }

        setButtonStates()
    }

    private fun setButtonStates() {
        val hasCode = !mBinding.etGroupCode.text.isNullOrBlank()

        mBinding.btEnter.background = ContextCompat.getDrawable(windowContext,
            if (hasCode) R.drawable.bg_button_rounded_purple else R.drawable.bg_button_rounded_grey)

        mBinding.btEnter.isClickable = hasCode
        mBinding.btEnter.isFocusable = hasCode

        mBinding.btWithoutCode.background = ContextCompat.getDrawable(windowContext,
            if (!hasCode) R.drawable.bg_button_rounded_purple else R.drawable.bg_button_rounded_grey)

        mBinding.btWithoutCode.isClickable = !hasCode
        mBinding.btWithoutCode.isFocusable = !hasCode
    }

    /** Build and show [GroupCodeSheet] directly. */
    fun show(ctx: Context, width: Int? = null, func: GroupCodeSheet.() -> Unit): GroupCodeSheet {
        this.windowContext = ctx
        this.width = width

        func()
        show()

        return this
    }
}