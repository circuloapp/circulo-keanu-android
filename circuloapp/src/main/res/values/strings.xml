<resources>
    <string name="app_name">Círculo</string>

    <!-- Welcome screens -->
    <string name="welcome_screen_text">A safe place to connect with a small group of people</string>
    <string name="welcome_get_started_text">Get started</string>
    <string name="welcome_login_text">Login</string>

    <!-- Toolbar -->
    <string name="toolbar_arrow_text">Back</string>

    <!-- Create Profile -->
    <string name="create_profile_toolbar_title">Profile</string>
    <string name="create_profile_choose_name_description">Choose a name to use on Círculo.</string>
    <string name="create_profile_choose_name_edit_text_hint">Any name</string>
    <string name="create_profile_choose_name_explanation">It doesn’t have to be your real name. You can change this anytime.</string>


    <!-- Now -->
    <string name="now">Now</string>
    <string name="see_who_needs_help">See who needs help</string>
    <string name="main_now_description_text">Once you join a circle, you’ll see updates and calls for help here.</string>
    <plurals name="comment_counts">
        <item quantity="one">%d new comment</item>
        <item quantity="other">%d new comments</item>
    </plurals>
    <string name="last_comment_time">%d min</string>
    <string name="people_need_help">People need help</string>
    <string name="i_need_help">I need help</string>
    <string name="content_description_urgent">Urgent</string>

    <!-- Our Circle -->
    <string name="our_circle">Our Circle</string>
    <string name="form_your_circle">Form Your Circle</string>
    <string name="form_your_circle_description">A circle is made up of 6 people who agree to support each other.</string>
    <string name="join_a_circle">Join a Circle</string>
    <string name="or">Or</string>
    <string name="create_a_circle">Create a circle</string>
    <string name="message_invited_to_circle">You’ve been invited to the ‘%s’ circle.</string>
    <string name="message_leave_circle_to_join_title">"Leave ‘%s’ to join ‘%s’"</string>
    <string name="message_leave_circle_to_join_content">You’ve been invited to the ‘%s’ circle. Would you like to leave your current circle to join it?</string>
    <string name="info">Info</string>
    <string name="your_circle_is_secure">Your Circle is Secure</string>
    <string name="people_cannot_join_without_invite_link">People cannot join without the invite link.</string>
    <string name="content_description_add_member">Add a member</string>

    <!-- Join a circle -->
    <string name="join_circle_headline">To join a circle, open an invite link with Círculo or paste it below.</string>
    <string name="join_circle_description">Your circle agreement starts outside of this app. Talk with the people who will be in a circle together. One person from the group should create the circle and send an invite link to the others.</string>
    <string name="join_circle_hint">Paste a circle link</string>
    <string name="not_now">Not Now</string>
    <string name="join">Join</string>
    <string name="yes">Yes</string>
    <string name="leave_circle_direct_title">Leave ‘%s’?</string>
    <string name="leave_circle_title">Leave ‘%s’ to join?</string>
    <string name="leave_circle_message">Would you like to leave your current circle to join this new circle?</string>
    <string name="error_message_circle_not_found">The circle you tried to join cannot be reached</string>

    <!-- Profile -->
    <string name="profile">Profile</string>
    <string name="available">Available</string>
    <string name="edit_profile">Edit Profile</string>
    <string name="my_status">My Status</string>
    <string name="empty">Empty</string>
    <string name="notifications">Notifications</string>
    <string name="on">On</string>
    <string name="off">Off</string>
    <string name="physical_security">Physical Security</string>
    <string name="account">Account</string>
    <string name="settings">Settings</string>
    <string name="updating">Updating…</string>
    <string name="msg_clear_current_avatar_warning">This will clear your current avatar.\nAre you sure?</string>
    <string name="security_privacy">Security &amp; Privacy</string>
    <string name="preferences">Preferences</string>
    <string name="about">About</string>
    <string name="feedback">Feedback</string>
    <string name="physical_safety">Physical Safety</string>

    <!-- Settings -->
    <string name="when_to_notify_new_statuses">When to Notify: New Statuses</string>
    <string name="when_to_notify_anytime">Anytime someone asks for help</string>
    <string name="when_to_notify_urgent">Only if it’s urgent</string>

    <string name="when_to_notify_update_response">When to Notify: Updates &amp; Responses</string>
    <string name="anytime_someone_responds">Anytime someone responds</string>
    <string name="only_if_mentioned">Only if I’m mentioned</string>
    <string name="never">Never</string>

    <string name="customize_circulo_notifications">Customize Círculo Notifications</string>
    <string name="customize_urgent_notifications">Customize Urgent Notifications</string>
    <string name="vibrate">Vibrate</string>
    <string name="sound">Sound</string>
    <string name="ringtone">Ringtone</string>
    <string name="urgent_notification_description">If someone makes their status urgent, you will receive an urgent notification. </string>
    <string name="notification_description">If someone posts a new status or responds you will receive a normal Círculo notification. </string>
    <string name="notification_select_ringtone">Select ringtone for notifications</string>

    <string name="language">Language</string>
    <string name="general_tuning">General Tuning</string>
    <string name="start_automatically">Start Automatically</string>
    <string name="start_automatically_description">Always start and automatically sign into previously logged in accounts.</string>

    <string name="login_info">Login Info</string>
    <string name="change_password">Change Password</string>
    <string name="log_out">Log Out</string>

    <string name="setting_security_my_sessions_description">Where you’re logged in</string>
    <string name="cross_signing">Cross Signing</string>
    <string name="setting_security_cross_signing_description">Allows friends to trust new sessions connected to your ID without manually verifying</string>
    <string name="key_backup">Key Backup</string>
    <string name="key_backup_description">Allows you to sync chat history between devices and sessions.</string>
    <string name="block_screenshots">Block Screenshots</string>

    <string name="auto_lock">Auto Lock</string>
    <string name="disguise">Disguise</string>
    <string name="trigger_auto_protection">Trigger Auto-Protection</string>
    <string name="lock_app">Lock App</string>
    <string name="lock">Lock</string>
    <string name="pin_lock">Pin Lock</string>
    <string name="change_pin">Change Pin</string>
    <string name="use_custom_app_icon">Use a custom app icon</string>
    <string name="change_icon">Change icon</string>
    <string name="lock_screen_disable_error">Failed to disable lock screen</string>

    <string name="about_introduction">Círculo was created by the Guardian Project and ARTICLE 19, based on the experience, needs and concerns of women who occupy a central role in society by working in the journalistic activity, social initiatives and mobilizations for the defense of human rights.\n\nAt the heart of the app is wellness, ensuring that each journalist, activist and human rights defender is supported by a self-chosen community. This can help to mitigate burnout and trauma and consequently self-censorship in what are high-risk fields of work.</string>
    <string name="view_resources">View Resources</string>
    <string name="developer">Developer</string>
    <string name="contact_support">Contact Support</string>

    <string name="send_debug_logs">Send debug logs</string>
    <string name="measurement_study">Measurement Study</string>
    <string name="consent">Consent</string>

    <string name="settings_app_icon_chooser_title">App Icon</string>
    <string name="settings_app_icon_chooser_description">Select an app icon and name, which will be visible on your phone\'s home screen and app drawer.</string>
    <string name="app_icon_chooser_label_night_watch">Night Watch</string>
    <string name="app_icon_chooser_label_assistant">Assistant</string>
    <string name="app_icon_chooser_label_paint">Paint</string>
    <string name="app_icon_chooser_label_tetras">Tetras</string>
    <string name="app_icon_chooser_label_todo">To-Do</string>
    <string name="app_icon_chooser_label_fit_grit">FitGrit</string>
    <string name="app_icon_chooser_label_birdie">Birdie</string>

    <string name="version_label">Version</string>

    <!-- People status -->
    <string name="safe">Safe</string>
    <string name="not_safe">Not Safe</string>
    <string name="uncertain">Uncertain</string>
    <string name="tap_to_view_location">Tap to view location</string>
    <string name="sent">Sent</string>
    <string name="update">Update</string>
    <string name="my_condition">My Condition</string>
    <string name="help_now">Help now</string>
    <string name="status_new">New</string>
    <string name="send_to_my_circle">Send to my circle</string>
    <string name="content_description_voice_message">Voice message</string>
    <string name="content_description_add_media">Add media</string>
    <string name="seen_by">Seen by</string>
    <string name="latest">Latest</string>
    <string name="camera">Camera</string>
    <string name="gallery">Gallery</string>
    <string name="hint_update_status">What is your status?</string>

    <string-array name="quick_response_1">
        <item>Everything is OK.</item>
        <item>I’m being threatened.</item>
        <item>I’m scared for my safety.</item>
        <item>I’m fleeing.</item>
    </string-array>

    <string-array name="quick_response_2">
        <item>Help me.</item>
        <item>Call me.</item>
        <item>What should I do?</item>
        <item>Meet me.</item>
    </string-array>

    <!-- Circle Info -->
    <string name="delete_circle">Delete Circle</string>
    <string name="leave">Leave</string>
    <string name="admin_only">Admin only</string>
    <string name="members">Members</string>
    <string name="show_qr">Show QR</string>
    <string name="share_link">Share Link</string>
    <string name="open">Open</string>
    <string name="closed">Closed</string>
    <string name="anyone_with_a_link_can_join">Anyone with a link can join</string>
    <string name="only_people_added_in_your_circle">Only people added are in your circle</string>
    <string name="grant_moderator_permissions">Grant Moderator Permissions</string>
    <string name="join_status">Join status</string>
    <string name="edit_details">Edit Details</string>
    <string name="circle_info">Circle Info</string>
    <string name="qr_to_join_circle">QR to join this Circle</string>
    <string name="save">Save</string>
    <string name="hint_circle_name">Name your Circle</string>
    <string name="hint_circle_description">Describe your Circle</string>
    <string name="select_join_status_description">Open - Anyone with a link can join\nClosed - Only people added are in your circle</string>

    <string name="hello_">Hello, %s</string>
    <string name="status_set_to_uncertain">Status set to uncertain</string>
    <string name="status_set_to_not_safe">Status set to not safe</string>
    <string name="status_set_to_safe">Status set to safe</string>
    <string name="circle_created_by">Created by %s</string>
    <string name="resolve_title">Resolved</string>
    <string name="resolve_message">This circle concern been resolved.</string>
    <string name="no_active_statuses">No active statuses</string>
    <string name="are_you_sure">Are you sure?</string>
    <string name="logout_warning">This will clear all data and access to your circle. You will not be able to login again without knowing your password.</string>
    <string name="resolve">Resolve</string>

    <string name="crash_dialog_text">Submit your crash reports</string>
    <string name="delete_circle_warning">This will clear all messages and membership information of your circle.</string>

    <string name="reset_member_role">Reset to normal user</string>
    <string name="content_desc_admin">Admin</string>

    <!-- Clean Insights measurement -->
    <string name="clean_insight_consent_prompt">We are trying to understand how people are interacting with their circles, and would like to record a small bit of data in an anonymous, private way to help us learn more. Is that okay?</string>
    <string name="ci_title">Help us make Círculo better</string>
    <string name="ci_negative">Not now</string>
    <string name="ci_confirm">Gladly!</string>
    <string name="want_to_contribute">Want to contribute?</string>
    <string name="feedback_consent_description">Help us understand how Círculo is used. The study measures 5 things about your usage.</string>
    <string name="feedback_consent_description_2">Your location is never exposed.</string>
    <string name="response_time">Response time</string>
    <string name="how_long_status_is_active">How long a status is active</string>
    <string name="use_of_conditions">Use of conditions (safe, uncertain, etc)</string>
    <string name="frequency_location_sharing">Frequency of location sharing</string>
    <string name="if_messages_are_sent_offline">If messages are sent offline</string>
    <string name="you_can_opt_out_anytime">You can opt out of this study anytime.</string>

    <!-- Mention -->
    <string name="no_user">No user here!</string>
    <string name="sorry">Sorry!</string>
    <string name="status_notification_new">posted a new status</string>
    <string name="status_notification_updated">updated a status</string>
    <string name="status_notification_resolved">resolved their status</string>
    <string name="status_notification_geo">shared their location</string>
    <string name="help_improve_the_app_with_confidential_usage_stats">Help improve the app with confidential usage stats.</string>
    <string name="consent_given_to_measure_for_x_more_days">Consent given to measure for %d more days.</string>
    <string name="exit_app">Exit App</string>
    <string name="add_group_code">Add Group Code</string>
    <string name="if_you_have_joined_a_research_study_ask_the_facilitator_for_your_group_code">If you have joined a research study, ask the facilitator for your group code.</string>
    <string name="enter">Enter</string>
    <string name="contribute_without_a_code">Contribute Without a Code</string>
    <string name="enter_group_code">Enter group code</string>

    <string name="msg_no_internet">No internet</string>
    <string name="status_online">Online</string>
    <string name="share_location">Share Location?</string>
    <string name="send_alert">Send Alert?</string>
    <string name="release_to_send">Release to send</string>
    <string name="share_location_15_min">15 min</string>
    <string name="share_location_1_hr">1 hr</string>
    <string name="share_location_4_hrs">4 hrs</string>
    <string name="share_location_8_hrs">8 hrs</string>
    <string name="share_location_until_i_arrive">Until I arrive</string>
    <string name="circulo_location_updates">Circulo Location Updates</string>
    <string name="hint_recording_audio">Recording Audio</string>
    <string name="open_in_maps">Open in Maps</string>
    <string name="mark_seen">Mark Seen</string>
    <string name="i_m_okay">I\'m Okay</string>
    <string name="alert_from">"Alert from "</string>
    <string name="waiting_to_be_seen">Waiting to be seen...</string>
    <string name="name_your_circle">Name your circle</string>
    <string name="seen_by_1_people">Seen by %d people</string>
    <string name="you_re_done_sharing_location">You\'re done sharing location</string>
    <string name="your_history_is_available_to_your_circle_until_you_delete_it">Your history is available to your circle until you delete it</string>
    <string name="alert_active">Alert Active</string>
    <string name="action_got_it">Got It</string>
    <string name="tap_the_button_to_let_them_know_you_ve_seen_it">Tap the button to let them know you\'ve seen it.</string>
    <string name="minutes_s_left">minutes(s) left</string>
    <string name="min_s_left">min(s) left</string>
    <string name="hours">hour(s),</string>
    <string name="_0_min_left">0 min left</string>
    <string name="battery_level">Battery Level</string>
    <string name="are_you_sure_you_want_to_stop_sharing_your_location_with_your_circle">Are you sure you want to stop sharing your location with your circle?</string>
    <string name="stop_location_share">Stop Location Share</string>
    <string name="glad_you_re_okay">Glad you\'re okay</string>
    <string name="we_ll_notify_your_circle_and_close_the_alert">We\'ll notify your circle and close the alert</string>
    <string name="action_okay">Okay</string>
    <string name="not_okay">Not Okay</string>
    <string name="confirmation_sent">Confirmation Sent</string>
    <string name="would_you_like_to_share_a_crash_report">Would you like to share a crash report?</string>
    <string name="crash_report">Crash Report</string>
    <string name="ROOT_WARNING">WARNING: Your device is rooted, which can impact the security of this device and app.</string>
</resources>
