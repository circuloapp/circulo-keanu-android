package info.guardianproject.keanuapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

open class BaseViewModel: ViewModel() {
    protected val mErrorLiveData = MutableLiveData<Throwable>()
    val observableErrorLiveData = mErrorLiveData as LiveData<Throwable>

    protected val mExceptionHandler = object : CoroutineExceptionHandler {
        override val key: CoroutineContext.Key<*>
            get() = CoroutineExceptionHandler

        override fun handleException(context: CoroutineContext, exception: Throwable) {
            Timber.e(exception)
            mErrorLiveData.postValue(exception)
        }
    }
}