package info.guardianproject.keanuapp.viewmodel.qr

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.zxing.BarcodeFormat
import info.guardianproject.keanuapp.ui.qr.zxing.encode.Contents
import info.guardianproject.keanuapp.ui.qr.zxing.encode.QRCodeEncoder
import info.guardianproject.keanuapp.viewmodel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class QrViewModel: BaseViewModel() {
    private val mGetQrBitmapLiveData = MutableLiveData<Bitmap>()
    val observableGetQrBitmapLiveData = mGetQrBitmapLiveData as LiveData<Bitmap>

    fun getQrBitmap(qrCodeDimension: Int, qrData: String) {
        viewModelScope.launch (Dispatchers.IO + mExceptionHandler) {
            val qrCodeEncoder = QRCodeEncoder(qrData, null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                qrCodeDimension)
            mGetQrBitmapLiveData.postValue(qrCodeEncoder.encodeAsBitmap())
        }
    }
}