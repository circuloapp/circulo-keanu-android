/**
 *
 */
package info.guardianproject.keanuapp.ui.accounts;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import androidx.cursoradapter.widget.CursorAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import info.guardianproject.keanu.core.service.IImConnection;
import info.guardianproject.keanu.core.service.RemoteImService;

public class AccountAdapter extends CursorAdapter implements AccountListItem.SignInManager {

    private LayoutInflater mInflater;
    private int mResId;
    private Cursor mStashCursor;
    private AsyncTask<Void, Void, List<AccountSetting>> mBindTask;
    private Listener mListener;
    private Activity mActivity;


    private static Handler sHandler = new Handler()
    {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            //update notifications from async task
        }

    };

    public AccountAdapter(Activity context,
            LayoutInflater.Factory factory, int resId) {
        super(context, null, 0);
        mActivity = context;
        mInflater = LayoutInflater.from(context).cloneInContext(context);
        mInflater.setFactory(factory);
        mResId = resId;
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        if(mBindTask != null) {
            mBindTask.cancel(false);
            mBindTask = null ;
        }

        if (mStashCursor != null && (!mStashCursor.isClosed()))
                mStashCursor.close();

        mStashCursor = newCursor;

        if (mStashCursor != null) {
            // Delay swapping in the cursor until we get the extra info
           // List<AccountInfo> accountInfoList = getAccountInfoList(mStashCursor) ;
           // runBindTask((Activity)mContext, accountInfoList);
        }
        return super.swapCursor(mStashCursor);
    };

    /**
     * @param cursor
     * @return
     */
    private List<AccountInfo> getAccountInfoList(Cursor cursor) {
        List<AccountInfo> aiList = new ArrayList<AccountInfo>();
        cursor.moveToPosition(-1);
        while( cursor.moveToNext() ) {
            aiList.add( getAccountInfo(cursor));
        }
        return aiList;
    }

    static class AccountInfo {
        int providerId;
        String activeUserName;
        int dbConnectionStatus;
        int presenceStatus;
    }

    static class AccountSetting {
        String mProviderNameText;
        String mSecondRowText;
        boolean mSwitchOn;
        String activeUserName;
        int connectionStatus ;

        String domain;
        String host;
        int port;
        boolean isTor;

    }

    AccountInfo getAccountInfo( Cursor cursor ) {
        AccountInfo accountInfo = new AccountInfo();

        return accountInfo;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // create a custom view, so we can manage it ourselves. Mainly, we want to
        // initialize the widget views (by calling getViewById()) in newView() instead of in
        // bindView(), which can be called more often.
        AccountListItem view = (AccountListItem) mInflater.inflate(mResId, parent, false);
        boolean showLongName = true;
        view.init(mActivity, cursor, showLongName, this);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((AccountListItem) view).bindView(cursor);

    }

    private void runBindTask( final Activity context, final List<AccountInfo> accountInfoList ) {
        final Resources resources = context.getResources();
        final ContentResolver resolver = context.getContentResolver();

        // if called multiple times
        if (mBindTask != null)
            mBindTask.cancel(false);
        //


        mBindTask = new AsyncTask<Void, Void, List<AccountSetting>>() {

            @Override
            protected List<AccountSetting> doInBackground(Void... params) {
                List<AccountSetting> accountSettingList = new ArrayList<AccountSetting>();
                for( AccountInfo ai : accountInfoList ) {
                    accountSettingList.add( getAccountSettings(ai) );
                }
                return accountSettingList;
            }

            private AccountSetting getAccountSettings(AccountInfo ai) {
                AccountSetting as = new AccountSetting();



                return as;
            }

            @Override
            protected void onPostExecute(List<AccountSetting> result) {
                // store
                mBindTask = null;
                // swap
                AccountAdapter.super.swapCursor(mStashCursor);
                if (mListener != null)
                    mListener.onPopulate();
            }
        };
        mBindTask.execute();
    }

    public interface Listener {
        void onPopulate();
    }

    public void signIn(long providerId, long accountId) {
        if (accountId <= 0) {
            return;
        }


    }


    public void signOut(final long providerId, final long accountId) {

    }

    private void setKeepSignedIn(final long accountId, boolean signin) {

    }

    static final int PROVIDER_ID_COLUMN = 0;
    static final int PROVIDER_NAME_COLUMN = 1;
    static final int PROVIDER_FULLNAME_COLUMN = 2;
    static final int PROVIDER_CATEGORY_COLUMN = 3;
    static final int ACTIVE_ACCOUNT_ID_COLUMN = 4;
    static final int ACTIVE_ACCOUNT_USERNAME_COLUMN = 5;
    static final int ACTIVE_ACCOUNT_PW_COLUMN = 6;
    static final int ACTIVE_ACCOUNT_LOCKED = 7;
    static final int ACTIVE_ACCOUNT_KEEP_SIGNED_IN = 8;
    static final int ACCOUNT_PRESENCE_STATUS = 9;
    static final int ACCOUNT_CONNECTION_STATUS = 10;
}