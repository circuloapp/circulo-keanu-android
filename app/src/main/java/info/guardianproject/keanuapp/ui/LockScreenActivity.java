package info.guardianproject.keanuapp.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.UUID;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.cacheword.CacheWordHandler;
import info.guardianproject.keanu.core.cacheword.ICacheWordSubscriber;
import info.guardianproject.keanu.core.cacheword.PassphraseSecrets;
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.databinding.ActivityLockScreenBinding;
import timber.log.Timber;

public class LockScreenActivity extends BaseActivity implements ICacheWordSubscriber {
    private final static int MIN_PASS_LENGTH = 4;
    private CacheWordHandler mCacheWord;
    private String mPasswordError;
    private TwoViewSlider mSlider;
    private ImApp mApp;

    public static final String ACTION_CHANGE_PASSPHRASE = "cp";

    private final Handler mHandler = new Handler();
    private String mAction = "unlock";
    private ActivityLockScreenBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = (ImApp) getApplication();
        mBinding = ActivityLockScreenBinding.inflate(getLayoutInflater());

        setContentView(mBinding.getRoot());

        if (getIntent() != null && getIntent().getAction() != null)
            mAction = getIntent().getAction();

        mCacheWord = new CacheWordHandler(mApp, this);
        mCacheWord.connectToService();

        mSlider = new TwoViewSlider(mBinding.viewFlipper1, mBinding.flipView1, mBinding.flipView2, mBinding.editNewPassphrase, mBinding.editConfirmNewPassphrase);

        mBinding.btnSkip.setOnClickListener(v -> mHandler.post(() -> LockScreenActivity.this.finish()));

        if (mAction.equals(ACTION_CHANGE_PASSPHRASE)) {
            changePassphrase();
        } else {
            promptPassphrase();
        }
        final PreferenceProvider preferenceProvider = new PreferenceProvider(getApplicationContext());
        int themeColorBg = preferenceProvider.getBackgroundColor();

        if (themeColorBg != -1)
            mBinding.llRoot.setBackgroundColor(themeColorBg);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCacheWord.disconnectFromService();
    }

    private boolean newEqualsConfirmation() {
        return mBinding.editNewPassphrase.getText().toString()
                .equals(mBinding.editConfirmNewPassphrase.getText().toString());
    }

    private void showValidationError() {
        Toast.makeText(LockScreenActivity.this, mPasswordError, Toast.LENGTH_LONG).show();
        mBinding.editNewPassphrase.requestFocus();
    }

    private void showInequalityError() {
        Toast.makeText(LockScreenActivity.this,
                getString(R.string.lock_screen_passphrases_not_matching),
                Toast.LENGTH_SHORT).show();
        clearNewFields();
    }

    private void clearNewFields() {
        mBinding.editNewPassphrase.getEditableText().clear();
        mBinding.editConfirmNewPassphrase.getEditableText().clear();
    }

    private boolean isPasswordValid() {
        return validatePassword(mBinding.editNewPassphrase.getText().toString().toCharArray());
    }

    private boolean isPasswordFieldEmpty() {
        return mBinding.editNewPassphrase.getText().toString().length() == 0;
    }

    private boolean isConfirmationFieldEmpty() {
        return mBinding.editConfirmNewPassphrase.getText().toString().length() == 0;
    }

    private void initializeWithPassphrase() {
        try {
            String passphrase = mBinding.editNewPassphrase.getText().toString();
            if (!passphrase.isEmpty()) {

                PassphraseSecrets p = (PassphraseSecrets) mCacheWord.getCachedSecrets();
                mCacheWord.changePassphrase(p, passphrase.toCharArray());

                //now remove the temp passphrase if it exists
                finish();
            }

        } catch (Exception e) {
            Timber.e(e);
        }
    }

    private void changePassphrase() {
        // Passphrase is not set, so allow the user to create one

        mBinding.llCreatePassphrase.setVisibility(View.VISIBLE);
        mBinding.llEnterPassphrase.setVisibility(View.GONE);

        mBinding.editNewPassphrase.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
                if (isPasswordFieldEmpty()) {
                    resetPassphrase();
                    finish();
                } else if (!isPasswordValid())
                    showValidationError();
                else
                    mSlider.showConfirmationField();
            }
            return false;
        });

        mBinding.editConfirmNewPassphrase.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_DONE) {
                if (!newEqualsConfirmation()) {
                    showInequalityError();
                    mSlider.showNewPasswordField();
                }
            }
            return false;
        });

        mBinding.btnCreate.setOnClickListener(v -> {
            // validate
            if (isPasswordFieldEmpty()) {
                resetPassphrase();
                finish();
            } else if (!isPasswordValid()) {
                showValidationError();
                mSlider.showNewPasswordField();
            } else if (isConfirmationFieldEmpty() && !isPasswordFieldEmpty()) {
                mBinding.btnSkip.setVisibility(View.GONE);
                mSlider.showConfirmationField();
                mBinding.btnCreate.setText(R.string.lock_screen_confirm_passphrase);
            } else if (!newEqualsConfirmation()) {
                showInequalityError();
            } else if (!isConfirmationFieldEmpty() && !isPasswordFieldEmpty()) {
                initializeWithPassphrase();
            }
        });


    }

    private void promptPassphrase() {
        mBinding.llCreatePassphrase.setVisibility(View.GONE);
        mBinding.llEnterPassphrase.setVisibility(View.VISIBLE);

        mBinding.editEnterPassphrase.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_NULL || actionId == EditorInfo.IME_ACTION_GO) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    Handler threadHandler = new Handler();
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0, new ResultReceiver(
                            threadHandler) {
                        @Override
                        protected void onReceiveResult(int resultCode, Bundle resultData) {
                            super.onReceiveResult(resultCode, resultData);

                            if (mBinding.editEnterPassphrase.getText().toString().length() == 0)
                                return;
                            // Check passphrase
                            try {
                                char[] passphrase = mBinding.editEnterPassphrase.getText().toString().toCharArray();

                                mCacheWord.setPassphrase(passphrase);
                            } catch (Exception e) {
                                mBinding.editEnterPassphrase.setText("");
                                // TODO implement try again and wipe if fail
                                Timber.e(e);
                            }

                        }
                    });
                    return true;
                }
                return false;
            }
        });
    }

    private boolean validatePassword(char[] pass) {

        if (pass.length < MIN_PASS_LENGTH && pass.length != 0) {
            // should we support some user string message here?
            mPasswordError = getString(R.string.pass_err_length);
            return false;
        }

        return true;
    }

    public class TwoViewSlider {

        private boolean firstIsShown = true;
        private ViewFlipper flipper;
        private LinearLayout container1;
        private LinearLayout container2;
        private View firstView;
        private View secondView;
        private Animation pushRightIn;
        private Animation pushRightOut;
        private Animation pushLeftIn;
        private Animation pushLeftOut;

        public TwoViewSlider(ViewFlipper flipper, LinearLayout container1, LinearLayout container2,
                             View view1, View view2) {
            this.flipper = flipper;
            this.container1 = container1;
            this.container2 = container2;
            this.firstView = view1;
            this.secondView = view2;

            pushRightIn = AnimationUtils.loadAnimation(LockScreenActivity.this, R.anim.push_right_in);
            pushRightOut = AnimationUtils.loadAnimation(LockScreenActivity.this, R.anim.push_right_out);
            pushLeftIn = AnimationUtils.loadAnimation(LockScreenActivity.this, R.anim.push_left_in);
            pushLeftOut = AnimationUtils.loadAnimation(LockScreenActivity.this, R.anim.push_left_out);

        }

        public void showNewPasswordField() {
            if (firstIsShown)
                return;

            flipper.setInAnimation(pushRightIn);
            flipper.setOutAnimation(pushRightOut);
            flip();
        }

        public void showConfirmationField() {
            if (!firstIsShown)
                return;

            flipper.setInAnimation(pushLeftIn);
            flipper.setOutAnimation(pushLeftOut);
            flip();
        }

        private void flip() {
            if (firstIsShown) {
                firstIsShown = false;
                container2.removeAllViews();
                container2.addView(secondView);
            } else {
                firstIsShown = true;
                container1.removeAllViews();
                container1.addView(firstView);
            }
            flipper.showNext();
        }
    }

    @Override
    public void onCacheWordUninitialized() {

        //this should never happen

    }

    @Override
    public void onCacheWordLocked() {
        promptPassphrase();
    }

    @Override
    public void onCacheWordOpened() {

        if (mAction.equals("unlock")) {
            setResult(RESULT_OK);
            finish();
        }
    }

    void resetPassphrase() {
        //set temporary passphrase
        try {
            PassphraseSecrets p = (PassphraseSecrets) mCacheWord.getCachedSecrets();

            if (p != null) {
                String tempPassphrase = UUID.randomUUID().toString();
                mCacheWord.changePassphrase(p, tempPassphrase.toCharArray());
            }

        } catch (Exception e) {
            Timber.e(e);
        }
    }
}