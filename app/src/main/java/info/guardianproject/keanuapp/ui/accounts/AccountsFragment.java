package info.guardianproject.keanuapp.ui.accounts;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.ListFragment;
import androidx.loader.app.LoaderManager.LoaderCallbacks;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import info.guardianproject.keanuapp.R;

import static info.guardianproject.keanu.core.KeanuConstants.IMPS_CATEGORY;

public class AccountsFragment extends ListFragment {

        private FragmentActivity mActivity;
        private int mAccountLayoutView;

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

            mActivity = (FragmentActivity)activity;

            mAccountLayoutView = R.layout.account_list_item;

            initProviderCursor();


        }

        @Override
        public void onDetach() {
            super.onDetach();
            mActivity = null;
        }

        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
        }



    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        // Get the list
        ListView list = (ListView)v;

        // Get the list item position
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        int position = info.position;

        // Now you can do whatever.. (Example, load different menus for different items)
        //list.getItem(position);account_view

        menu.add("One");
        menu.add("Two");
        menu.add("Three");

    }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {


            return super.onCreateView(inflater, container, savedInstanceState);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            super.onViewCreated(view, savedInstanceState);
        }

        private void initProviderCursor()
        {

        }


        private class ProviderListItemFactory implements LayoutInflater.Factory {
            public View onCreateView(String name, Context context, AttributeSet attrs) {
                if (name != null && name.equals(AccountListItem.class.getName())) {
                    return new AccountListItem(context,attrs);
                }
                return null;
            }
        }





    }