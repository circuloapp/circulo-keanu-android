package info.guardianproject.keanuapp.ui.widgets;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.util.ActivityCompatUtils;
import info.guardianproject.keanu.core.util.SecureMediaStore;
import info.guardianproject.keanuapp.R;

public class WebViewActivity extends AppCompatActivity {

    public static final String EXTRA_EVENT_ID = "id";


    private boolean mShowResend = false;
    private Uri mMediaUri = null;
    private String mMimeType = null;
    private String mMessageId = null;

    private WebView mWebView = null;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
       // supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
       // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mShowResend = getIntent().getBooleanExtra("showResend",false);

        //setContentView(R.layout.image_view_activity);
        getSupportActionBar().show();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle("");

        setContentView(R.layout.activity_web_viewer);

        mWebView = findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.getSettings().setAllowFileAccess(false);
        mWebView.getSettings().setAllowContentAccess(false);
        mWebView.getSettings().setAllowFileAccessFromFileURLs(false);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(false);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
     //   mWebView.getSettings().setLoadWithOverviewMode(true);
    //    mWebView.getSettings().setUseWideViewPort(true);

        mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        // chromium, enable hardware acceleration
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mWebView.getSettings().setSafeBrowsingEnabled(true);
        }

        mMediaUri = getIntent().getData();
        mMimeType = getIntent().getType();
        mMessageId = getIntent().getStringExtra(EXTRA_EVENT_ID);

        InputStream is;

        if (mMediaUri.getScheme() == null || mMediaUri.getScheme().equals("vfs"))
        {
            try {
                is = (new FileInputStream(mMediaUri.getPath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }
        else
        {
            try {
                is = (getContentResolver().openInputStream(mMediaUri));

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        if (is != null)
        {
            try
            {

                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                is.close();
                String html = new String(buffer);
                mWebView.loadData(html, mMimeType, "UTF-8");

            }
            catch (IOException ioe)
            {
                ioe.printStackTrace();
            }

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_message_context, menu);

        menu.findItem(R.id.menu_message_copy).setVisible(false);
        menu.findItem(R.id.menu_message_resend).setVisible(mShowResend);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
            return true;
        } else if (itemId == R.id.menu_message_forward) {
            forwardMediaFile();
            return true;
        } else if (itemId == R.id.menu_message_share) {
            exportMediaFile();
            return true;
        } else if (itemId == R.id.menu_message_resend) {
            resendMediaFile();
            return true;
        } else if (itemId == R.id.menu_message_delete) {
            deleteMediaFile();
            return true;
        } else if (itemId == R.id.menu_message_nearby) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkPermissions () {
        boolean hasWritePermission = ActivityCompatUtils.INSTANCE.hasPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!hasWritePermission) {
            ActivityCompatUtils.INSTANCE.requestWriteExternalStorage(this, 1);
            return false;
        }

        return true;
    }

    private void deleteMediaFile () {
        Uri deleteUri = mMediaUri;
        if (deleteUri.getScheme() != null && deleteUri.getScheme().equals("vfs"))
        {
            java.io.File fileMedia = new java.io.File(deleteUri.getPath());
            fileMedia.delete();
        }

        //Imps.deleteMessageInDb(getContentResolver(), mMessageId);
        //TODO Delete
        setResult(RESULT_OK);
        finish();
    }

    public void exportMediaFile ()
    { if (checkPermissions()) {

            java.io.File exportPath = SecureMediaStore.exportPath(this,mMimeType, mMediaUri,true);
            exportMediaFile(mMimeType, mMediaUri, exportPath);

    }
    }

    private void exportMediaFile (String mimeType, Uri mediaUri, java.io.File exportPath)
    {
        try {

            SecureMediaStore.exportContent(mimeType, mediaUri, exportPath);
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(exportPath));
            shareIntent.setType(mimeType);
            startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.export_media)));
        } catch (IOException e) {
            Toast.makeText(this, "Export Failed " + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void forwardMediaFile ()
    {
        startActivity(((ImApp) getApplication()).getRouter().router(this, mMediaUri, mMimeType));
    }

    private void resendMediaFile ()
    {
        Intent intentResult = new Intent();
        intentResult.putExtra("resendImageUri",mMediaUri);
        intentResult.putExtra("resendImageMimeType",mMimeType);
        setResult(RESULT_OK,intentResult);
        finish();

    }

}
