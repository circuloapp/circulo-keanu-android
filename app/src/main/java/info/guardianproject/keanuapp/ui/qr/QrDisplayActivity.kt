package info.guardianproject.keanuapp.ui.qr

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.lifecycle.ViewModelProvider
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.viewmodel.qr.QrViewModel

@Suppress("deprecation")
class QrDisplayActivity : BaseActivity() {

    private lateinit var mQrCodeViewModel: QrViewModel
    private lateinit var mIvQrCode: ImageView

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mQrCodeViewModel = ViewModelProvider(this)[QrViewModel::class.java]
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        supportActionBar?.hide()
        val qrData = intent.getStringExtra(Intent.EXTRA_TEXT) ?: ""
        mIvQrCode = ImageView(this)
        mIvQrCode.scaleType = ImageView.ScaleType.FIT_CENTER
        mIvQrCode.setBackgroundColor(Color.WHITE)
        mIvQrCode.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT, 1f
        )
        val display = windowManager.defaultDisplay
        val portrait = display.width < display.height
        val layoutMain = LinearLayout(this)
        if (portrait) layoutMain.orientation =
            LinearLayout.VERTICAL else layoutMain.orientation = LinearLayout.HORIZONTAL
        layoutMain.weightSum = 1f
        layoutMain.addView(mIvQrCode)
        setContentView(layoutMain)
        mQrCodeViewModel.getQrBitmap(240, qrData)
        observeLiveData()
    }

    private fun observeLiveData() {
        mQrCodeViewModel.observableGetQrBitmapLiveData.observe(this) {
            mIvQrCode.setImageBitmap(it)
        }
    }
}