/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.guardianproject.keanuapp.ui;

import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.Collection;

import info.guardianproject.keanu.core.ImApp;
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider;
import info.guardianproject.keanu.core.util.StickerHelper;
import info.guardianproject.keanuapp.R;
import info.guardianproject.keanuapp.ui.stickers.StickerGroup;
import info.guardianproject.keanuapp.ui.stickers.StickerManager;
import info.guardianproject.keanuapp.ui.stickers.StickerPagerAdapter;

public class StickerActivity extends BaseActivity {


    private ImApp mApp;
    private ViewPager mStickerPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.awesome_activity_stickers);
        setTitle(R.string.action_stickers);
        mApp = (ImApp) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mStickerPager = findViewById(R.id.stickerPager);

        applyStyleForToolbar();

        initStickers();
    }


    public void applyStyleForToolbar() {


        //not set color
        final PreferenceProvider preferenceProvider = new PreferenceProvider(getApplicationContext());
        int selColor = preferenceProvider.getHeaderColor();

        if (selColor != -1) {
            getWindow().setNavigationBarColor(selColor);
            getWindow().setStatusBarColor(selColor);

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(selColor));
        }

    }

    private void initStickers() {

        Collection<StickerGroup> emojiGroups = StickerManager.getInstance(this).getEmojiGroups();

        if (!emojiGroups.isEmpty()) {
            StickerPagerAdapter emojiPagerAdapter = new StickerPagerAdapter(this, new ArrayList<>(emojiGroups),
                    s -> exportAsset(s.assetUri));

            mStickerPager.setAdapter(emojiPagerAdapter);

        }


    }

    private final static int MY_PERMISSIONS_REQUEST_FILE = 1;

    private void exportAsset(Uri mediaUri) {
        String mimeType = "image/png";
        Uri contentUri = StickerHelper.INSTANCE.createShareStickerUri(getBaseContext(), mediaUri);
        startActivity(mApp.getRouter().router(this, contentUri, mimeType));
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);


    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

}
