@file:Suppress("DEPRECATION")

package info.guardianproject.keanuapp.ui.qr

import android.hardware.Camera
import android.hardware.Camera.PreviewCallback
import com.google.zxing.BinaryBitmap
import com.google.zxing.LuminanceSource
import com.google.zxing.PlanarYUVLuminanceSource
import com.google.zxing.Reader
import com.google.zxing.ReaderException
import com.google.zxing.Result
import com.google.zxing.common.HybridBinarizer
import com.google.zxing.qrcode.QRCodeReader
import timber.log.Timber
import java.util.ArrayDeque

class QrCodeDecoder(private val callback: ResultCallback) : PreviewConsumer, PreviewCallback {
	private val reader: Reader = QRCodeReader()
	private var stopped = false
	private val mPreviewArray = ArrayDeque<PreviewFrame>()
	private var previewSize: Camera.Size? = null
	private var camera: Camera? = null

	override fun start(camera: Camera) {
		Timber.d("Started")
		stopped = false
		this.camera = camera
		askForPreviewFrame(camera)
		previewSize = camera.parameters.previewSize
		Thread(DecoderTask()).start()
	}

	override fun stop() {
		Timber.d("Stopped")
		stopped = true
		camera?.setPreviewCallback(null)
	}

	private fun askForPreviewFrame(camera: Camera) {
		if (!stopped) camera.setPreviewCallback(this)
	}

	override fun onPreviewFrame(data: ByteArray, camera: Camera) {
		val cameraSize = previewSize ?: return
		if (!stopped) {
			val frame = PreviewFrame(data, cameraSize.width, cameraSize.height)
			mPreviewArray.add(frame)
		}
	}

	private inner class PreviewFrame(val data: ByteArray, val width: Int, val height: Int)

	private inner class DecoderTask : Runnable {
		override fun run() {
			var frame: PreviewFrame?
			while (!stopped) {
				frame = mPreviewArray.poll()
				if (frame != null) {
					val now = System.currentTimeMillis()
					val src: LuminanceSource = PlanarYUVLuminanceSource(
						frame.data, frame.width,
						frame.height, 0, 0, frame.width, frame.height, false
					)
					val bitmap = BinaryBitmap(HybridBinarizer(src))
					val result: Result? = try {
						reader.decode(bitmap)
					} catch (e: ReaderException) {
						continue
					} catch (e2: OutOfMemoryError) {
						continue
					} catch (e3: NullPointerException) {
						continue
					} finally {
						reader.reset()
					}
					System.gc()
					val duration = System.currentTimeMillis() - now
					Timber.d("Decoding barcode took " + duration + " ms")
					callback.handleResult(result)
				} else {
					try {
						Thread.sleep(1000)
					} catch (e: Exception) {
						Timber.e(e)
					}
				}
			}
		}
	}

	interface ResultCallback {
		fun handleResult(result: Result?)
	}
}