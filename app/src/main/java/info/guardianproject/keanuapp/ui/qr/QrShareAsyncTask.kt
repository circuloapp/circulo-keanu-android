package info.guardianproject.keanuapp.ui.qr

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Build
import android.view.WindowInsets
import androidx.core.content.FileProvider
import com.google.zxing.BarcodeFormat
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.qr.zxing.encode.Contents
import info.guardianproject.keanuapp.ui.qr.zxing.encode.QRCodeEncoder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File


/**
 * Creates a QR code from a given invite link. Sends an `ACTION_SEND` intent, when done,
 * so people can share a link to a room or the like with others via another messenger or mail or
 * the like.
 *
 * This replaces the deprecated AsyncTask implementation with a Kotlin Coroutine implementation.
 */
class QrShareAsyncTask {
    @SuppressLint("SetWorldReadable")
    suspend fun shareQrCode(activity: Activity, inviteLink: String) {
        val size = getSize(activity)

        val message = "${inviteLink}\n\n${activity.getString(R.string.action_tap_invite)}"

        @Suppress("BlockingMethodInNonBlockingContext")
        val file = withContext(Dispatchers.IO) {
            val encoder = QRCodeEncoder(inviteLink, null, Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    if (size.x < size.y) size.x else size.y)

            val qrCode = encoder.encodeAsBitmap()

            val file = File(activity.cacheDir, "qr.png")
            file.outputStream().use { os ->
                qrCode.compress(Bitmap.CompressFormat.PNG, 100, os)

                os.flush()
            }

            return@withContext file
        }

        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "image/png"

            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_GRANT_READ_URI_PERMISSION

            putExtra(Intent.EXTRA_TEXT, message)

            val uri = FileProvider.getUriForFile(activity, "${activity.packageName}.provider", file)

            putExtra(Intent.EXTRA_STREAM, uri)

            clipData = ClipData.newUri(activity.contentResolver, activity.getString(R.string.app_name), uri)
        }

        activity.startActivity(intent)
    }

    @Suppress("DEPRECATION")
    private fun getSize(activity: Activity): Point {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            val size = Point()
            activity.windowManager.defaultDisplay.getSize(size)

            return size
        }

        val metrics = activity.windowManager.currentWindowMetrics
        val insets = metrics.windowInsets.getInsetsIgnoringVisibility(
                WindowInsets.Type.navigationBars() or WindowInsets.Type.displayCutout())

        return Point(metrics.bounds.width() - insets.right - insets.left,
                metrics.bounds.height() - insets.top - insets.bottom)
    }
}