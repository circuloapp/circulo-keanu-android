package info.guardianproject.keanuapp.ui.widgets;

import android.content.Context;
import android.database.Cursor;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;

/**
 * Created by N-Pex on 2019-05-31.
 * <p>
 * A class that plays audio from a DB cursor. It uses the very helpful ConcatenatingMediaSource
 * provided by ExoPlayer to automatically add new items to a playlist.
 */
public class StoryAudioPlayer {
    private final Context context;
    private SimpleExoPlayer player;
    private ConcatenatingMediaSource concatenatingMediaSource;
    private Cursor cursor;

    public StoryAudioPlayer(Context context) {
        this.context = context;
    }

    public SimpleExoPlayer getPlayer() {
        return getOrCreatePlayer();
    }

    public void reset() {
        if (this.player != null) {
            this.player.setPlayWhenReady(false);
        }
        if (this.concatenatingMediaSource != null) {
            concatenatingMediaSource.removeMediaSourceRange(0, concatenatingMediaSource.getSize());
        }
        if (cursor != null) {
            cursor.close();
        }
        cursor = null;
    }

    private SimpleExoPlayer getOrCreatePlayer() {
        if (player == null) {
            concatenatingMediaSource = new ConcatenatingMediaSource();

            player = new SimpleExoPlayer.Builder(context).build();
            player.setMediaSource(concatenatingMediaSource);
            player.prepare();
        }

        return player;
    }
}
