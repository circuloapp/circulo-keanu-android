package info.guardianproject.keanuapp.ui.widgets

import android.Manifest
import android.animation.Animator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.barteksc.pdfviewer.PDFView
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.ui.StyledPlayerView
import info.guardianproject.keanu.core.util.ActivityCompatUtils
import info.guardianproject.keanu.core.util.SecureMediaStore.exportContent
import info.guardianproject.keanu.core.util.SecureMediaStore.exportPath
import info.guardianproject.keanu.core.util.SecureMediaStore.isVfsUri
import info.guardianproject.keanu.core.util.extensions.getParcelableArrayListExtraCompat
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.ui.BaseActivity
import info.guardianproject.keanuapp.ui.widgets.PZSImageView.PSZImageViewImageMatrixListener
import info.guardianproject.keanuapp.viewmodel.gallery.MediaProcessViewModel
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import kotlin.math.abs
import kotlin.math.sign

class ImageViewActivity : BaseActivity(), PSZImageViewImageMatrixListener {
    private lateinit var viewPagerPhotos: ConditionallyEnabledViewPager
    private val tempRect = RectF()
    private var uris: ArrayList<Uri>? = null
    private var mimeTypes: ArrayList<String>? = null
    private var messagePacketIds: ArrayList<String>? = null
    private var mShowResend = false
    private lateinit var mMediaProcessViewModel: MediaProcessViewModel
    private val REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE = 104

    @SuppressLint("ClickableViewAccessibility")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMediaProcessViewModel = ViewModelProvider(this)[MediaProcessViewModel::class.java]
        window.requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY)
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY)
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mShowResend = intent.getBooleanExtra(EXTRA_SHOW_RESEND, false)
        viewPagerPhotos = ConditionallyEnabledViewPager(this)
        viewPagerPhotos.setBackgroundColor(0x33333333)
        setContentView(viewPagerPhotos)
        supportActionBar?.show()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = ""
        viewPagerPhotos.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                updateTitle()
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        viewPagerPhotos.addOnLayoutChangeListener { v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int ->
            if (right - left > 0 && bottom - top > 0 && viewPagerPhotos.adapter == null) {
                uris = intent.getParcelableArrayListExtraCompat(EXTRA_URIS, Uri::class.java)
                val photoUris = uris ?: return@addOnLayoutChangeListener

                mimeTypes = intent.getStringArrayListExtra(EXTRA_MIME_TYPES)
                val photoMimeTypes = mimeTypes ?: return@addOnLayoutChangeListener

                messagePacketIds = intent.getStringArrayListExtra(EXTRA_MESSAGE_IDS)
                if (photoUris.isNotEmpty() && photoUris.size == photoMimeTypes.size) {
                    val info = ArrayList<MediaInfo>(photoUris.size)
                    for (i in photoUris.indices) {
                        info.add(MediaInfo(photoUris[i], photoMimeTypes[i]))
                    }
                    viewPagerPhotos.adapter = MediaPagerAdapter(this@ImageViewActivity, info)
                    val currentIndex = intent.getIntExtra(EXTRA_CURRENT_INDEX, 0)
                    viewPagerPhotos.currentItem = currentIndex
                    updateTitle()
                }
            }
        }
        observeLiveData()
    }

    private fun updateTitle() {
        val itemCount = viewPagerPhotos.adapter?.count ?: 0
        if (itemCount > 0) {
            val title = getString(
                R.string.item_x_of_y, viewPagerPhotos.currentItem + 1, itemCount
            )
            setTitle(title)
        } else {
            title = ""
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_message_context, menu)
        menu.findItem(R.id.menu_message_copy).isVisible = false
        menu.findItem(R.id.menu_message_delete).isVisible = false
        menu.findItem(R.id.menu_message_resend).isVisible = mShowResend
        val showForward = intent.getBooleanExtra(EXTRA_SHOW_FORWARD, true)
        menu.findItem(R.id.menu_message_forward).isVisible = showForward
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.menu_message_forward -> {
                forwardMediaFile()
                return true
            }
            R.id.menu_message_share -> {
                exportMediaFile()
                return true
            }
            R.id.menu_downLoad -> {
                if (ActivityCompatUtils.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    && ActivityCompatUtils.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    downloadMediaFile()
                } else {
                    ActivityCompatUtils.requestReadWriteExternalStorage(this@ImageViewActivity,
                        REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE)
                }
                return true
            }
            R.id.menu_message_resend -> {
                resendMediaFile()
                return true
            }
            R.id.menu_message_delete -> {
                deleteMediaFile()
                return true
            }
            R.id.menu_message_nearby -> {
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun checkPermissions(): Boolean {
        val hasWritePermission = ActivityCompatUtils.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (!hasWritePermission) {
            ActivityCompatUtils.requestWriteExternalStorage(this, 1)
            return false
        }
        return true
    }

    private fun exportMediaFile() {
        val photoUris = uris ?: return
        val photoMimeTypes = mimeTypes ?: return
        if (checkPermissions()) {
            val currentItem = viewPagerPhotos.currentItem
            if (currentItem >= 0 && currentItem < photoUris.size) {
                val exportPath =
                    exportPath(this, photoMimeTypes[currentItem], photoUris[currentItem], true)
                exportMediaFile(photoMimeTypes[currentItem], photoUris[currentItem], exportPath)
            }
        }
    }

    private fun downloadMediaFile() {
        if (checkPermissions()) {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
            mOpenDocumentTreeResult.launch(intent)
        }
    }

    private var mOpenDocumentTreeResult = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == RESULT_OK) {
            val baseDocumentTreeUri = result.data?.data
            val takeFlags =
                Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION

            // take persistable Uri Permission for future use
            if (baseDocumentTreeUri != null) {
                contentResolver.takePersistableUriPermission(baseDocumentTreeUri, takeFlags)
            }

            val photoUris = uris ?: return@registerForActivityResult
            val photoMimeTypes = mimeTypes ?: return@registerForActivityResult

            val currentItem = viewPagerPhotos.currentItem
            if (currentItem >= 0 && currentItem < photoUris.size) {
                mMediaProcessViewModel.savePhotoToGallery(
                    photoMimeTypes[currentItem],
                    photoUris[currentItem],
                    true,
                    customUri = baseDocumentTreeUri
                )
            }

        }
    }

    private fun exportMediaFile(mimeType: String, mediaUri: Uri, exportPath: File) {
        try {
            exportContent(mimeType, mediaUri, exportPath)
            val uri = FileProvider.getUriForFile(this, "$packageName.provider", exportPath)
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
            shareIntent.type = mimeType
            startActivity(
                Intent.createChooser(
                    shareIntent,
                    resources.getText(R.string.export_media)
                )
            )
        } catch (e: IOException) {
            Toast.makeText(this, "Export Failed " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    private fun forwardMediaFile() {
        val photoUris = uris ?: return
        val photoMimeTypes = mimeTypes ?: return
        val currentItem = viewPagerPhotos.currentItem
        if (currentItem >= 0 && currentItem < photoUris.size) {
            val exportPath = exportPath(this, photoMimeTypes[currentItem], photoUris[currentItem], false)
            try {
                exportContent(photoMimeTypes[currentItem], photoUris[currentItem], exportPath)
                startActivity(
                    mApp.router.router(
                        this,
                        Uri.fromFile(exportPath), photoMimeTypes[currentItem]
                    )
                )
            } catch (e: IOException) {
                Timber.e(e)
            }
        }
    }

    private fun resendMediaFile() {
        val photoUris = uris ?: return
        val photoMimeTypes = mimeTypes ?: return
        val currentItem = viewPagerPhotos.currentItem
        if (currentItem >= 0 && currentItem < photoUris.size) {
            val resharePath = photoUris[currentItem].toString()
            val mimeType = photoMimeTypes[currentItem]
            val intentResult = Intent()
            intentResult.putExtra("resendImageUri", resharePath)
            intentResult.putExtra("resendImageMimeType", mimeType)
            setResult(RESULT_OK, intentResult)
            finish()
        }
    }

    private fun deleteMediaFile() {
        if (messagePacketIds != null) {
            val photoUris = uris ?: return
            val currentItem = viewPagerPhotos.currentItem
            if (currentItem >= 0 && currentItem < photoUris.size) {
                val deleteUri = photoUris[currentItem]
                if (deleteUri.scheme != null && deleteUri.scheme == "vfs") {
                    deleteUri.path?.let {
                        val fileMedia = File(it)
                        fileMedia.delete()
                    }
                }
                setResult(RESULT_OK)
                finish()
            }
        }
    }

    inner class MediaPagerAdapter(var context: Context, private var listMedia: List<MediaInfo>) :
        PagerAdapter() {
        private val imageRequestOptions: RequestOptions = RequestOptions().centerInside().diskCacheStrategy(DiskCacheStrategy.NONE)
            .error(R.drawable.broken_image_large)

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val mediaInfo = listMedia[position]
            val mediaView: View?
            if (mediaInfo.isPDF) {
                val pdfView = PDFView(context, null)
                mediaView = pdfView
                pdfView.id = position
                container.addView(mediaView)
                var `is`: InputStream? = null
                if (isVfsUri(mediaInfo.uri)) {
                    try {
                        `is` = FileInputStream(mediaInfo.uri.path)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                } else {
                    try {
                        `is` = contentResolver.openInputStream(mediaInfo.uri)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }
                }
                if (`is` != null) {
                    pdfView.fromStream(`is`)
                        .enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .defaultPage(0)
                        .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                        .password(null)
                        .scrollHandle(null)
                        .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                        // spacing between pages in dp. To define spacing color, set view background
                        .spacing(0)
                        .load()
                }
            } else if (mediaInfo.isAudio || mediaInfo.isVideo) {
                val playerView = StyledPlayerView(context)
                mediaView = playerView
                mediaView.setBackgroundColor(-0xcccccd)
                mediaView.setId(position)
                container.addView(mediaView)

                // 2. Create the player
                val exoPlayer = SimpleExoPlayer.Builder(context).build()

                ////Set media controller
                playerView.useController = true //set to true or false to see controllers
                playerView.requestFocus()
                // Bind the player to the view.
                playerView.player = exoPlayer
                exoPlayer.setMediaItem(MediaItem.fromUri(mediaInfo.uri))
                exoPlayer.prepare()
                exoPlayer.playWhenReady = false //run file/link when ready to play.
            } else {
                val imageView = PZSImageView(context)
                mediaView = imageView
                imageView.setBackgroundColor(-0xcccccd)
                imageView.id = position
                container.addView(imageView)
                try {
                    imageView.setMatrixListener(this@ImageViewActivity)
                    if (isVfsUri(mediaInfo.uri)) {
                        val filePath = mediaInfo.uri.path ?: ""
                        val fileMedia = File(filePath)
                        if (fileMedia.exists()) {
                            Glide.with(context)
                                .asBitmap()
                                .apply(imageRequestOptions)
                                .load(FileInputStream(fileMedia))
                                .into(imageView)
                        } else {
                            Glide.with(context)
                                .asBitmap()
                                .apply(imageRequestOptions)
                                .load(R.drawable.broken_image_large)
                                .into(imageView)
                        }
                    } else {
                        Glide.with(context)
                            .asBitmap()
                            .apply(imageRequestOptions)
                            .load(mediaInfo.uri)
                            .into(imageView)
                    }
                } catch (t: Throwable) { // may run Out Of Memory
                    Timber.w("unable to load thumbnail: $t")
                }
            }
            return mediaView
        }

        override fun getCount(): Int {
            return listMedia.size
        }

        override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
            return arg0 === arg1
        }

        override fun destroyItem(collection: ViewGroup, position: Int, arg2: Any) {
            collection.removeView(arg2 as View)
        }
    }

    override fun onImageMatrixSet(
        view: PZSImageView,
        imageWidth: Int,
        imageHeight: Int,
        imageMatrix: Matrix
    ) {
        if (view.id != viewPagerPhotos.currentItem) {
            return
        }
        tempRect[0f, 0f, imageWidth.toFloat()] = imageHeight.toFloat()
        imageMatrix.mapRect(tempRect)
        val width = view.width - view.paddingLeft - view.paddingRight
        val height = view.height - view.paddingTop - view.paddingBottom
        if (tempRect.width() > width || tempRect.height() > height) {
            viewPagerPhotos.enableSwiping = false
            return
        }
        viewPagerPhotos.enableSwiping = true
    }

    internal inner class ConditionallyEnabledViewPager(context: Context) : ViewPager(
        context
    ) {
        var enableSwiping = true
        private val gestureDetector: GestureDetector
        private val gestureDetectorListener: SwipeToCloseListener
        private val velocityTracker: VelocityTracker
        private var inSwipeToCloseGesture = false
        private var isClosing = false
        private var startingY = 0f

        init {
            gestureDetectorListener = SwipeToCloseListener(context)
            gestureDetector = GestureDetector(context, gestureDetectorListener)
            velocityTracker = VelocityTracker.obtain()
        }

        override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
            if (inSwipeToCloseGesture) {
                if (!isClosing) {
                    velocityTracker.addMovement(ev)
                    if (ev.action == MotionEvent.ACTION_CANCEL || ev.action == MotionEvent.ACTION_UP) {
                        velocityTracker.computeCurrentVelocity(1000) // Pixels per second
                        val velocityY = velocityTracker.yVelocity
                        val dy = ev.y - startingY
                        val vc = ViewConfiguration.get(context)
                        if (abs(dy) > vc.scaledTouchSlop && abs(velocityY) > vc.scaledMinimumFlingVelocity) {
                            closeByFling(dy, abs(viewPagerPhotos.height / velocityY))
                        } else {
                            // Reset all children. Lazy approach, instead of keeping count of "current photo" which
                            // might have changed during the motion event.
                            for (i in 0 until viewPagerPhotos.childCount) {
                                val child = viewPagerPhotos.getChildAt(i)
                                if (child.translationY != 0f) {
                                    child.animate().translationY(0f).alpha(1.0f).rotation(0f)
                                        .start()
                                }
                            }
                        }
                        inSwipeToCloseGesture = false
                    } else {
                        gestureDetector.onTouchEvent(ev)
                    }
                }
                return true
            } else if (enableSwiping && gestureDetector.onTouchEvent(ev)) {
                inSwipeToCloseGesture = true
                velocityTracker.clear()
                velocityTracker.addMovement(ev)
                return true
            }
            if (ev.actionMasked == MotionEvent.ACTION_DOWN) {
                startingY = ev.y
                gestureDetectorListener.setDisabled(false)
            } else if (ev.actionMasked == MotionEvent.ACTION_POINTER_DOWN) {
                gestureDetectorListener.setDisabled(true) // More than one finger, disable swipe to close
            }
            return super.dispatchTouchEvent(ev)
        }

        override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
            return if (!enableSwiping) {
                false
            } else super.onInterceptTouchEvent(ev)
        }

        override fun onTouchEvent(ev: MotionEvent): Boolean {
            return if (!enableSwiping) {
                true
            } else super.onTouchEvent(ev)
        }

        private fun closeByFling(dy: Float, seconds: Float) {
            @Suppress("NAME_SHADOWING")
            var seconds = seconds
            seconds = seconds.coerceAtMost(0.7f) // Upper limit on animation time!
            isClosing = true // No further touches
            val currentPhoto = viewPagerPhotos.findViewById<View>(
                viewPagerPhotos.currentItem
            )
            if (currentPhoto != null) {
                currentPhoto.pivotX = 0.8f * currentPhoto.width
                currentPhoto.translationY = dy
                currentPhoto.alpha = 0f.coerceAtLeast(1 - abs(dy) / (viewPagerPhotos.height / 2))
                currentPhoto.rotation = 30 * (dy / (viewPagerPhotos.height / 2))
                currentPhoto.animate().rotation(sign(dy) * 30)
                    .translationY(sign(dy) * currentPhoto.height).alpha(0f)
                    .setDuration((1000 * seconds).toLong())
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}
                        override fun onAnimationEnd(animation: Animator) {
                            finish()
                        }

                        override fun onAnimationCancel(animation: Animator) {}
                        override fun onAnimationRepeat(animation: Animator) {}
                    }).start()
            } else {
                // Hm, no animation, just close
                finish()
            }
        }

        override fun performClick(): Boolean {
            return !enableSwiping || super.performClick()
        }

        private inner class SwipeToCloseListener(context: Context) : SimpleOnGestureListener() {
            private val minDistance: Float
            private var disabled = false
            private var inGesture = false

            init {
                minDistance = ViewConfiguration.get(context).scaledTouchSlop.toFloat()
            }

            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                val dy = e2.y - (e1?.y ?: 0f)
                val dx = e2.x - (e1?.x ?: 0f)
                if (abs(dy) > minDistance && !disabled) {
                    val currentPhoto = viewPagerPhotos.findViewById<View>(
                        viewPagerPhotos.currentItem
                    )
                    if (currentPhoto != null) {
                        currentPhoto.pivotX = 0.8f * currentPhoto.width
                        currentPhoto.translationY = dy
                        currentPhoto.alpha =
                            0f.coerceAtLeast(1 - abs(dy) / (viewPagerPhotos.height / 2))
                        currentPhoto.rotation = 30 * (dy / (viewPagerPhotos.height / 2))
                    }
                    inGesture = true
                    return true
                } else if (abs(dx) > minDistance && !inGesture) {
                    disabled =
                        true // Looks like we have a horizontal movement, disable "swipe-to-close"
                }
                return false
            }

            fun setDisabled(disabled: Boolean) {
                this.disabled = disabled
                inGesture = false
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_READ_WRITE_EXTERNAL_STORAGE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val currentItem = viewPagerPhotos.currentItem
                val photoUris = uris ?: return
                if (currentItem >= 0 && currentItem < photoUris.size) {
                    downloadMediaFile()
                }
            } else {
                // Permission Denied
                Toast.makeText(this@ImageViewActivity, getString(R.string.permission_denied), Toast.LENGTH_SHORT)
                    .show()
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun observeLiveData() {
        mMediaProcessViewModel.observableSaveMediaLiveData.observe(this) {
            if (!TextUtils.isEmpty(it)) {
                showToast(getString(R.string.msg_download_image, it))
            } else {
                showToast(getString(R.string.msg_download_image_errorr))
            }
        }
    }
    companion object {
        const val EXTRA_URIS = "uris"
        const val EXTRA_MIME_TYPES = "mime_types"
        const val EXTRA_MESSAGE_IDS = "message_ids"
        const val EXTRA_CURRENT_INDEX = "current_index"
        const val EXTRA_SHOW_RESEND = "showResend"
        const val EXTRA_SHOW_FORWARD = "showForward"
    }
}