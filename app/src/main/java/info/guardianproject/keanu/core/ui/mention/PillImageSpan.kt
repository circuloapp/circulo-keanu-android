@file:Suppress("DEPRECATION")

package info.guardianproject.keanu.core.ui.mention

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.style.ReplacementSpan
import android.widget.TextView
import androidx.annotation.UiThread
import androidx.appcompat.content.res.AppCompatResources
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.chip.ChipDrawable
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.tasks.GlideApp
import org.matrix.android.sdk.api.session.room.send.MatrixItemSpan
import org.matrix.android.sdk.api.util.MatrixItem
import java.lang.ref.WeakReference

/**
 * This span is able to replace a text by a [ChipDrawable]
 * It's needed to call [bind] method to start requesting avatar, otherwise only the placeholder icon will be displayed if not already cached.
 * Implements MatrixItemSpan so that it could be automatically transformed in matrix links and displayed as pills.
 */
class PillImageSpan(
    private val context: Context,
    override val matrixItem: MatrixItem
) : ReplacementSpan(), MatrixItemSpan {

    private val pillDrawable = createChipDrawable()
    private var tv: WeakReference<TextView>? = null

    @UiThread
    fun bind(textView: TextView) {
        tv = WeakReference(textView)

    }

    // ReplacementSpan *****************************************************************************


    override fun getSize(
        paint: Paint, text: CharSequence,
        start: Int,
        end: Int,
        fm: Paint.FontMetricsInt?
    ): Int {
        val rect = pillDrawable.bounds
        if (fm != null) {
            val fmPaint = paint.fontMetricsInt
            val fontHeight = fmPaint.bottom - fmPaint.top
            val drHeight = rect.bottom - rect.top
            val top = drHeight / 2 - fontHeight / 4
            val bottom = drHeight / 2 + fontHeight / 4
            fm.ascent = -bottom
            fm.top = -bottom
            fm.bottom = top
            fm.descent = top
        }
        return rect.right
    }


    override fun draw(
        canvas: Canvas, text: CharSequence,
        start: Int,
        end: Int,
        x: Float,
        top: Int,
        y: Int,
        bottom: Int,
        paint: Paint
    ) {
        canvas.save()
        val fm = paint.fontMetricsInt
        val transY: Int = y + (fm.descent + fm.ascent - pillDrawable.bounds.bottom) / 2
        canvas.save()
        canvas.translate(x, transY.toFloat())
        pillDrawable.draw(canvas)
        canvas.restore()
    }

    // Private methods *****************************************************************************

//    private fun createChipDrawable(): ChipDrawable {
//        val textPadding = context.resources.getDimension(R.dimen.pill_text_padding)
//
//        return ChipDrawable.createFromResource(context, R.xml.pill_view).apply {
//            text = /*matrixItem.*/displayName
//            textEndPadding = textPadding
//            textStartPadding = textPadding
//            setChipMinHeightResource(R.dimen.pill_min_height)
//            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
//        }
//    }

    private fun createChipDrawable(): ChipDrawable {
        val textPadding = context.resources.getDimension(R.dimen.pill_text_padding)
        //      val userImageUrl = GlideUtils.resolve(Uri.parse(matrixItem.avatarUrl), true)
        //  setChipImage(userImageUrl)

        return ChipDrawable.createFromResource(context, R.xml.pill_view).apply {
            text = matrixItem.displayName
            textEndPadding = textPadding
            chipIcon = AppCompatResources.getDrawable(context, R.drawable.avatar_unknown)
            textStartPadding = textPadding
            setChipMinHeightResource(R.dimen.pill_min_height)
            setChipIconSizeResource(R.dimen.pill_avatar_size)
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
        }
    }

    private fun setChipImage(userImageUrl: Uri?) {
        GlideApp.with(context)
            .load(userImageUrl)
            .into(object : CustomTarget<Drawable>(30, 30) {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    pillDrawable.chipIcon = resource
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    pillDrawable.chipIcon =
                        AppCompatResources.getDrawable(context, R.drawable.avatar_unknown)
                }

            })


    }
}
