package info.guardianproject.keanu.core.ui.friends

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanuapp.R

/**
 * Created by n8fr8 on 3/29/16.
 */
class FriendViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    @JvmField
    val line1: TextView? = view.findViewById(R.id.line1)

    @JvmField
    val line2: TextView? = view.findViewById(R.id.line2)

    @JvmField
    val avatar: ImageView? = view.findViewById(R.id.avatar)

    val avatarCheck: ImageView? = view.findViewById(R.id.avatarCheck)

    val subBox: View? = view.findViewById(R.id.subscriptionBox)

    val buttonSubApprove: Button? = view.findViewById(R.id.btnApproveSubscription)

    val buttonSubDecline: Button? = view.findViewById(R.id.btnDeclineSubscription)

    val container: View? = view.findViewById(R.id.message_container)

    @JvmField
    var address: String? = null

    @JvmField
    var nickname: String? = null

    @JvmField
    var type = 0

    val providerId: Long = 0

    val accountId: Long = 0

    @JvmField
    var contactId: Long = 0

    val mediaThumb: ImageView? = null
}
