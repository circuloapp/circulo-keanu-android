package info.guardianproject.keanu.core.ui.me

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import info.guardianproject.keanu.core.ImApp
import info.guardianproject.keanu.core.model.settings.SettingScreen
import info.guardianproject.keanu.core.ui.me.settings.SettingsActivity
import info.guardianproject.keanu.core.ui.onboarding.OnboardingManager
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanu.core.util.ImageChooserHelper
import info.guardianproject.keanu.core.util.SnackbarExceptionHandler
import info.guardianproject.keanu.core.util.constants.BundleKeys
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.DialogProfileQrCodeBinding
import info.guardianproject.keanuapp.databinding.FragmentMyProfileBinding
import info.guardianproject.keanuapp.ui.qr.zxing.encode.Contents
import info.guardianproject.keanuapp.ui.qr.zxing.encode.QRCodeEncoder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.session.content.ContentUrlResolver
import org.matrix.android.sdk.api.session.user.model.User
import timber.log.Timber
import java.io.File

class MyProfileFragment : Fragment(), View.OnClickListener, TextView.OnEditorActionListener {

    private val mApp
        get() = activity?.application as? ImApp

    private val mSession: Session?
        get() = mApp?.matrixSession

    private val mUser: User?
        get() = mSession?.myUserId?.let { mSession?.userService()?.getUser(it) }

    private val MY_PERMISSIONS_REQUEST_CAMERA = 1

    private lateinit var mBinding: FragmentMyProfileBinding

    private val mCoroutineScope: CoroutineScope by lazy {
        CoroutineScope(Dispatchers.IO + SnackbarExceptionHandler(mBinding.root))
    }

    private var mImageChooserHelper: ImageChooserHelper? = null
    private var userImageUrl: String = ""

    private lateinit var mPickAvatar: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val activity = activity ?: return

        val helper = ImageChooserHelper(activity)
        mImageChooserHelper = helper

        mPickAvatar = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            helper.getCallback(this::setAvatar)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentMyProfileBinding.inflate(inflater, container, false)

        mBinding.ivAvatar.setOnClickListener(this)


        mBinding.ivSmallCamera.setOnClickListener {
            showProfileQrDialog(mSession?.myUserId ?: "", mUser?.displayName ?: "")
        }

        mBinding.btnEditProfile.setOnClickListener {
            val editProfileIntent = Intent(activity, EditProfileActivity::class.java)
            editProfileIntent.putExtra(BundleKeys.BUNDLE_KEY_DISPLAY_NAME, mUser?.displayName ?: "")
            editProfileIntent.putExtra(BundleKeys.BUNDLE_KEY_IMAGE_NAME, userImageUrl)
            startActivity(editProfileIntent)
        }
        initSettingItems()

        return mBinding.root
    }

    private fun initSettingItems() {
        mBinding.layoutItemNotifications.setOnClickListener {
            openSetting(SettingScreen.NOTIFICATIONS)
        }

        mBinding.layoutItemPreferences.setOnClickListener {
            openSetting(SettingScreen.PREFERENCES)
        }

        mBinding.layoutItemAccount.setOnClickListener {
            openSetting(SettingScreen.ACCOUNT)
        }

        mBinding.layoutItemSecurityPrivacy.setOnClickListener {
            openSetting(SettingScreen.SECURITY_PRIVACY)
        }

        mBinding.layoutItemPhysicalSafety.setOnClickListener {
            openSetting(SettingScreen.PHYSICAL_SAFETY)
        }

        mBinding.layoutItemPhotoVideo.setOnClickListener {
            openSetting(SettingScreen.PHOTOS_VIDEOS)
        }

        mBinding.layoutItemAbout.setOnClickListener {
            openSetting(SettingScreen.ABOUT)
        }

        mBinding.layoutItemFeedback.setOnClickListener {
            openSetting(SettingScreen.FEEDBACK)
        }
    }

    private fun openSetting(settingScreen: SettingScreen) {
        val intent = Intent(activity, SettingsActivity::class.java)
        intent.putExtra(BundleKeys.BUNDLE_KEY_SETTING_SCREEN, settingScreen)
        startActivity(intent)
    }

    private fun showProfileQrDialog(userId: String, displayName: String) {
        context?.let {
            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            val binding = DialogProfileQrCodeBinding.inflate(layoutInflater)
            dialog.setContentView(binding.root)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            qrCodeGenerator("matrix://invite?id=$userId", 400, binding.ivProfileQr)

            binding.scanButton.setOnClickListener {
                if (hasCameraPermission()) {
                    mApp?.matrixSession?.myUserId?.let {
                        OnboardingManager.inviteScan(
                            requireActivity(),
                            OnboardingManager.generateMatrixLink(it)
                        )
                    }
                }
            }

            binding.shareButton.setOnClickListener {
                OnboardingManager.inviteShare(
                    context = requireActivity(),
                    message = OnboardingManager.generateInviteMessage(requireActivity(), displayName, userId)
                )
            }
            binding.ivClose.setOnClickListener {
                dialog.dismiss()
            }

            binding.tvUserId.text = userId
            if (!TextUtils.isEmpty(displayName)) {
                binding.tvDisplayName.text = displayName
            } else {
                binding.tvDisplayName.visibility = View.GONE
            }

            dialog.show()
        }
    }

    private fun hasCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(Manifest.permission.CAMERA),
                MY_PERMISSIONS_REQUEST_CAMERA
            )

            false
        } else {
            true
        }
    }

    private fun qrCodeGenerator(qrData: String, dimension: Int, imageView: ImageView) {

        val qrCodeEncoder = QRCodeEncoder(
            qrData, null,
            Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), dimension
        )
        try {
            val bitmap = qrCodeEncoder.encodeAsBitmap()
            imageView.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            Timber.tag("MyProfile").e("Error Generating bitmap")
        }

    }

    override fun onClick(v: View?) {
        val activity = activity ?: return

        when (v) {
            mBinding.ivAvatar -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            activity,
                            Manifest.permission.CAMERA
                        )
                    ) {
                        Snackbar.make(v, R.string.grant_perms, Snackbar.LENGTH_LONG).show()
                    } else {
                        mRequestCamera.launch(Manifest.permission.CAMERA)
                    }
                } else {
                    mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
                }
            }
        }
    }

    /**
     * Handle name change.
     */
    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        // According to an answer on StackOverflow
        // https://stackoverflow.com/questions/1489852/android-handle-enter-in-an-edittext
        // This is the definitive way to handle all enter key events.
        if (event == null) {
            if (actionId != EditorInfo.IME_ACTION_DONE && actionId != EditorInfo.IME_ACTION_NEXT) return false
        } else if (event.action != KeyEvent.ACTION_DOWN) {
            return false
        } else if (actionId != EditorInfo.IME_NULL) {
            return false
        }

        v?.clearFocus()

        (activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(v?.windowToken, 0)

        val userId = mSession?.myUserId ?: return true
        val name = v?.text?.toString() ?: return true

        mCoroutineScope.launch {
            mSession?.profileService()?.setDisplayName(userId, name)
        }

        return true
    }

    private var mRequestCamera =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            mImageChooserHelper?.intent?.let { mPickAvatar.launch(it) }
        }

    private fun setAvatar(bmp: Bitmap) {
        mBinding.ivAvatar.setImageDrawable(BitmapDrawable(resources, bmp))

        val userId = mSession?.myUserId ?: return

        mCoroutineScope.launch {
            @Suppress("BlockingMethodInNonBlockingContext")
            val file = File.createTempFile("avatar", "jpg")

            file.outputStream().use { os ->
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, os)
            }

            try {
                mSession?.profileService()?.updateAvatar(userId, Uri.fromFile(file), file.name)
            } finally {
                file.delete()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        displayUserContent()

    }

    private fun displayUserContent() {
        mSession?.contentUrlResolver()
            ?.resolveThumbnail(mUser?.avatarUrl, 512, 512, ContentUrlResolver.ThumbnailMethod.SCALE)
            ?.let {
                userImageUrl = it
                GlideUtils.loadImageFromUri(
                    activity,
                    Uri.parse(userImageUrl),
                    mBinding.ivAvatar,
                    false
                )
            }

        mUser?.displayName?.let { displayName ->
            mBinding.tvNickname.visibility = View.VISIBLE
            mBinding.tvNickname.text = displayName
        }


    }
}