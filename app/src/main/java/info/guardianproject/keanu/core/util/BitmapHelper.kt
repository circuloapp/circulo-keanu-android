package info.guardianproject.keanu.core.util

import android.content.ContentValues
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
import android.provider.MediaStore.MediaColumns.DISPLAY_NAME
import android.provider.MediaStore.MediaColumns.IS_PENDING
import android.provider.MediaStore.MediaColumns.MIME_TYPE
import android.provider.MediaStore.MediaColumns.RELATIVE_PATH
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.QRCodeWriter
import info.guardianproject.keanu.core.ImApp
import java.io.File
import java.io.FileOutputStream

object BitmapHelper {

    fun qrCode(contents: String, width: Int, height: Int): Bitmap {
        val qrWriter = QRCodeWriter()

        val hints = HashMap<EncodeHintType, Any>()
        hints[EncodeHintType.MARGIN] = 0

        val matrix = qrWriter.encode(contents, BarcodeFormat.QR_CODE, width, height, hints)

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)

        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (matrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }

        return bitmap
    }

    /**
     * Saves a bitmap as a Jpeg file.
     *
     * Note that `.jpg` extension is added to the filename.
     */
    fun Bitmap.saveAsJPG(filename: String) = "$filename".let { name ->
        val appContext = ImApp.sImApp?.applicationContext
        val compressionQuality = 100
        appContext?.let {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q)
                FileOutputStream(File(FileUtils.getExternalCachePictureDirectory(appContext), name))
                    .use { compress(Bitmap.CompressFormat.JPEG, compressionQuality, it) }
            else {
                val values = ContentValues().apply {
                    put(DISPLAY_NAME, name)
                    put(MIME_TYPE, "image/jpeg")
                    put(RELATIVE_PATH, FileUtils.DIR_PICTURES)
                    put(IS_PENDING, 0)
                }

                val resolver = appContext.contentResolver
                val uri = resolver.insert(EXTERNAL_CONTENT_URI, values)
                uri?.let { resolver.openOutputStream(it) }
                    ?.use { compress(Bitmap.CompressFormat.JPEG, compressionQuality, it) }

                values.clear()
                values.put(IS_PENDING, 0)
                uri?.also {
                    resolver.update(it, values, null, null) }
            }
        }

    }
}
