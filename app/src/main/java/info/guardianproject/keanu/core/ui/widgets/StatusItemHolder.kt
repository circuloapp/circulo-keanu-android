package info.guardianproject.keanu.core.ui.widgets

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.GlideUtils
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.StatusItemBinding
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.message.MessageType
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent

class StatusItemHolder(private var mBinding: StatusItemBinding): RecyclerView.ViewHolder(mBinding.root) {

    private val mContext
        get() = mBinding.root.context

    fun bind(te: TimelineEvent) {
        mBinding.root.visibility = View.VISIBLE
        mBinding.ivAvatar.visibility = View.GONE

        when (te.root.type) {
            EventType.STATE_ROOM_NAME -> {
                mBinding.tvMessage.text = mContext.getString(R.string._changed_the_room_name_to_,
                        te.senderInfo.disambiguatedDisplayName,
                        getContent(te, "name"))
            }

            EventType.STATE_ROOM_TOPIC -> {
                mBinding.tvMessage.text = mContext.getString(R.string._changed_the_room_topic_to_,
                        te.senderInfo.disambiguatedDisplayName,
                        getContent(te, "topic"))
            }

            EventType.STATE_ROOM_MEMBER -> {
                val m = getContent(te, "membership")

                when (Membership.values().firstOrNull { it.name == m }) {
                    Membership.JOIN -> {
                        mBinding.tvMessage.text = mContext.getString(R.string.contact_joined,
                                te.senderInfo.disambiguatedDisplayName)

                        setAvatar(te)
                    }

                    Membership.LEAVE,
                    Membership.BAN -> {
                        mBinding.tvMessage.text = mContext.getString(R.string.contact_left,
                                te.senderInfo.disambiguatedDisplayName)

                        setAvatar(te)
                    }

                    Membership.INVITE -> {
                        mBinding.tvMessage.text = mContext.getString(R.string._was_invited,
                                te.senderInfo.disambiguatedDisplayName)

                        setAvatar(te)
                    }

                    Membership.KNOCK -> {
                        mBinding.tvMessage.text = mContext.getString(R.string._is_knocking,
                                te.senderInfo.disambiguatedDisplayName)

                        setAvatar(te)
                    }

                    else -> {
                        hide()
                    }
                }
            }

            EventType.MESSAGE -> {
                if (te.getLastMessageContent()?.msgType == MessageType.MSGTYPE_VERIFICATION_REQUEST) {

                }
                else {
                    hide()
                }
            }

            else -> {
                // Could be EventType.REACTION. That's not shown directly, but on the reacted upon event.
                hide()
            }
        }
    }

    private fun getContent(te: TimelineEvent, key: String): String? {
        return te.root.getClearContent()?.get(key)?.toString()?.trim()
    }

    private fun setAvatar(te: TimelineEvent) {
        mBinding.ivAvatar.visibility = View.VISIBLE
        mBinding.tvMessage.visibility = View.VISIBLE
        mBinding.root.visibility = View.VISIBLE

        GlideUtils.loadAvatar(mBinding.ivAvatar, te.senderInfo)
    }

    private fun hide() {
        mBinding.ivAvatar.visibility = View.GONE
        mBinding.tvMessage.visibility = View.GONE
        mBinding.root.visibility = View.GONE
    }
}