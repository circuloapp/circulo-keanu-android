package info.guardianproject.keanu.core.util.extensions

import android.content.Context
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.message.MessageAudioContent
import org.matrix.android.sdk.api.session.room.model.message.MessageImageContent
import org.matrix.android.sdk.api.session.room.model.message.MessageStickerContent
import org.matrix.android.sdk.api.session.room.model.message.MessageVideoContent
import org.matrix.android.sdk.api.session.room.model.message.MessageWithAttachmentContent
import org.matrix.android.sdk.api.session.room.model.relation.ReactionContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import org.matrix.android.sdk.api.session.room.timeline.getLastMessageContent
import org.matrix.android.sdk.api.session.room.timeline.getRelationContent
import timber.log.Timber

fun TimelineEvent.getSummary(context: Context): String {
    val name = senderInfo.disambiguatedDisplayName
    val mc = getLastMessageContent()

    Timber.d("tuancoltech getSummary body: " + mc?.body + ". name: " + name)
    when {
        mc is MessageImageContent -> return context.getString(R.string._sent_an_image, name)

        mc is MessageAudioContent -> return context.getString(R.string._sent_audio, name)

        mc is MessageVideoContent -> return context.getString(R.string._sent_a_video, name)

        mc is MessageStickerContent -> return context.getString(R.string._sent_a_sticker_, name, mc.body)

        mc is MessageWithAttachmentContent -> return context.getString(R.string._sent_a_file, name)

        mc?.body?.isNotEmpty() == true -> return "$name: ${mc.body}"

        mc == null -> {
            val reaction = root.getClearContent().toModel<ReactionContent>()

            if (reaction != null) {

                return context.getString(
                    R.string._reacted_with_, name,
                    reaction.relatesTo?.key ?: context.getString(R.string.unknown))
            }
        }
    }

    return context.getString(R.string._did_something, name)
}

fun TimelineEvent.isReaction(): Boolean {
    return root.getClearType() == EventType.REACTION
}

fun TimelineEvent.getRelatedEventId(): String? {
    return getRelationContent()?.eventId
}

fun TimelineEvent.getReplyToEventId(): String? {
    return getRelationContent()?.inReplyTo?.eventId
}

fun TimelineEvent.getLastMessageBody(): String? {
    return getLastMessageContent()?.body
}