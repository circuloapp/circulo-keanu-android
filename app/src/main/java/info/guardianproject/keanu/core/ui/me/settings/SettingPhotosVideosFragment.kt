package info.guardianproject.keanu.core.ui.me.settings

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingPhotosVideosBinding

class SettingPhotosVideosFragment: Fragment(), TextView.OnEditorActionListener {

    private lateinit var mBinding: FragmentSettingPhotosVideosBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mBinding = FragmentSettingPhotosVideosBinding.inflate(inflater, container, false)

        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_photos_videos))

        return mBinding.root
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return true
    }
}