package info.guardianproject.keanu.core.ui.onboarding

abstract class OnboardingProvider {

    abstract val BASE_INVITE_URL: String
    abstract val REQUEST_SCAN: Int
    abstract val DEFAULT_SCHEME: String

    fun generateInviteLink(username: String): String {
        return BASE_INVITE_URL + username
    }

    fun generateMatrixLink(username: String): String {
        return "${DEFAULT_SCHEME}://invite?id=$username"
    }
}