package info.guardianproject.keanu.core.ui.friends

import agency.tango.android.avatarview.AvatarPlaceholder
import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import info.guardianproject.keanu.core.KeanuConstants
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanu.core.util.GlideUtils.loadAvatar
import info.guardianproject.keanuapp.R

class FriendListItem(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {

    var viewHolder: FriendViewHolder? = null

    private var mShowPresence = false

    fun bind(holder: FriendViewHolder, address: String?, nickname: String?, avatarUrl: String?) {
        val typeUnmasked = KeanuConstants.Contacts.TYPE_NORMAL

        val nick = if (nickname.isNullOrEmpty()) {
            address?.split(":")?.firstOrNull()
        } else {
            nickname
        }

        holder.line1?.text = nick

        holder.avatar?.let {
            it.visibility = VISIBLE
            loadAvatar(context, avatarUrl, AvatarPlaceholder(nick), it)
        }

        var statusText = address

        if (typeUnmasked and KeanuConstants.Contacts.TYPE_FLAG_HIDDEN != 0) {
            statusText += " | " + context.getString(R.string.action_archive)
        }
        holder.line2?.text = statusText
        holder.line1?.visibility = VISIBLE
    }

    fun applyStyleColors(holder: FriendViewHolder) {
        //not set color

        val themeColorText = PreferenceProvider(context).getTextColor()

        if (themeColorText != -1) {
            holder.line1?.setTextColor(themeColorText)
            holder.line2?.setTextColor(themeColorText)
        }
    }

    fun setShowPresence(showPresence: Boolean) {
        mShowPresence = showPresence
    }
}