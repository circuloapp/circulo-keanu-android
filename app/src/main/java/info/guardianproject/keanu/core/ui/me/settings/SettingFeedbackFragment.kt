package info.guardianproject.keanu.core.ui.me.settings

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import info.guardianproject.keanu.core.ui.me.providers.PreferenceProvider
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.FragmentSettingFeedbackBinding

class SettingFeedbackFragment : Fragment(), TextView.OnEditorActionListener {

    private lateinit var mBinding: FragmentSettingFeedbackBinding
    private lateinit var preferenceProvider: PreferenceProvider

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentSettingFeedbackBinding.inflate(inflater, container, false)

        (activity as SettingsActivity).setBackButtonText(getString(R.string.profile_feedback))
        preferenceProvider = PreferenceProvider(requireActivity())
        mBinding.sbSendLogs.isChecked = preferenceProvider.getDebugLogInfo()
        mBinding.sbSendLogs.setOnCheckedChangeListener { _, isChecked ->
            preferenceProvider.setDebugLogInfo(isChecked)
        }
        return mBinding.root
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return true
    }
}