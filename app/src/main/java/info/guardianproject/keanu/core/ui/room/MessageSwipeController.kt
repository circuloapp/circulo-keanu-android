package info.guardianproject.keanu.core.ui.room

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import androidx.recyclerview.widget.RecyclerView
import info.guardianproject.keanu.core.util.DisplayHelper
import info.guardianproject.keanuapp.R
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import kotlin.math.abs

class MessageSwipeController(
    private val context: Context,
    private val swipeControllerActions: SwipeControllerActions
) :
    ItemTouchHelper.Callback() {

    private lateinit var imageDrawable: Drawable
    private lateinit var shareRound: Drawable

    private var currentItemViewHolder: RecyclerView.ViewHolder? = null
    private lateinit var mView: View
    private var dX = 0f

    private var swipeBack = false
    @Suppress("SameParameterValue")
    private var startTracking = false

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        mView = viewHolder.itemView
        imageDrawable = AppCompatResources.getDrawable(
            context,
            R.drawable.ic_reply_black
        )!!
        shareRound = AppCompatResources.getDrawable(
            context,
            R.drawable.ic_round_shape
        )!!
        return makeMovementFlags(ACTION_STATE_IDLE, RIGHT)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        // Functionality not needed
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int {
        if (swipeBack) {
            swipeBack = false
            return 0
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState == ACTION_STATE_SWIPE) {
            setTouchListener(recyclerView, viewHolder)
        }

        if (mView.translationX < convertToDp(100) || dX < this.dX) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            this.dX = dX
            startTracking = true
        }
        currentItemViewHolder = viewHolder
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchListener(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        recyclerView.setOnTouchListener { _, event ->
            swipeBack =
                event.action == MotionEvent.ACTION_CANCEL || event.action == MotionEvent.ACTION_UP
            if (swipeBack && abs(mView.translationX) >= this@MessageSwipeController.convertToDp(100)) {
                recyclerView.adapter?.notifyItemChanged(viewHolder.absoluteAdapterPosition)
                val timeLine =
                    (viewHolder.bindingAdapter as MessageListAdapter).getTimelineEvent(
                        viewHolder.absoluteAdapterPosition
                    )
                swipeControllerActions.showReplyUI(timeLine = timeLine!!)
            }
            false
        }
    }

    @Suppress("SameParameterValue")
    private fun convertToDp(pixel: Int): Int {
        return DisplayHelper.pix2dp(pixel, context)
    }

}

interface SwipeControllerActions {

    fun showReplyUI(timeLine: TimelineEvent)
}