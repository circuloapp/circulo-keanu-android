package info.guardianproject.keanu.core

import okhttp3.Cache
import okhttp3.Dns
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.dnsoverhttps.DnsOverHttps
import java.io.File
import java.net.InetAddress
import java.util.concurrent.TimeUnit

open class MatrixNetworkInterceptor : Interceptor {

    private var dohClient : Dns
    private var okClient : OkHttpClient

    init {
        //initialize DNS over HTTPS client using Google DNS
        val appCache = Cache(File("cacheDir", "okhttpcache"), 10 * 1024 * 1024)
        val bootstrapClient = OkHttpClient.Builder().cache(appCache).build()

        dohClient = DnsOverHttps.Builder().client(bootstrapClient)
            .url("https://1.1.1.1/dns-query".toHttpUrl())
            .bootstrapDnsHosts(InetAddress.getByName("1.1.1.1"))
            .build()

        okClient = OkHttpClient.Builder()
            .dns(dohClient)
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build()

    }
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = okClient.newCall(request = chain.request()).execute()
        return response
    }
}