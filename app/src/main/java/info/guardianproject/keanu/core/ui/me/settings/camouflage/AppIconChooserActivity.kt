package info.guardianproject.keanu.core.ui.me.settings.camouflage

import android.graphics.Color
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import info.guardianproject.keanu.core.ui.me.settings.SettingPhysicalSafetyFragment
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityAppIconChooserBinding
import info.guardianproject.keanuapp.ui.BaseActivity

class AppIconChooserActivity : BaseActivity() {

    private lateinit var mBinding: ActivityAppIconChooserBinding

    private var mAppIconList = mutableListOf<AppIconChooserModel>()
    private lateinit var mIconChooserAdapter: AppIconChooserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityAppIconChooserBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        window.apply {
            statusBarColor = Color.TRANSPARENT
        }

        supportActionBar?.hide()
        mBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }

        mBinding.toolbar.tvBackText.setOnClickListener {
            finish()
        }
        mBinding.toolbar.tvBackText.text = getString(R.string.settings_app_icon_chooser_title)
        showAppIconData()
    }

    private fun showAppIconData() {

        mAppIconList.apply {
            add(
                AppIconChooserModel(
                packageName = SettingPhysicalSafetyFragment.disguisePackage[0],
                appIconRes = R.mipmap.ic_launcher,
                appIconNameRes = R.string.app_name)
            )
            add(
                AppIconChooserModel(
                packageName = SettingPhysicalSafetyFragment.disguisePackage[1],
                appIconRes = R.drawable.ic_camouflage_settings,
                appIconNameRes = R.string.app_icon_chooser_label_settings)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[2],
                appIconRes = R.drawable.ic_camouflage_camera,
                appIconNameRes = R.string.app_icon_chooser_label_camera)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[3],
                appIconRes = R.drawable.ic_camouflage_firefox,
                appIconNameRes = R.string.app_icon_chooser_label_firefox)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[4],
                appIconRes = R.drawable.ic_camouflage_messenger,
                appIconNameRes = R.string.app_icon_chooser_label_messenger)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[5],
                appIconRes = R.drawable.ic_camouflage_navigator,
                appIconNameRes = R.string.app_icon_chooser_label_navigator)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[6],
                appIconRes = R.drawable.ic_camouflage_skype,
                appIconNameRes = R.string.app_icon_chooser_label_skype)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[7],
                appIconRes = R.drawable.ic_camouflage_telegram,
                appIconNameRes = R.string.app_icon_chooser_label_telegram)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[8],
                appIconRes = R.drawable.ic_camouflage_whatsapp,
                appIconNameRes = R.string.app_icon_chooser_label_whatsapp)
            )
            add(
                AppIconChooserModel(packageName = SettingPhysicalSafetyFragment.disguisePackage[9],
                appIconRes = R.drawable.ic_camouflage_youtube,
                appIconNameRes = R.string.app_icon_chooser_label_youtube)
            )
        }

        mBinding.recyclerViewAppIcon.layoutManager = GridLayoutManager(this, 4)
        mIconChooserAdapter = AppIconChooserAdapter(this, mAppIconList)
        mBinding.recyclerViewAppIcon.adapter = mIconChooserAdapter
    }
}