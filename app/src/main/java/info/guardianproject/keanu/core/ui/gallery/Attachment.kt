package info.guardianproject.keanu.core.ui.gallery

import android.net.Uri
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Attachment (
    val sender: String?,
    val file: Uri,
    val time: Long?,
    val duration: Long?
    ) : Parcelable