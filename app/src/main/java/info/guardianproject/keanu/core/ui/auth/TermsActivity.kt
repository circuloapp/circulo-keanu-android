package info.guardianproject.keanu.core.ui.auth

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import info.guardianproject.keanuapp.R
import info.guardianproject.keanuapp.databinding.ActivityTermsRecaptchaBinding
import info.guardianproject.keanuapp.ui.BaseActivity
import kotlinx.parcelize.Parcelize
import org.matrix.android.sdk.api.auth.data.LocalizedFlowDataLoginTerms

@Parcelize
data class TermsActivityArgument(
        val localizedFlowDataLoginTerms: List<LocalizedFlowDataLoginTerms>
) : Parcelable


class TermsActivity: BaseActivity() {

    companion object {
        const val EXTRA_TERMS = "extra_terms"
    }

    private lateinit var mTerms: List<LocalizedFlowDataLoginTerms>

    private var mIndex = 0

    private lateinit var mBinding: ActivityTermsRecaptchaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityTermsRecaptchaBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mTerms = intent.getParcelableExtra<TermsActivityArgument>(EXTRA_TERMS)?.localizedFlowDataLoginTerms
                ?: ArrayList()

        @SuppressLint("SetJavaScriptEnabled")
        mBinding.webView.settings.javaScriptEnabled = true

        mTerms.firstOrNull()?.localizedUrl?.let { load(it) }

        supportActionBar?.title = mTerms.firstOrNull()?.localizedName
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.panic_setup, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)

                finish()

                true
            }

            R.id.menu_done -> {
                mIndex++

                if (mIndex < mTerms.size) {
                    mTerms[mIndex].localizedUrl?.let { load(it) }
                    supportActionBar?.title = mTerms[mIndex].localizedName
                }
                else {
                    setResult(Activity.RESULT_OK)

                    finish()
                }

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun load(url: String) {
        mBinding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                if (request?.url?.toString() == url) {
                    return false
                }

                request?.url?.let {
                    val intent = Intent(Intent.ACTION_VIEW, it)
                    startActivity(intent)
                }

                return true
            }
        }

        mBinding.webView.loadUrl(url)
    }
}