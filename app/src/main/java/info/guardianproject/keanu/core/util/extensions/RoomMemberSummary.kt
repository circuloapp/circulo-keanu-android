package info.guardianproject.keanu.core.util.extensions

import info.guardianproject.keanu.core.ImApp
import org.matrix.android.sdk.api.session.room.model.RoomMemberSummary

/**
 * It seems, there's currently an issue with #displayName. This is a workaround to fix that.
 * Deprecate and remove, as soon as #displayName does what it should again!
 */
val RoomMemberSummary.displayNameWorkaround: String?
    get() {
        if (displayName?.isNotBlank()==true) return displayName

        if (userId != null) {
            return userId
        }

        return displayName
    }