package info.guardianproject.keanu.core.util

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import org.matrix.android.sdk.api.session.room.model.message.ImageInfo
import org.matrix.android.sdk.api.session.room.model.message.MessageStickerContent
import java.io.File

object StickerHelper {

    fun createShareStickerUri(context: Context, assetUri: Uri) : Uri{
        val inputStream = context.assets.open(assetUri.path!!)
        val temp = File(context.cacheDir, assetUri.lastPathSegment!!)
        temp.outputStream().use { inputStream.copyTo(it) }
        return  FileProvider.getUriForFile(context, "${context.packageName}.provider", temp)
    }

    fun createStickerContent(context: Context, assetUri: Uri): MessageStickerContent {
        val inputStream = context.assets.open(assetUri.path!!)
        val temp = File(context.cacheDir, assetUri.lastPathSegment!!)

        temp.outputStream().use { inputStream.copyTo(it) }

        val contentUri = FileProvider.getUriForFile(context, "${context.packageName}.provider", temp)

        val data = AttachmentHelper.createFromContentUri(context.contentResolver, contentUri)

        val name = assetUri.pathSegments[assetUri.pathSegments.size - 2] +
                "-" + assetUri.lastPathSegment?.split("\\.".toRegex())?.firstOrNull()

        return MessageStickerContent(
            body = name,
            info = ImageInfo(
                mimeType = data.getSafeMimeType(),
                width = data.width?.toInt() ?: 0,
                height = data.height?.toInt() ?: 0,
                size = data.size
            ),
            url = contentUri.toString()
        )
    }
}