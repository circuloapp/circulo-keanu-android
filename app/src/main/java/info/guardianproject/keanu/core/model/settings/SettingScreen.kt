package info.guardianproject.keanu.core.model.settings

enum class SettingScreen {
    NOTIFICATIONS,
    PREFERENCES,
    ACCOUNT,
    SECURITY_PRIVACY,
    PHYSICAL_SAFETY,
    PHOTOS_VIDEOS,
    ABOUT,
    FEEDBACK
}